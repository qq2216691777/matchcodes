#ifndef _INTERFACE_H__
#define _INTERFACE_H__


typedef struct DOWNLOAD_DATA{
	unsigned short Header;
	unsigned char Len;
	unsigned char Sum;			// Sum = (Buff_speed[4] +....+ Buff_speed[11]);
	unsigned int angle_id;		//�Ƕ�id  
	int angle;
	unsigned char voice_cmd;
}send2PcBuff;



typedef struct UPLOAD_DATA{
	unsigned short Header;
	unsigned char Len;
	unsigned char Sum;
	char angle_cmd;
	char voice_cmd; 

}send2StmBuff;

typedef struct devicestatus
{
	send2PcBuff send;
	send2StmBuff rec;	
}Device;

extern Device DevStatus;


#define RESET_ANGLE 1

#define STOP_VOICE 1
#define WARNING_VOICE 2
#define TIPS_VOICE 3

#define SPEAKING  1
#define NOSPEAKING  2

#define PC2STM	0XAAAA
#define STM2PC	0XAAAA

#define VOICE	0XAAAF


unsigned char process2StmData( unsigned char *pData,long len );
void send2PcData( unsigned char *sendData );
void sendVoice();


#endif
