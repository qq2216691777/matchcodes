/* STM32F10X_HD,USE_STDPERIPH_DRIVER */


/**
  ***************************************
  *@File     main.c
  *@Auther   YNXF  
  *@date     2015-10-29
  *@Version  v1.0
  *@brief              
  *@HAUST - RobotLab
  ***************************************
**/

#include "stm32f10x.h"
#include "delay.h"
#include "usart.h"
#include "stm32f10x_conf.h"
#include "xfs5051.h"
#include "timer.h"


/*
DMA UART Receive data: 
	UART1   A9   A10
	UART2   A2 tx  A3 rx
	UART3	B10	 B11
*/

int main( void )
{
	u32 t;
	delay_init();
	NVIC_Configuration(); //设置中断分组
	uart_init( 115200 );
	uart2_init( 115200 );
	uart3_init( 115200 );
	delay_ms(500);
	TIM3_Int_Init(399,7199);//10Khz的计数频率，计数到5000为500ms  
	XFS_FrameInfo("[h0] ");
	//

	while(1)
	{
	 //	printf("串口测试:%d\n",t++);
	//	sendVoice();
		delay_ms(100);
		if(t>65536) 
			t=0;
	}
}

