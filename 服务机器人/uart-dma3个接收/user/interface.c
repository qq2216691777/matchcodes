#include "interface.h"
#include <string.h>
#include <stdlib.h>
#include "xfs5051.h"


Device DevStatus;

unsigned char voicedata[4096]={0};

unsigned char speaking = 0;

void sendVoice()
{
	if(speaking)
	{
		XFS_FrameInfo(voicedata);
		speaking = 0;
	}
}

unsigned char process2StmData( unsigned char *pData,long len )
{
	
	
	send2StmBuff *p = (send2StmBuff *)pData;
//	printf("%d\r\n",p->Len);//复位
	if( p->Header == VOICE )
	{
		memcpy(voicedata,&pData[2],len);
		voicedata[len+2] = 0;
		speaking = 1;				
	}
	if( p->Header != PC2STM )
		return 0;
	
	if( (pData[4]+pData[5]) != p->Sum )
	{
		return 0;
	}
	
	if( p->angle_cmd == RESET_ANGLE )		//复位角度
	{
	//	printf("RESET_ANGLE\r\n");//复位
		Uart2PutString("RESET\r\n");
	}
	
	if( p->voice_cmd ==STOP_VOICE )
	{
	//	printf("STOP_VOICE\r\n");//停止说话
	}
	else if( p->voice_cmd ==WARNING_VOICE )
	{
	//	printf("WARNING_VOICE\r\n");
	}
	else if( p->voice_cmd ==TIPS_VOICE )
	{
	//	printf("TIPS_VOICE\r\n");
	}
	
	return 1;
		
}

unsigned char process2PcData( unsigned char *pData, send2PcBuff *p )
{
	unsigned char i;
	unsigned char sum=0;
	p = (send2PcBuff *)pData;
	
	if( p->Header != STM2PC )
		return 0;
	
	for(i=4;i<12;i++)
	{
		sum += pData[i];
	}
	if(p->Sum != sum)
		return 0;
	
	
	
	
	
	return 1;
		
}


void send2PcData( unsigned char *sendData )
{
	unsigned char sum = 0;
	unsigned char i;
	unsigned char *pData = (unsigned char *)&DevStatus.send;
	send2PcBuff *p = (send2PcBuff *)sendData;
	memcpy(sendData,pData,14);
	
	p->Header = STM2PC;
	
	for( i=4;i<13;i++ )
	{
		sum += pData[i];
	}
	
	p->Sum = sum;
	
	//DevStatus.send.angle_id++;
	
	return ;
	
}

unsigned char * send2Stm( send2StmBuff *p )
{

	unsigned char *pData = (unsigned char *)p;
	
	p->Sum = pData[4]+pData[5];
	p->Header = PC2STM;
	return pData;
	
}

