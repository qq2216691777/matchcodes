#ifndef __USART3_H__
#define __USART3_H__
#include "main.h"

#if __JUDSYSTEM__TEMP__

void USART3_Configuration(void);

#else

void USART3_Configuration(void);
static void USART3_FIFO_Init(void);
void *USART3_GetRxBuf(void);

#define  BSP_USART3_DMA_RX_BUF_LEN               60u 
#define  BSP_USART3_RX_BUF_SIZE_IN_FRAMES         (BSP_USART3_RX_BUF_SIZE / UART3_FRAME_LENGTH)
#define  UART3_FRAME_LENGTH                            48u
#endif

#endif
