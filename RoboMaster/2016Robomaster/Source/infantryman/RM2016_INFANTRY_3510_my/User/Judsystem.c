#include "main.h"

#if (0==__JUDSYSTEM__TEMP__)

#include "Judgmentsystem.h"

GameInfo gameinfo;

void Judsystem_Init( void )
{
	gameinfo.remainTime = 0;		//比赛剩余时间
	gameinfo.remainLifeValue = 0;	//机器人剩余血量
	gameinfo.realChassisOutV = 0;		//实时地盘输出电压
	gameinfo.realChassisOutA = 0;		//实时地盘输出电流
	gameinfo.runeStatus[0] = 0;		//四个小符点状态
	gameinfo.runeStatus[1] = 0;
	gameinfo.runeStatus[2] = 0;
	gameinfo.runeStatus[3] = 0;
	gameinfo.bigRune0Status = 0;		//大符点1状态
	gameinfo.bigRune1status = 0;		//大符点2状态
	gameinfo.weakId = 0;				//收到伤害的装甲ID
	gameinfo.ways = 0;				//血量变化类型
	gameinfo.realBulletShootSpeed = 0;	//子弹射速
	gameinfo.realBulletShootFreq = 0;	//子弹射频
	gameinfo.realGolfShootSpeed = 0;	//高尔夫射速
	gameinfo.realGolfShootFreq = 0;	//高尔夫射频
}

//void Can_send()
//{
//	int16_t cm1_iq, cm2_iq, cm3_iq, cm4_iq;
//	
//    CanTxMsg tx_message;
//    tx_message.StdId = 0x558;
//    tx_message.IDE = CAN_Id_Standard;
//    tx_message.RTR = CAN_RTR_Data;
//    tx_message.DLC = 0x08;
//	
//	cm1_iq = (int16_t)gameinfo.ways;
//	cm2_iq = (int16_t)gameinfo.weakId;
//	cm3_iq = (int16_t)(gameinfo.realChassisOutV*100);
//	cm4_iq = (int16_t)(gameinfo.realChassisOutA*100);
//    
//    tx_message.Data[0] = (uint8_t)(cm1_iq >> 8);
//    tx_message.Data[1] = (uint8_t)cm1_iq;
//    tx_message.Data[2] = (uint8_t)(cm2_iq >> 8);
//    tx_message.Data[3] = (uint8_t)cm2_iq;
//    tx_message.Data[4] = (uint8_t)(cm3_iq >> 8);
//    tx_message.Data[5] = (uint8_t)cm3_iq;
//    tx_message.Data[6] = (uint8_t)(cm4_iq >> 8);
//    tx_message.Data[7] = (uint8_t)cm4_iq;
//    CAN_Transmit(CAN1,&tx_message);
//}

void Judsystem_Prcess( uint8_t *pData )
{
	
	tGameInfo *Raw_gameinfo;
	tRealBloodChangedData *Raw_GameBlood;
	tRealShootData *GameShoot;
	
	GameBlood *Raw_Hdr = (GameBlood *)pData;
	
	if( 0XA5 != Raw_Hdr->Fram_Hdr.Sof )
	{
		return ;
	}
	
	switch( Raw_Hdr->CmdID )
	{
		case 0x0001:
		{
			if( Raw_Hdr->Fram_Hdr.DataLengh == 38 )
			{
				Raw_gameinfo = (tGameInfo *)(&pData[6]);
				gameinfo.remainTime = Raw_gameinfo->remainTime;		//比赛剩余时间
				gameinfo.remainLifeValue = Raw_gameinfo->remainLifeValue;	//机器人剩余血量
				gameinfo.realChassisOutV = Raw_gameinfo->realChassisOutV;		//实时地盘输出电压
				gameinfo.realChassisOutA = Raw_gameinfo->realChassisOutA;		//实时地盘输出电流
				
				gameinfo.runeStatus[0] = Raw_gameinfo->runeStatus[0];		//四个小符点状态
				gameinfo.runeStatus[1] = Raw_gameinfo->runeStatus[1];		
				gameinfo.runeStatus[2] = Raw_gameinfo->runeStatus[2];		
				gameinfo.runeStatus[3] = Raw_gameinfo->runeStatus[3];		
				
				gameinfo.bigRune0Status = Raw_gameinfo->bigRune0Status;		//大符点1状态
				gameinfo.bigRune1status = Raw_gameinfo->bigRune1status;		//大符点2状态
				gameinfo.Total_Power = gameinfo.realChassisOutV*gameinfo.realChassisOutA;
				
			}
			break;
		}
				
		case 0x0002:		//血量变化
		{
			if( Raw_Hdr->Fram_Hdr.DataLengh == 3 )
			{
				Raw_GameBlood = (tRealBloodChangedData *)(&pData[6]);
				gameinfo.weakId = Raw_GameBlood->weakId;				//收到伤害的装甲ID
				gameinfo.ways = Raw_GameBlood->way;				//血量变化类型
				//while(1);
			}
			break;
		}
		
		case 0x0003:
		{
			if( Raw_Hdr->Fram_Hdr.DataLengh == 16 )
			{
				GameShoot = (tRealShootData *)(&pData[6]);
				gameinfo.realBulletShootSpeed = GameShoot->realBulletShootSpeed;	//子弹射速
				gameinfo.realBulletShootFreq = GameShoot->realBulletShootFreq;	//子弹射频
				gameinfo.realGolfShootSpeed = GameShoot->realGolfShootSpeed;	//高尔夫射速
				gameinfo.realGolfShootFreq = GameShoot->realGolfShootFreq;	//高尔夫射频
			}
			break;
		}
		default: 
			break;
		
	}

	//Can_send();
	
}

#endif

