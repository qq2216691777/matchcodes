#ifndef _JUDGMENTSYSTEM_H__
#define _JUDGMENTSYSTEM_H__

#include "stm32f4xx.h"

/***********FrameHeader 格式***********************/
typedef __packed struct
{
	uint8_t Sof;
	uint16_t DataLengh;
	uint8_t CRC8;	
}FrameHeader;

typedef __packed struct
{
	uint16_t CRC16;
}FrameTail;


/***********比赛进程信息（ 0x0001）46Bytes***************/
typedef enum{
	BUFF_TYPE_NONE, //无效
	BUFF_TYPE_ARMOR = 0x01, //防御符
	BUFF_TYPE_SUPPLY = 0x04, //加血符
	BUFF_TYPE_BULLFTS= 0x08, //加弹符
}eBuffType;

typedef __packed struct
{
	uint8_t flag; //0 无效， 1 有效
	uint32_t x;
	uint32_t y;
	uint32_t z;
	uint32_t compass;
}tGpsData;

typedef __packed struct
{
	uint32_t remainTime;
	uint16_t remainLifeValue;
	float realChassisOutV;
	float realChassisOutA;
	uint8_t runeStatus[4];
	uint8_t bigRune0Status;
	uint8_t bigRune1status;
	uint8_t conveyorBelts0:2;
	uint8_t conveyorBelts1:2;
	uint8_t parkingApron0:1;
	uint8_t parkingApron1:1;
	uint8_t parkingApron2:1;
	uint8_t parkingApron3:1;
	tGpsData gpsData;
}tGameInfo;

typedef __packed struct
{
	FrameHeader 	Fram_Hdr;
	uint16_t 		CmdID;
	tGameInfo 		GameInfo;
	FrameTail 		CRC16;
	
}GameProcess;


/*****************实时血量变化信息(0x0002)*******************/
typedef __packed struct
{
	uint8_t weakId:4;
	uint8_t way:4;
	uint16_t value;
}tRealBloodChangedData;

typedef __packed struct
{
	FrameHeader 		Fram_Hdr;
	uint16_t 			CmdID;
	tRealBloodChangedData 		BloodChangedData;
	FrameTail 			CRC16;
}GameBlood;

/*******************实时射击信息(0x0003)*********************/
typedef __packed struct
{
	float realBulletShootSpeed;
	float realBulletShootFreq;
	float realGolfShootSpeed;
	float realGolfShootFreq;
}tRealShootData;

typedef __packed struct
{
	FrameHeader 		Fram_Hdr;
	uint16_t 			CmdID;
	tRealShootData 		ShootData;
	FrameTail 			CRC16;
}GameShoot;

/*********************************************************/


#endif

