#ifndef _JUDSYSTEM__H_
#define _JUDSYSTEM__H_
#include "stm32f4xx.h"

void Judsystem_Prcess( uint8_t *pData );
void Judsystem_Init( void );

	

typedef __packed struct
{
	uint32_t remainTime;		//比赛剩余时间
	uint16_t remainLifeValue;	//机器人剩余血量
	float realChassisOutV;		//实时地盘输出电压
	float realChassisOutA;		//实时地盘输出电流
	uint8_t runeStatus[4];		//四个小符点状态
	uint8_t bigRune0Status;		//大符点1状态
	uint8_t bigRune1status;		//大符点2状态
	uint8_t weakId;				//收到伤害的装甲ID
	uint8_t ways;				//血量变化类型
	uint16_t Change_Bloodvalue;
	float realBulletShootSpeed;	//子弹射速
	float realBulletShootFreq;	//子弹射频
	float realGolfShootSpeed;	//高尔夫射速
	float realGolfShootFreq;	//高尔夫射频
}GameInfo;

extern GameInfo gameinfo;

#endif
