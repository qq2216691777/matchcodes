#ifndef _PORTS_H__
#define _PORTS_H__
#include "main.h"

typedef struct
{
	float Pitch_angle;
	float Yaw_angle;
	unsigned char wheel;
	unsigned char shoot;
	int8_t fb;
	int8_t lr;	
}F_Port;

extern F_Port F103_port;

#endif

