#ifndef _MAIN_H___
#define _MAIN_H___

#include "stm32f10x.h"
#include "stm32f10x_conf.h"
#include "delay.h"
#include "usart.h"
#include "led.h"
#include "CAN.h"
#include "infrared.h"
#include "timer.h"
#include "move_process.h"

extern u8 Status;
extern Can_send_data Can_Data;
extern u8 rx_flag;

#define 	MOVE_SPEED 	70		//�����ƶ��ٶ� 1~99


#define VAL_LIMIT(val, min, max)\
if(val<=min)\
{\
	val = min;\
}\
else if(val>=max)\
{\
	val = max;\
}\


#endif

