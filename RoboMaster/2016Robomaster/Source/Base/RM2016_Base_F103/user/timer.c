#include "main.h"

//////////////////////////////////////////////////////////////////////////////////	 
//本程序只供学习使用，未经作者许可，不得用于其它任何用途
//ALIENTEK战舰STM32开发板
//定时器 驱动代码	   
//正点原子@ALIENTEK
//技术论坛:www.openedv.com
//修改日期:2012/9/3
//版本：V1.0
//版权所有，盗版必究。
//Copyright(C) 广州市星翼电子科技有限公司 2009-2019
//All rights reserved									  
//////////////////////////////////////////////////////////////////////////////////   	 

//通用定时器3中断初始化
//这里时钟选择为APB1的2倍，而APB1为36M
//arr：自动重装值。
//psc：时钟预分频数
//这里使用的是定时器3!
void TIM3_Int_Init(u16 arr)
{
    TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE); //时钟使能
	
	//定时器TIM3初始化
	TIM_TimeBaseStructure.TIM_Period = arr; //设置在下一个更新事件装入活动的自动重装载寄存器周期的值	
	TIM_TimeBaseStructure.TIM_Prescaler =7199; //设置用来作为TIMx时钟频率除数的预分频值
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1; //设置时钟分割:TDTS = Tck_tim
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  //TIM向上计数模式
	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure); //根据指定的参数初始化TIMx的时间基数单位
 
	TIM_ITConfig(TIM3,TIM_IT_Update,ENABLE ); //使能指定的TIM3中断,允许更新中断

	//中断优先级NVIC设置
	NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;  //TIM3中断
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;  //先占优先级0级
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;  //从优先级3级
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE; //IRQ通道被使能
	NVIC_Init(&NVIC_InitStructure);  //初始化NVIC寄存器


	TIM_Cmd(TIM3, ENABLE);  //使能TIMx					 
}

extern u16  can_tx_pixel_X;
extern u16  can_tx_pixel_Y; 
extern u16  can_tx_can_flag;
extern u8 move_flag;
extern infrared inf;
extern int ms100;
int8_t fb_speed = MOVE_SPEED;
int8_t lr_speed = MOVE_SPEED;
extern int ms15;
//定时器3中断服务程序
void TIM3_IRQHandler(void)   //TIM3中断
{

	if (TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET)  //检查TIM3更新中断发生与否
	{
		TIM_ClearITPendingBit(TIM3, TIM_IT_Update  );  //清除TIMx更新中断标志 
		if(ms100>0)
			ms100--;
		else
			ms100 = 0;
		
		
		
		if(ms15)
		{
			
			move_flag = 1;
		}
		else
		{
			
			move_flag = 0;
		}
		
		if( ms15>0 )
			ms15--;
		
		if( move_flag )		//若有无人机
		{
			
			Scan_inf();
			
			if( inf.No5 )
			{
				fb_speed = -MOVE_SPEED;
				
			}
			else if( inf.No7 )
			{	
				fb_speed = MOVE_SPEED;
				
			}
			if( inf.No6 )
			{	
				lr_speed = -MOVE_SPEED;
			}			
			else if( inf.No8 )
			{
				lr_speed = MOVE_SPEED;
			}
			
			
			Can_Data.fb = fb_speed;
			Can_Data.lr = lr_speed;
//			Can_Data.shoot = 0;
//			Can_Data.wheel = 0;
		}
		else
		{
			Can_Data.fb = 0;
			Can_Data.lr = 0;
		}
		
		if( !Status )
		{
			//printf( "%.2f-%.2f-%d-%d-%d-%d\r\n",Can_Data.Pitch, Can_Data.Yaw, Can_Data.wheel, Can_Data.shoot, Can_Data.fb, Can_Data.lr);
			tx_data_can1( Can_Data.Pitch, Can_Data.Yaw, Can_Data.wheel, Can_Data.shoot, Can_Data.fb, Can_Data.lr );
		}
		else
		{
			//ms15 = 0;
			tx_data_can1( 0, 0, 0, 0, 0, 0 );
			move_flag = 0;
		}
	}
}












