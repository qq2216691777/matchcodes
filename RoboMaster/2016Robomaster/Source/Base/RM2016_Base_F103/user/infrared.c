#include "main.h"

infrared inf;

u8 rx_flag = 0;

#define Inf_NO5	PBin(15)
#define Inf_NO6	PBin(14)
#define Inf_NO7	PBin(13)
#define Inf_NO8	PBin(12)

void infrared_Init( void )
{
	 GPIO_InitTypeDef GPIO_InitStructure;
	//初始化KEY0-->GPIOA.13,KEY1-->GPIOA.15  上拉输入
 	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB,ENABLE);//使能PORTA,PORTE时钟

	GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_15|GPIO_Pin_14|GPIO_Pin_13|GPIO_Pin_12;//PB12~15
	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU; //设置成上拉输入
 	GPIO_Init(GPIOB, &GPIO_InitStructure);//初始化GPIOE2,3,4
	
	
	inf.No5 = 0;
	inf.No6 = 0;
	inf.No7 = 0;
	inf.No8 = 0;


}

int Scan_inf( void )
{
	static u8 No5_num = 0;
	static u8 No6_num = 0;
	static u8 No7_num = 0;
	static u8 No8_num = 0;
	

/************************************/
	if( 0==Inf_NO5 )
	{
		No5_num++;
		if( No5_num>4 )
		{
			inf.No5 = 1;
			No5_num = 11;
		}
	}
	else
	{
		No5_num = 0;
		inf.No5 = 0;
	}
/************************************/	
	if( 0==Inf_NO6 )
	{
		No6_num++;
		if( No6_num>4 )
		{
			inf.No6 = 1;
			No6_num = 11;
		}
	}
	else
	{
		No6_num = 0;
		inf.No6 = 0;
	}
/************************************/	
	if( 0==Inf_NO7 )
	{
		No7_num++;
		if( No7_num>4 )
		{
			inf.No7 = 1;
			No7_num = 11;
		}
	}
	else
	{
		No7_num = 0;
		inf.No7 = 0;
	}
/************************************/	
	if( 0==Inf_NO8 )
	{
		No8_num++;
		if( No8_num>4 )
		{
			inf.No8 = 1;
			No8_num = 11;
		}
	}
	else
	{
		No8_num = 0;
		inf.No8 = 0;
	}
		
	
	return 0;
}
/**
  **************************************
  *@ Pitch  俯仰角度
  *@ Yaw	航向角度
  *@ shoot	是否发射子弹
  *@ fb		前行后退速度
  *@ lr 	左右平移速度
  **************************************
**/
void tx_data_can1( float tar_Pitch, float tar_Yaw, u8 tar_whell, u8 tar_shoot, int8_t tar_fb, int8_t tar_lr )
{
	u16 sdata1,sdata2,sdata3,sdata4;
	static float Now_DataPitch = 0;
	static float Now_DataYaw = 0;
	
	VAL_LIMIT( tar_Pitch, -10.0,10.0 );
	VAL_LIMIT( tar_Yaw, -60.0, 60.0);
	VAL_LIMIT( tar_fb, -100,100 );
	VAL_LIMIT( tar_lr, -100,100 );
	
	if( Now_DataPitch>tar_Pitch )
	{
		if( (Now_DataPitch-tar_Pitch)>0.5 )
			Now_DataPitch -= 0.5;
		else
			Now_DataPitch=tar_Pitch;
	}
	else if( Now_DataPitch<tar_Pitch )
	{
		if( (Now_DataPitch-tar_Pitch)<-0.5 )
			Now_DataPitch += 0.5;
		else
			Now_DataPitch=tar_Pitch;
	}
	
	if( Now_DataYaw>tar_Yaw )
	{
		if( (Now_DataYaw-tar_Yaw)>0.5 )
			Now_DataYaw -= 0.5;
		else
			Now_DataYaw=tar_Yaw;
	}
	else if( Now_DataYaw<tar_Yaw )
	{
		if( (Now_DataYaw-tar_Yaw)<-0.5 )
			Now_DataYaw += 0.5;
		else
			Now_DataYaw=tar_Yaw;
	}
	
	sdata1 = ((short)(Now_DataPitch*100))*2;
	sdata2 = ((short)(Now_DataYaw*100))*2;
	
	sdata3 = (short)(((tar_fb+128)<<8)|(tar_lr+128));
	sdata4 = rx_flag;
	
	if( tar_shoot )
	{
		tar_whell = 1;
		sdata1 |= 0x0001;
	}
	else
		sdata1 &= (~0x0001);
	
	if( tar_whell )
		sdata2 |= 0x0001;
	else
		sdata2 &= (~0x0001);
//	printf("%d-%d-%d\r\n",sdata1,sdata2,sdata4);
	CAN1_sent( sdata1,sdata2,sdata3,sdata4 );
}

void Send_Move( void )
{
	while((USART1->SR&0X40)==0);//循环发送,直到发送完毕   
    USART1->DR = (u8) 'M';  
	while((USART1->SR&0X40)==0);//循环发送,直到发送完毕   
    USART1->DR = (u8) 'O';  
	while((USART1->SR&0X40)==0);//循环发送,直到发送完毕   
    USART1->DR = (u8) 'V';  
	while((USART1->SR&0X40)==0);//循环发送,直到发送完毕   
    USART1->DR = (u8) 'E';  
	while((USART1->SR&0X40)==0);//循环发送,直到发送完毕   
    USART1->DR = (u8) 0X0d; 
	while((USART1->SR&0X40)==0);//循环发送,直到发送完毕   
    USART1->DR = (u8) 0X0a; 
}


void Send_Stop( void )
{
	while((USART1->SR&0X40)==0);//循环发送,直到发送完毕   
    USART1->DR = (u8) 'S';  
	while((USART1->SR&0X40)==0);//循环发送,直到发送完毕   
    USART1->DR = (u8) 'T';  
	while((USART1->SR&0X40)==0);//循环发送,直到发送完毕   
    USART1->DR = (u8) 'O';  
	while((USART1->SR&0X40)==0);//循环发送,直到发送完毕   
    USART1->DR = (u8) 'P';  
	while((USART1->SR&0X40)==0);//循环发送,直到发送完毕   
    USART1->DR = (u8) 0X0d;
	while((USART1->SR&0X40)==0);//循环发送,直到发送完毕   
    USART1->DR = (u8) 0X0a;
}
