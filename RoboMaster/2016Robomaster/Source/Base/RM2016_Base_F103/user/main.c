/* STM32F10X_HD,USE_STDPERIPH_DRIVER */

/**
  ***************************************
  *@File     main.c
  *@Auther   YNXF  
  *@date     2016-08-11
  *@Version  v1.0
  *@brief    RoboMasters Base          
  *@HAUST - RobotLab
  ***************************************
**/


#include "main.h"
#include "interface.h"
#include <math.h>

Base_Inter *Inter;
Can_send_data Can_Data;

u8 move_flag = 0;

short  can_tx_pixel_X=0;
short  can_tx_pixel_Y=0; 
u16  can_tx_can_flag = 0;

extern u16 Change_Blood;
extern unsigned char Receive_Attack;


#define Inf_NO5	PBin(15)
#define Inf_NO6	PBin(14)
#define Inf_NO7	PBin(13)
#define Inf_NO8	PBin(12)

short  Temp_pixel_X[3]={0};
short  Temp_pixel_Y[3]={0};

int ms15=0;
int ms100=0;
u8 move_stop=0;
int main( void )
{
	int i=5;
	const static float PIXEL_Buttom_P = 0.36397*360;
	const static float PIXEL_Buttom_Y = 1.7321*640;
	unsigned char Temp_pixel_YI=0;
	unsigned char Temp_pixel_XI=0;
	float tar_pitch_angle;
	float tar_yaw_angle;
	static int uart_ii=0;
	
	
	delay_init();
	NVIC_Configuration(); //设置中断分组
	delay_ms(100);
	
	LED_Configuration();
	uart_init( 115200 );
	uart3_init( 115200 );
	
	move_flag = 0;
	
	CAN_Configuration();
	
	Can_Data.fb = 0;
	Can_Data.lr = 0;
	Can_Data.Pitch = 0;
	Can_Data.Yaw = 0;
	Can_Data.wheel = 0;
	Can_Data.shoot = 0;
	
	printf("Uart3 is ok \r\n");
	
	TIM3_Int_Init(149); //15ms  
	LED_ON();

	while(1)
	{
		if( Change_Blood>40 )		//受到攻击就开始移动
		{
			ms15 = 1666;			//血量变化时 基地运行时间
			Change_Blood = 0;
		}
		if( USART_RX_STA & 0x8000 )
		{
			
			USART_RX_BUF[USART_RX_STA&0xfff] = 0;
			Inter = (Base_Inter *)USART_RX_BUF;
			//printf("%s",USART_RX_BUF);
			if( Inter->sum == (uint8)(Inter->Flag + Inter->x + Inter->y) )
			{
				uart_ii = 0;
				can_tx_pixel_X = ((short)Inter->x)+60;
				can_tx_pixel_Y = ((short)Inter->y)-120;
				can_tx_can_flag = (u16)Inter->Flag;	
				//printf("Get Data from Uart1\r\n");
				
				Temp_pixel_YI = (Temp_pixel_YI+1)%3;
				Temp_pixel_Y[Temp_pixel_YI] = can_tx_pixel_Y;
				can_tx_pixel_Y = (Temp_pixel_Y[0]+Temp_pixel_Y[1]+Temp_pixel_Y[2])/3;
				
				Temp_pixel_XI = (Temp_pixel_XI+1)%3;
				Temp_pixel_X[Temp_pixel_XI] = can_tx_pixel_X;
				can_tx_pixel_X = (Temp_pixel_X[0]+Temp_pixel_X[1]+Temp_pixel_X[2])/3;
				
				tar_yaw_angle = atan2( can_tx_pixel_X,PIXEL_Buttom_Y )*40.0;
				tar_pitch_angle = -atan2( can_tx_pixel_Y,PIXEL_Buttom_P )*4.70;
				
		//		printf("x:%d y:%d p:%f y:%f\r\n",can_tx_pixel_X ,can_tx_pixel_Y,tar_pitch_angle,tar_yaw_angle);
				
				if( (Inter->Flag==0x41) )	//若发现无人机
				{
					if(ms100==0)
						ms100 = 100;
					else if( ms100>10 )
						ms15 = 1666;
				}
				
					if(Inter->Flag==0x81)	//开始发射子弹
					{
						Can_Data.wheel = 1;
						Can_Data.shoot = 1;
//						Can_Data.fb = 0;
//						Can_Data.lr = 0;
						Can_Data.Pitch = tar_pitch_angle;
						Can_Data.Yaw = tar_yaw_angle;
						//tx_data_can1( tar_pitch_angle,tar_yaw_angle,1,1,0,0);
					}
					else if(Inter->Flag==0x80)
					{
						Can_Data.wheel = 1;
						Can_Data.shoot = 0;
//						Can_Data.fb = 0;
//						Can_Data.lr = 0;
						Can_Data.Pitch = tar_pitch_angle;
						Can_Data.Yaw = tar_yaw_angle;
						
					}
				i--;
				if(i<5)
				{
//					if( move_flag )
					//	Send_Stop();
//					move_flag = 0;
					i=5;
				}
				else if( i>20 )
				{
					i = 30;
				//	move_flag = 1;
					ms15 = 1666;		//移动10s
				//	Send_Move();//发送move
				}
				
				
			}
		
			USART_RX_STA= 0;
		}
		else
		{
			uart_ii++;
			if(uart_ii>100)
			{
				uart_ii = 201;
				Can_Data.wheel = 0;
				Can_Data.shoot = 0;
			}
		}
		delay_ms(10);
		if( move_flag )
		{
			if( move_stop==1 )
			{
				Send_Move();
				Send_Move();
				move_stop=0;
			}
		}
		else
		{
			if( move_stop==0 )
			{
				Send_Stop();
				Send_Stop();
				move_stop=1;
			}
		}
			//printf("\r\n");

	}
}

