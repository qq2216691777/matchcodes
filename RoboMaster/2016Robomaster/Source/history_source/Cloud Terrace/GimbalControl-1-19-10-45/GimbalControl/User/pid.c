/**
  ******************************************************************************
  *@file      pid.c
  *@author    叶念西风
  *@version   2015.12.15
  *@brief     RoboMasters -- Cloud Terrace        
  *@HAUST - RobotLab           
  ******************************************************************************
**/
#include "pid.h"

#define INIT_Y_POSITION 169.65f
#define INIT_P_POSITION 192.20f

/**
  ***********************************************
  *@函数名: int Motor_Yaw_Control( float Tar_Angle, float Now_Angle, float groy )
  *@参数  : Tar_Angle 目标角度      Now_Angle 当前角度      groy  当前角速度
  *@返回值: 电机需要的转速  
  *@作用  : 电机角度控制
  ***********************************************
**/
int Motor_Yaw_Control( float Tar_Angle_Y, float Now_Angle_Y, float groy_Y )
{
	int output=0;
	if( Tar_Angle_Y > 60 )
	{
		Tar_Angle_Y = 60;
	}
	else if( Tar_Angle_Y < -60 )
	{
		Tar_Angle_Y = -60;
	} 
	Tar_Angle_Y += INIT_Y_POSITION;
	
	output = Velocity_Control_203( Position_Control_203( Tar_Angle_Y, Now_Angle_Y ), groy_Y );
	
	return output;
}

int Motor_Pitch_Control( float Tar_Angle_P, float Now_Angle_P, float groy_P )
{
	
	static float last_error=0;
	static float error = 0;
	static double i_error=0;
	
	const float p_p = 100.0;
	const float p_i = 0.10;
    const float p_d = 400;
	
	if( Tar_Angle_P > 60 )
	{
		Tar_Angle_P = 60;
	}
	else if( Tar_Angle_P < -60 )
	{
		Tar_Angle_P = -60;
	} 
	
	Tar_Angle_P += INIT_P_POSITION;
	
	last_error = error;
	error = Tar_Angle_P - Now_Angle_P;
	i_error += error;
	
	if( i_error > 200 )
	{
		i_error=200;
	}
	else if( i_error<-200 )
	{
		i_error=-200;
	}
	
	return (error*p_p + i_error*p_i + ( error - last_error)*p_d);
}

/*
int Motor_Pitch_Control( float Tar_Angle, float Now_Angle, float groy )
{
	int output=0;
	if( Tar_Angle > 60 )
	{
		Tar_Angle = 60;
	}
	else if( Tar_Angle < -60 )
	{
		Tar_Angle = -60;
	} 
	Tar_Angle += INIT_Y_POSITION;
	
	output = Velocity_Control_201( Position_Control_201( Tar_Angle, Now_Angle ), groy );
	
	return output;
}*/

/**
  ***********************************************
  *@函数名: float Velocity_Control_203(float target_velocity_203, float current_velocity_203)
  *@参数  : target_velocity_203 目标角速度      current_velocity_203 当前角速度   
  *@返回值: YAW轴 电机需要的转速  
  *@作用  : YAW轴 电机角速度控制
  ***********************************************
**/
float Velocity_Control_203(float target_velocity_203, float current_velocity_203)
{
    const float vy_p = 10.0;
//	const float v_i = 0.00;
    const float vy_d = 2.0;
    
    static float errory_vy[2] = {0.0,0.0};
//	static double i_error=0;
    
    errory_vy[0] = errory_vy[1];
    errory_vy[1] = target_velocity_203 - current_velocity_203;
 //   i_error += error_v[1];
//	if( i_error> 200 )
//		i_error = 200;
//	else if( i_error<-200)
//		i_error=-200;
	
 //   output = error_v[1] * v_p + i_error*v_i + (error_v[1] - error_v[0]) * v_d;
    
	return errory_vy[1] * vy_p  + (errory_vy[1] - errory_vy[0]) * vy_d;
}


/**
  ***********************************************
  *@函数名: float Position_Control_203(float target_position_203, float current_position_203)
  *@参数  : target_position_203 目标角度      current_position_203 当前角度   
  *@返回值: YAW轴 电机需要的转速  
  *@作用  : YAW轴 电机角度控制
  ***********************************************
**/
float Position_Control_203(float target_position_203, float current_position_203)
{
    const float ly_p = 8.00; 
	const float ly_i = 0.001; 
    const float ly_d = 6.0; 
    
    static float error_py[2] = {0.0,0.0};
	static double I_error = 0;
    
    error_py[0] = error_py[1];
    error_py[1] =  target_position_203 - current_position_203;
	
	I_error += error_py[1];
	if( I_error>500 )
	{
		I_error = 500;
	}
	else if( I_error<-500 )
	{
		I_error = -500;
	}
	
    return error_py[1] * ly_p + I_error * ly_i + (error_py[1] - error_py[0]) * ly_d;
    
}


/********************************************************************************
                           pitch轴电调板的速度环控制
                      输入 pitch轴当前速度 pitch轴目标速度
*********************************************************************************/
float Velocity_Control_201(float target_velocity_201, float current_velocity_201)
{
    const float vp_p = 0.0;
	const float vp_i = 0.0;
    const float vp_d = 0.0;
    
    static float error_vp[2] = {0.0,0.0};
	static double ivp_error=0;
    
    error_vp[0] = error_vp[1];
    error_vp[1] = target_velocity_201 - current_velocity_201;
    ivp_error += error_vp[1];
	if( ivp_error> 500 )
		ivp_error = 500;
	else if( ivp_error<-500)
		ivp_error=-500;
	
    return error_vp[1] * vp_p + ivp_error*vp_i + (error_vp[1] - error_vp[0]) * vp_d;
     
}


/********************************************************************************
                           pitch轴电调板的位置环控制
                      输入 pitch轴当前位置 pitch轴目标位置
*********************************************************************************/
float Position_Control_201(float target_position_201, float current_position_201)
{
    const float lp_p = 0.000;//3#5#:0.760
	const float lp_i = 0.0;//0.000035;
    const float lp_d = 0.0;//3.5;
    
    static float error_pp[2] = {0.0,0.0};
	static double Ipp_error = 0;
    
    error_pp[0] = error_pp[1];
    error_pp[1] =  target_position_201 - current_position_201;
	
	Ipp_error += error_pp[1];
	if( Ipp_error>500 )
	{
		Ipp_error = 500;
	}
	else if( Ipp_error<-500 )
	{
		Ipp_error = -500;
	}
	
    return error_pp[1] * lp_p + Ipp_error * lp_i + (error_pp[1] - error_pp[0]) * lp_d;

}

