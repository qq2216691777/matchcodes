/**
  ******************************************************************************
  *@file      pid.h
  *@author    Ҷ������
  *@version   2015.12.15
  *@brief     RoboMasters -- Cloud Terrace        
  *@HAUST - RobotLab           
  ******************************************************************************
**/
#ifndef _PID_H__
#define _PID_H__

#define abs(x) ((x)>0? (x):(-(x)))

int Motor_Yaw_Control( float Tar_Angle, float Now_Angle, float groy );
int Motor_Pitch_Control( float Tar_Angle, float Now_Angle, float groy );

float Position_Control_203(float target_position_203, float current_position_203);
float Velocity_Control_203(float target_velocity_203, float current_velocity_203);

float Position_Control_201(float target_position_201, float current_position_201 );
float Velocity_Control_201(float target_velocity_201, float current_velocity_201 );

#endif
