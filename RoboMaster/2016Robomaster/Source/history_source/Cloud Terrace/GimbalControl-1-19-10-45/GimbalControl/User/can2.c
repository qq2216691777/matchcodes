#include "stm32f4xx.h"                  // Device header
#include "delay.h"
#include "usart.h"
#include "can1.h"
#include "can2.h"
#include "pid.h"
#include "timer.h"
#include "mpu6050.h"

/*----CAN2_TX-----PB13----*/
/*----CAN2_RX-----PB12----*/

void CAN2_Configuration(void)
{
    CAN_InitTypeDef        can;
    CAN_FilterInitTypeDef  can_filter;
    GPIO_InitTypeDef       gpio;
    NVIC_InitTypeDef       nvic;

    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN1, ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN2, ENABLE);

    GPIO_PinAFConfig(GPIOB, GPIO_PinSource13, GPIO_AF_CAN2);
    GPIO_PinAFConfig(GPIOB, GPIO_PinSource12, GPIO_AF_CAN2); 

    gpio.GPIO_Pin = GPIO_Pin_13 | GPIO_Pin_12 ;
    gpio.GPIO_Mode = GPIO_Mode_AF;
    GPIO_Init(GPIOB, &gpio);

    nvic.NVIC_IRQChannel = CAN2_RX0_IRQn;  
    nvic.NVIC_IRQChannelPreemptionPriority = 0;
    nvic.NVIC_IRQChannelSubPriority = 1;
    nvic.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&nvic);

    CAN_DeInit(CAN2);
    CAN_StructInit(&can);

    can.CAN_TTCM = DISABLE;
    can.CAN_ABOM = DISABLE;    
    can.CAN_AWUM = DISABLE;    
    can.CAN_NART = DISABLE;    
    can.CAN_RFLM = DISABLE;    
    can.CAN_TXFP = ENABLE;     
    can.CAN_Mode = CAN_Mode_Normal; 
    can.CAN_SJW  = CAN_SJW_1tq;
    can.CAN_BS1 = CAN_BS1_6tq;
    can.CAN_BS2 = CAN_BS2_1tq;
    can.CAN_Prescaler = 2;   //CAN BaudRate 42/(1+9+4)/3=1Mbps
    CAN_Init(CAN2, &can);
    
    can_filter.CAN_FilterNumber=14;
    can_filter.CAN_FilterMode=CAN_FilterMode_IdMask;
    can_filter.CAN_FilterScale=CAN_FilterScale_32bit;
    can_filter.CAN_FilterIdHigh=0x0000;
    can_filter.CAN_FilterIdLow=0x0000;
    can_filter.CAN_FilterMaskIdHigh=0x0000;
    can_filter.CAN_FilterMaskIdLow=0x0000;
    can_filter.CAN_FilterFIFOAssignment=0;//the message which pass the filter save in fifo0  
    can_filter.CAN_FilterActivation=ENABLE;
    CAN_FilterInit(&can_filter);
    
    CAN_ITConfig(CAN2,CAN_IT_FMP0,ENABLE);
//	CAN_ITConfig(CAN2,CAN_IT_TME,ENABLE); 
}




void CAN2_TX0_IRQHandler(void)
{
    CanRxMsg rx_message;
    if (CAN_GetITStatus(CAN2,CAN_IT_FMP0)!= RESET)  
    {
        CAN_ClearITPendingBit(CAN2, CAN_IT_FMP0);  
        CAN_Receive(CAN2, CAN_IT_FMP0, &rx_message);
		//printf("OK\r\n");
	}		
}

extern float Target_Pitch;
extern float Target_Yaw;
#define MAX_VALUE  1684
#define MIN_VALUE  364
#define TEMP_SIZE  12

void CAN2_RX0_IRQHandler(void)
{
    CanRxMsg rx_message;
	u16 Pitch_Temp = 0;
	u16 Yaw_Temp = 0;
	u16 Pitch_Mouse = 0;
	u16 Yaw_Mouse = 0;
	u32 Data_Yaw_Sum=0;
	u32 Data_Pitch_Sum=0;
	static u16 Data_Yaw_Temp[TEMP_SIZE];
	static u16 Data_Pitch_Temp[TEMP_SIZE];
	static u8 TEMP_i=0;
	static int I_Pitch=0;
	static int I_Yaw=0;
	u8 i=0;
	
    if (CAN_GetITStatus(CAN2,CAN_IT_FMP0)!= RESET) 
    {
        CAN_ClearITPendingBit(CAN2, CAN_IT_FMP0);
        CAN_Receive(CAN2, CAN_FIFO0, &rx_message);
		
		if( rx_message.StdId == 0x503 )
		{
			if( 1 == rx_message.Data[5] )    //遥控器
			{
				Yaw_Temp = (u16)(rx_message.Data[1] | (rx_message.Data[0]<<8));   
				Pitch_Temp = (u16)(rx_message.Data[3] | (rx_message.Data[2]<<8));	

				if( Yaw_Temp > MAX_VALUE)
					Yaw_Temp = MAX_VALUE;
				else if( Yaw_Temp < MIN_VALUE)
					Yaw_Temp = MIN_VALUE;
				
				
				if( Pitch_Temp > MAX_VALUE)
					Pitch_Temp = MAX_VALUE;
				else if( Pitch_Temp < MIN_VALUE)
					Pitch_Temp = MIN_VALUE;
				
				Data_Yaw_Temp[TEMP_i] = Yaw_Temp;
				Data_Pitch_Temp [TEMP_i] = Pitch_Temp;
				for( i=0; i<TEMP_SIZE; i++ )
				{
					Data_Yaw_Sum += Data_Yaw_Temp[i];
					Data_Pitch_Sum += Data_Pitch_Temp[i];
				}
				TEMP_i++;
				if( TEMP_i >= TEMP_SIZE )
					TEMP_i=0;
				
				Yaw_Temp = (Data_Yaw_Sum/TEMP_SIZE);
				Pitch_Temp = (Data_Pitch_Sum/TEMP_SIZE);
				Target_Yaw = -( ( Yaw_Temp-1024)/660.0)*45;  //方向取反
				Target_Pitch = ( (Pitch_Temp-1024)/660.0)*30;
			}
			else if( 2 == rx_message.Data[5] )    //键盘鼠标
			{
				Yaw_Mouse = (u16)(rx_message.Data[1] | (rx_message.Data[0]<<8));   
				Pitch_Mouse = (u16)(rx_message.Data[3] | (rx_message.Data[2]<<8));	

				I_Pitch += Pitch_Mouse-1024;
				I_Yaw += Yaw_Mouse-1024;
				
				if( I_Yaw > MAX_VALUE-1024)
					I_Yaw = MAX_VALUE-1024;
				else if( I_Yaw < MIN_VALUE-1024)
					I_Yaw = MIN_VALUE-1024;
				
				if( I_Pitch > MAX_VALUE-1024)
					I_Pitch = MAX_VALUE-1024;
				else if( I_Pitch < MIN_VALUE-1024)
					I_Pitch = MIN_VALUE-1024;
				
				Target_Yaw = -( ( I_Yaw*0.7)/660.0)*45;  //方向取反  0.7为灵敏度
				Target_Pitch = ( (I_Pitch)/660.0)*30;
			//	printf("%d\r\n",I_Pitch);
			}
			
		}
    }
}
