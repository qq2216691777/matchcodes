#ifndef __CAN_H__
#define __CAN_H__

#include <stm32f4xx.h>
#include "delay.h"

extern volatile unsigned char OverCurr_flag;

void CAN1_Configuration(void);

void Cmd_ESC(int16_t current_201,int16_t current_202,int16_t current_203);

#endif 
