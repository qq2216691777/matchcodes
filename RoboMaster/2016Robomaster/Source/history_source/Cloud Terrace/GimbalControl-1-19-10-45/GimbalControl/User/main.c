/**
  ******************************************************************************
  *@file      main.c
  *@author    叶念西风
  *@version   2015.12.15
  *@brief     RoboMasters -- Cloud Terrace        
  *@HAUST - RobotLab           
  ******************************************************************************
**/

#include "stm32f4xx.h"                  // Device header
#include "delay.h"
#include "usart.h"
#include "can1.h"
#include "can2.h"
#include "pid.h"
#include "timer.h"
#include "mpu6050.h"

/*  外部25MHZ时钟无法使用  内部时钟16MHZ  */

void LED_Init(void);
int main(void)
{

	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);//设置系统中断优先级分组2
	delay_init( 16 );
	
	delay_ms(500);			//等待外设稳定
	
	LED_Init();
	
	CAN1_Configuration();
	CAN2_Configuration();
	uart_init(115200);
	
	Cmd_ESC(0,0,0);
	
	
	MPU6050_Init();
	
	printf("Init is ok\r\n");
	TIM3_Int_Init( 10000-1, 16-1 ); // 周期 10ms
	while(1)
	{
		
	
	}
}

/********************************************************************************
                           led灯调试使用
*********************************************************************************/
void LED_Init(void)
{    	 
	GPIO_InitTypeDef  GPIO_InitStructure;

	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);//使能GPIOF时钟

	//GPIOF9,F10初始化设置
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_14;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;//普通输出模式
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;//推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;//上拉
	GPIO_Init(GPIOB, &GPIO_InitStructure);//初始化

	GPIO_SetBits(GPIOB,GPIO_Pin_14 );//GPIOF9,F10设置高，灯灭

}


