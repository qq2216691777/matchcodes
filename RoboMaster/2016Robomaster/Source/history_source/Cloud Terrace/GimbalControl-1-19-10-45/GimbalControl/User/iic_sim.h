/**
  ***************************************
  *@Project  Air mouse
  *@FileName iic_sim.h
  *@Auther   YNXF  
  *@date     2015-11-4
  *@Version  v1.0
  *@Action   for stm32f103  
  ***************************************
**/

#ifndef _IIC_SIM_H__
#define _IIC_SIM_H__

#define IIC_SCL 	PBout(6)
#define IIC_SDA 	PBout(7)
#define READ_SDA 	PBin(7)

void IIC_SIM_Init( void );
void IIC_SIM_Start( void );
void IIC_SIM_Write_Byte( unsigned char txd );
unsigned char IIC_SIM_Read_Byte( void );
void IIC_SIM_Send_Ack( unsigned char Ack );
unsigned char IIC_SIM_Wait_Ack( void );
void IIC_SIM_Stop( void );
void IIC_SIM_Delay( unsigned char d );

static void SDA_IN(void);
static void SDA_OUT(void);

#endif

