#include "can1.h"
#include "usart.h"
//cyq:修改了管脚
//VP230---CAN_TX---PA12(CANTX) 
//VP230---CAN_RX---PA11(CANRX) 

/*************************************************************************
                          CAN1_Configuration
描述：初始化CAN1配置为1M波特率
*************************************************************************/
void CAN1_Configuration(void)
{
    CAN_InitTypeDef        can;
    CAN_FilterInitTypeDef  can_filter;
    GPIO_InitTypeDef       gpio;
    NVIC_InitTypeDef       nvic;

    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN1, ENABLE);

    GPIO_PinAFConfig(GPIOA, GPIO_PinSource11, GPIO_AF_CAN1);
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource12, GPIO_AF_CAN1);

    gpio.GPIO_Pin = GPIO_Pin_11 | GPIO_Pin_12;
	gpio.GPIO_Speed = GPIO_Speed_50MHz;
    gpio.GPIO_Mode = GPIO_Mode_AF;
    GPIO_Init(GPIOA, &gpio);
    
    nvic.NVIC_IRQChannel = CAN1_RX0_IRQn;
    nvic.NVIC_IRQChannelPreemptionPriority = 0;
    nvic.NVIC_IRQChannelSubPriority = 1;
    nvic.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&nvic);
    
    nvic.NVIC_IRQChannel = CAN1_TX_IRQn;
    nvic.NVIC_IRQChannelPreemptionPriority = 1;
    nvic.NVIC_IRQChannelSubPriority = 1;
    nvic.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&nvic); 
    
    CAN_DeInit(CAN1);
    CAN_StructInit(&can);
    
    can.CAN_TTCM = DISABLE;
    can.CAN_ABOM = DISABLE;
    can.CAN_AWUM = DISABLE;
    can.CAN_NART = DISABLE;
    can.CAN_RFLM = DISABLE;
    can.CAN_TXFP = ENABLE;
    can.CAN_Mode = CAN_Mode_Normal;  //  1 4 3 2
    can.CAN_SJW  = CAN_SJW_1tq;
    can.CAN_BS1 = CAN_BS1_6tq;
    can.CAN_BS2 = CAN_BS2_1tq;
    can.CAN_Prescaler =2;   //CAN BaudRate 42/(1+9+4)/3=1Mbps
    CAN_Init(CAN1, &can);

    can_filter.CAN_FilterNumber=0;
    can_filter.CAN_FilterMode=CAN_FilterMode_IdMask;
    can_filter.CAN_FilterScale=CAN_FilterScale_32bit;
    can_filter.CAN_FilterIdHigh=0x0000;
    can_filter.CAN_FilterIdLow=0x0000;
    can_filter.CAN_FilterMaskIdHigh=0x0000;
    can_filter.CAN_FilterMaskIdLow=0x0000;
    can_filter.CAN_FilterFIFOAssignment=0;//the message which pass the filter save in fifo0
    can_filter.CAN_FilterActivation=ENABLE;
    CAN_FilterInit(&can_filter);
    
    CAN_ITConfig(CAN1,CAN_IT_FMP0,ENABLE);
    CAN_ITConfig(CAN1,CAN_IT_TME,ENABLE); 
}


unsigned char can_tx_success_flag=0;
void CAN1_TX_IRQHandler(void) //CAN TX
{
    if (CAN_GetITStatus(CAN1,CAN_IT_TME)!= RESET) 
	{
	   CAN_ClearITPendingBit(CAN1,CAN_IT_TME);
       can_tx_success_flag=1;
    }
}

/*************************************************************************
                          CAN1_RX0_IRQHandler
描述：云台电机的CAN数据接收中断
*************************************************************************/
float Now_Pitch;
float Now_Yaw;
void CAN1_RX0_IRQHandler(void)
{
    CanRxMsg rx_message;    
    static float aver_p[4];
	static float aver_y[4];
	u32 Pitch;
	u32 Yaw;	
	
    if (CAN_GetITStatus(CAN1,CAN_IT_FMP0)!= RESET) 
	{
        CAN_ClearITPendingBit(CAN1, CAN_IT_FMP0);
        CAN_Receive(CAN1, CAN_FIFO0, &rx_message);       
                
        if(rx_message.StdId == 0x201)
        {             
			Pitch = (rx_message.Data[0]*256 + rx_message.Data[1]);
			aver_p[3] = aver_p[2];
			aver_p[2] = aver_p[1];
			aver_p[1] = aver_p[0];
			aver_p[0] = Pitch;
			
			Now_Pitch = (float)( aver_p[3] + aver_p[2] + aver_p[1] + aver_p[0] )*0.01098766f;
        }
        if(rx_message.StdId == 0x202)
        { 
             //获得云台电机0x202的码盘值           
        }		
        if(rx_message.StdId == 0x203)
        { 
			 Yaw = (rx_message.Data[0]*256 + rx_message.Data[1]);
			 aver_y[3] = aver_y[2];
			 aver_y[2] = aver_y[1];
			 aver_y[1] = aver_y[0];
			 aver_y[0] = Yaw;
			 Now_Yaw = (float)( aver_y[3] + aver_y[2] + aver_y[1] + aver_y[0] )*0.01098766f;
        }
	
    }
}





/********************************************************************************
   给电调板发送指令，ID号为0x200，只用两个电调板，数据回传ID为0x201和0x202
	 cyq:更改为发送三个电调的指令。
*********************************************************************************/
void Cmd_ESC(int16_t current_201,int16_t current_202,int16_t current_203)
{
    CanTxMsg tx_message;
	
	if( current_203 > 4999)
	{
		current_203=4999;
	}
	else if( current_203 < -4999)
	{
		current_203=-4999;
	}
	
	if( current_201 > 4999)
	{
		current_201=4999;
	}
	else if( current_201 < -4999)
	{
		current_201=-4999;
	}
    
    tx_message.StdId = 0x200;
    tx_message.IDE = CAN_Id_Standard;
    tx_message.RTR = CAN_RTR_Data;
    tx_message.DLC = 0x08;
    
    tx_message.Data[0] = (uint8_t)(current_201 >> 8); //高8位
    tx_message.Data[1] = (uint8_t)current_201;				//低8位
    tx_message.Data[2] = (uint8_t)(current_202 >> 8);
    tx_message.Data[3] = (uint8_t)current_202;
    tx_message.Data[4] = (uint8_t)(current_203 >> 8);
    tx_message.Data[5] = (uint8_t)current_203;
    tx_message.Data[6] = 0x00;
    tx_message.Data[7] = 0x00;
    
    CAN_Transmit(CAN1,&tx_message);
}



