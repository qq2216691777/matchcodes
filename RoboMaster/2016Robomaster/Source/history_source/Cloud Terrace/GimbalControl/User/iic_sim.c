/**
  ***************************************
  *@Project  Air mouse
  *@FileName iic_sim.c
  *@Auther   YNXF  
  *@date     2015-11-4
  *@Version  v1.0
  *@Action   for stm32f103  
  ***************************************
**/

#include "iic_sim.h"
#include "stm32f4xx.h"                  // Device header

#include "sys.h"

void IIC_SIM_Init( void )
{
    GPIO_InitTypeDef   gpio;
    
	
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB,ENABLE);
	
	gpio.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;
	gpio.GPIO_Mode = GPIO_Mode_OUT;
	gpio.GPIO_OType = GPIO_OType_OD;
	gpio.GPIO_Speed = GPIO_Speed_100MHz; 
    GPIO_Init(GPIOB, &gpio);
	
	IIC_SCL = 1;
	IIC_SDA = 1;
	
}

void IIC_SIM_Start( void )
{
	SDA_OUT();
	IIC_SDA = 1;
	IIC_SCL = 1;
	
	IIC_SIM_Delay( 18 );
	IIC_SDA = 0;
	IIC_SIM_Delay( 18 );
	IIC_SCL = 0;
}

void IIC_SIM_Write_Byte( unsigned char txd )
{
	unsigned char i;
	SDA_OUT();
	IIC_SCL = 0;
	for( i=0; i<8; i++ )
	{
		IIC_SDA = (txd&0x80)>>7;
		txd <<= 1;
		IIC_SIM_Delay( 14 );
		IIC_SCL = 1;
		IIC_SIM_Delay( 14 );
		IIC_SCL = 0;
		IIC_SIM_Delay( 14 );
	}
	while( IIC_SIM_Wait_Ack() );	
}

unsigned char IIC_SIM_Read_Byte( void )
{
	unsigned char i;
	unsigned char Receive=0;
	
	SDA_IN();
	for( i=0; i<8; i++ )
	{
		IIC_SCL = 0;
		IIC_SIM_Delay( 14 );
		IIC_SCL = 1;
		Receive <<= 1;
		if( READ_SDA )
			Receive++;
		IIC_SIM_Delay( 14 );
	}
	return Receive;
}

void IIC_SIM_Send_Ack( unsigned char Ack )
{
	IIC_SCL = 0;
	SDA_OUT();
	IIC_SDA = Ack;
	IIC_SIM_Delay( 16 );
	IIC_SCL = 1;
	IIC_SIM_Delay( 16 );
	IIC_SCL = 0;
}

unsigned char IIC_SIM_Wait_Ack( void )
{
	unsigned char ERRtime;
	SDA_IN();
	IIC_SDA = 1;
	IIC_SIM_Delay( 16 );
	IIC_SCL = 1;
	IIC_SIM_Delay( 16 );
	ERRtime = 0;
	while( READ_SDA )
	{
		ERRtime++;
		if( ERRtime > 250 )
		{
			IIC_SIM_Stop();
			return 1;
		}
	}
	IIC_SCL = 0;
	return 0;
}
void IIC_SIM_Stop( void )
{
	SDA_OUT();
	IIC_SDA = 0;
	IIC_SIM_Delay( 12 );
	IIC_SCL = 1;
	IIC_SIM_Delay( 12 );
	IIC_SDA = 1;
	IIC_SIM_Delay( 12 );
}

static void SDA_OUT(void)
{
    GPIO_InitTypeDef   gpio;
    
	gpio.GPIO_Pin = GPIO_Pin_7;
    gpio.GPIO_Mode = GPIO_Mode_OUT;
	gpio.GPIO_OType = GPIO_OType_OD;
	gpio.GPIO_Speed = GPIO_Speed_100MHz; 
    GPIO_Init(GPIOB, &gpio);
}

static void SDA_IN(void)
{
    GPIO_InitTypeDef   gpio;
    
	gpio.GPIO_Pin = GPIO_Pin_7;
    
    gpio.GPIO_Mode = GPIO_Mode_IN;
    gpio.GPIO_OType = GPIO_OType_PP;
    gpio.GPIO_PuPd = GPIO_PuPd_NOPULL;
    gpio.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &gpio);
}

void IIC_SIM_Delay( unsigned char d )
{
	for( ; d<1; d--);
}


