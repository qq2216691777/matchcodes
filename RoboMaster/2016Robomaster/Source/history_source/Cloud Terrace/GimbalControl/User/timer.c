/**
  ******************************************************************************
  *@file      timer.c
  *@author    叶念西风
  *@version   2015.12.15
  *@brief     RoboMasters -- Cloud Terrace        
  *@HAUST - RobotLab           
  ******************************************************************************
**/
#include "timer.h"
#include "stm32f4xx.h"                  // Device header
#include "can1.h"
#include "pid.h"
#include "usart.h"
#include "mpu6050.h"

extern float Now_Pitch;
extern float Now_Yaw;
extern short Pitch_S;
extern short Yaw_S;

//通用定时器3中断初始化
//arr：自动重装值。
//psc：时钟预分频数
//定时器溢出时间计算方法:Tout=((arr+1)*(psc+1))/Ft us.
//Ft=定时器工作频率,单位:Mhz
//这里使用的是定时器3!
void TIM3_Int_Init( unsigned short arr, unsigned short psc )
{
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3,ENABLE);  ///使能TIM3时钟
	
  TIM_TimeBaseInitStructure.TIM_Period = arr; 	//自动重装载值
	TIM_TimeBaseInitStructure.TIM_Prescaler=psc;  //定时器分频
	TIM_TimeBaseInitStructure.TIM_CounterMode=TIM_CounterMode_Up; //向上计数模式
	TIM_TimeBaseInitStructure.TIM_ClockDivision=TIM_CKD_DIV1; 
	
	TIM_TimeBaseInit(TIM3,&TIM_TimeBaseInitStructure);//初始化TIM3
	
	TIM_ITConfig(TIM3,TIM_IT_Update,ENABLE); //允许定时器3更新中断
	TIM_Cmd(TIM3,ENABLE); //使能定时器3
	
	NVIC_InitStructure.NVIC_IRQChannel=TIM3_IRQn; //定时器3中断
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=0x01; //抢占优先级1
	NVIC_InitStructure.NVIC_IRQChannelSubPriority=0x03; //子优先级3
	NVIC_InitStructure.NVIC_IRQChannelCmd=ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
}

float Target_Pitch=0;
float Target_Yaw=0;
//extern MPU6050_REAL_DATA   MPU6050_Real_Data;
//定时器3中断服务函数
void TIM3_IRQHandler(void)
{
	static int timeer=0;
	Angle gg;
	
	if(TIM_GetITStatus(TIM3,TIM_IT_Update)==SET) //溢出中断
	{
		GPIO_SetBits(GPIOB,GPIO_Pin_14 );
		Get_Acc_Ang_Data( &gg );
			
		Cmd_ESC( Motor_Pitch_Control( Target_Pitch, Now_Pitch, gg.Pitch ), 0, Motor_Yaw_Control( Target_Yaw, Now_Yaw , gg.Yaw ) );
		timeer++;
		
		if( timeer>10 )
		{
			timeer=0;
		//	printf("%d   \r\n",tarx);
		//	printf( "%f\r\n", gg.Yaw );
		//	printf("p %.3f  S %.3f \r\n", Pitch, gg.Yaw );
		}
		GPIO_ResetBits(GPIOB,GPIO_Pin_14 );
	}
	TIM_ClearITPendingBit(TIM3,TIM_IT_Update);  //清除中断标志位
}

