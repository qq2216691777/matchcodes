#include "main.h"

/***************************************
	*ZXW add function CAN send
****************************************/
void DRIVER_send_DATA(unsigned short RC_num1,unsigned short RC_num2,unsigned short RC_num3,unsigned short RC_num4)
{
    CanTxMsg tx_message;
    
    tx_message.StdId = 0x300;
    tx_message.IDE = CAN_Id_Standard;
    tx_message.RTR = CAN_RTR_Data;
    tx_message.DLC = 0x08;
    
    tx_message.Data[0] = (uint8_t)(RC_num1>>8);
    tx_message.Data[1] = (uint8_t)(RC_num1);
	
    tx_message.Data[2] = (uint8_t)(RC_num2>>8);
    tx_message.Data[3] = (uint8_t)(RC_num2);
	
    tx_message.Data[4] = (uint8_t)(RC_num3>>8);
    tx_message.Data[5] = (uint8_t)(RC_num3);
	
    tx_message.Data[6] = (uint8_t)(RC_num4>>8);
    tx_message.Data[7] = (uint8_t)(RC_num4);
    
    CAN_Transmit(CAN1,&tx_message);
}


/********************************************************************
                       keyboard cotorl function
********************************************************************/
static unsigned char keyboard_val = 0;
unsigned char CAN_send = 0;
#define zheng_SPEED 1024+50
#define   fan_SPEED 1024-50
#define stop_SPEED  1024
#define MIX_keyboard_value1 1024+90
#define MIX_keyboard_value2 1024-90



void keyboard_control_function( RC_Ctl_t RC_Ctl_temp )
{
	unsigned short SEND_SPEED_DATA[4] = {0};

	if(keyboard_val == 1)
		{
			keyboard_val = 0;
		}
	switch(RC_Ctl_temp.key.v)      //unsigned short --> unsigned char
		{
			case 0x00:
				
				SEND_SPEED_DATA[0]  =  stop_SPEED;
				SEND_SPEED_DATA[1]  =  stop_SPEED;
				SEND_SPEED_DATA[2]  =  stop_SPEED;
				SEND_SPEED_DATA[3]  =  stop_SPEED;
				break;
			case  KEYBOARD_forward:	//前进   1,2,3,4 zheng
			
				SEND_SPEED_DATA[0]  =  zheng_SPEED;
				SEND_SPEED_DATA[1]  =  zheng_SPEED;
				SEND_SPEED_DATA[2]  =  zheng_SPEED;
				SEND_SPEED_DATA[3]  =  zheng_SPEED;
				break;
			case  KEYBOARD_retreat:		//后退   1,2,3,4 fan
			
				SEND_SPEED_DATA[0]  =  fan_SPEED;
				SEND_SPEED_DATA[1]  =  fan_SPEED;
				SEND_SPEED_DATA[2]  =  fan_SPEED;
				SEND_SPEED_DATA[3]  =  fan_SPEED;
				break;
			case  KEYBOARD_right:	//右平移 1,4 zheng  2,3 fan
			
				SEND_SPEED_DATA[0]  =  fan_SPEED;
				SEND_SPEED_DATA[1]  =  zheng_SPEED;
				SEND_SPEED_DATA[2]  =  zheng_SPEED;
				SEND_SPEED_DATA[3]  =  fan_SPEED;
				break;
			case  KEYBOARD_left:	//左平移 2,3zheng 1,4 fan

				SEND_SPEED_DATA[0]  =  zheng_SPEED;
				SEND_SPEED_DATA[1]  =  fan_SPEED;
				SEND_SPEED_DATA[2]  =  fan_SPEED;
				SEND_SPEED_DATA[3]  =  zheng_SPEED;
				break;
			case  KEYBOARD_forward_right:   //右前  1,4 zheng
				keyboard_val = 1;	
			
				SEND_SPEED_DATA[0]  =  zheng_SPEED;
				SEND_SPEED_DATA[1]  =  stop_SPEED;
				SEND_SPEED_DATA[2]  =  stop_SPEED;
				SEND_SPEED_DATA[3]  =  zheng_SPEED;
				break;
			
			case KEYBOARD_retreat_left:   
				
				SEND_SPEED_DATA[0]  =  fan_SPEED;
				SEND_SPEED_DATA[1]  =  stop_SPEED;
				SEND_SPEED_DATA[2]  =  stop_SPEED;
				SEND_SPEED_DATA[3]  =  fan_SPEED;
				break;
			
			case  KEYBOARD_forward_left:   //左前  2,3 zheng
				keyboard_val = 1;	
			
				SEND_SPEED_DATA[0]  =  stop_SPEED;
				SEND_SPEED_DATA[1]  =  zheng_SPEED;
				SEND_SPEED_DATA[2]  =  zheng_SPEED;
				SEND_SPEED_DATA[3]  =  stop_SPEED;
				break;
			
			case KEYBOARD_retreat_right :
				
				SEND_SPEED_DATA[0]  =  stop_SPEED;
				SEND_SPEED_DATA[1]  =  fan_SPEED;
				SEND_SPEED_DATA[2]  =  fan_SPEED;
				SEND_SPEED_DATA[3]  =  stop_SPEED;
				break;
				
			case  KEYBOARD_Q:    //左旋转  2.4 zheng 1,3 fan
				
				SEND_SPEED_DATA[0]  =  fan_SPEED;
				SEND_SPEED_DATA[1]  =  fan_SPEED;
				SEND_SPEED_DATA[2]  =  zheng_SPEED;
				SEND_SPEED_DATA[3]  =  zheng_SPEED;
				break;
			
			case  KEYBOARD_left_forward:   //左上旋转前进
				
				SEND_SPEED_DATA[0]  =  stop_SPEED;
				SEND_SPEED_DATA[1]  =  stop_SPEED;
				SEND_SPEED_DATA[2]  =  zheng_SPEED;
				SEND_SPEED_DATA[3]  =  zheng_SPEED;
				break;
			
			case  KEYBOARD_E:   //右旋转 1,3 zheng 2,4 fan
				
				SEND_SPEED_DATA[0]  =  zheng_SPEED;
				SEND_SPEED_DATA[1]  =  zheng_SPEED;
				SEND_SPEED_DATA[2]  =  fan_SPEED;
				SEND_SPEED_DATA[3]  =  fan_SPEED;
				break;
			
			case  KEYBOARD_right_forward:               //右上旋转前进
				
				SEND_SPEED_DATA[0]  =  zheng_SPEED;
				SEND_SPEED_DATA[1]  =  zheng_SPEED;
				SEND_SPEED_DATA[2]  =  stop_SPEED;
				SEND_SPEED_DATA[3]  =  stop_SPEED;
				break;
			
			case KEYBOARD_SHIFT_W:
				SEND_SPEED_DATA[0]  =  MIX_keyboard_value1;
				SEND_SPEED_DATA[1]  =  MIX_keyboard_value1;
				SEND_SPEED_DATA[2]  =  MIX_keyboard_value1;
				SEND_SPEED_DATA[3]  =  MIX_keyboard_value1;
			    break;
			
			case KEYBOARD_SHIFT_S:
				SEND_SPEED_DATA[0]  =  MIX_keyboard_value2;
				SEND_SPEED_DATA[1]  =  MIX_keyboard_value2;
				SEND_SPEED_DATA[2]  =  MIX_keyboard_value2;
				SEND_SPEED_DATA[3]  =  MIX_keyboard_value2;
			    break;
			
			case KEYBOARD_SHIFT_A:
				SEND_SPEED_DATA[0]  =  MIX_keyboard_value2;
				SEND_SPEED_DATA[1]  =  MIX_keyboard_value1;
				SEND_SPEED_DATA[2]  =  MIX_keyboard_value1;
				SEND_SPEED_DATA[3]  =  MIX_keyboard_value2;
			    break;
			
			case KEYBOARD_SHIFT_D:
				SEND_SPEED_DATA[0]  =  MIX_keyboard_value1;
				SEND_SPEED_DATA[1]  =  MIX_keyboard_value2;
				SEND_SPEED_DATA[2]  =  MIX_keyboard_value2;
				SEND_SPEED_DATA[3]  =  MIX_keyboard_value1;
			    break;
			
			default :
				SEND_SPEED_DATA[0]  =  stop_SPEED;
				SEND_SPEED_DATA[1]  =  stop_SPEED;
				SEND_SPEED_DATA[2]  =  stop_SPEED;
				SEND_SPEED_DATA[3]  =  stop_SPEED;
			break;
		}	
		if(RC_Ctl_temp.mouse.x != 0)
		{
			SEND_SPEED_DATA[0]  +=  RC_Ctl_temp.mouse.x*1.2;
			SEND_SPEED_DATA[1]  +=  RC_Ctl_temp.mouse.x*1.2;
			SEND_SPEED_DATA[2]  -=  RC_Ctl_temp.mouse.x*1.2;
			SEND_SPEED_DATA[3]  -=  RC_Ctl_temp.mouse.x*1.2;
		}
		
		DRIVER_send_DATA(SEND_SPEED_DATA[0],SEND_SPEED_DATA[1],SEND_SPEED_DATA[2],SEND_SPEED_DATA[3]);

			SEND_SPEED_DATA[0]  = 0;
			SEND_SPEED_DATA[1]  = 0;
			SEND_SPEED_DATA[2]  = 0;
			SEND_SPEED_DATA[3]  = 0;
}

/********************************************************************
                         遥控器控制函数
********************************************************************/
void robomasters_ctol(unsigned short RC_ch0,unsigned short RC_ch2,unsigned short RC_ch3)
{
	if(RC_ch0 > 1644) 
	{	
		DRIVER_send_DATA(1025+40,1025+40,1023-40,1023-40);
		return;
	}  //右旋
	
	if(RC_ch0 < 404) 
	{
		DRIVER_send_DATA(1023-40,1023-40,1025+40,1025+40);
		return;
	}
	
	if(RC_ch2 <= 1074 && RC_ch2 >=974 && RC_ch3 <= 1074 && RC_ch3 >=974) //死区
	{
		DRIVER_send_DATA(1024,1024,1024,1024);
		return;
	}  
	
	if(RC_ch3 > 1074 )  //前进
	{
		DRIVER_send_DATA(1025+40,1025+40,1025+40,1025+40);
		return;
	}

	if( RC_ch3 < 974)  //后退
	{
		DRIVER_send_DATA(1023-40,1023-40,1023-40,1023-40);
		return;
	}

	if(RC_ch2 > 1074) //右移动
	{
		DRIVER_send_DATA(1025+40,1023-40,1023-40,1025+40);
		return;
	}

	if(RC_ch2 < 974) //左移
	{
		DRIVER_send_DATA(1023-40,1025+40,1025+40,1023-40);
		return;
	}
}

