#include "bsp.h"

#include "global_define.h"

void BSP_Init(void){
    
    /* Configure the NVIC Preemption Priority Bits */
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);   
    LED_Configuration();            //指示灯初始化
    RELAY_Configuration();	    	//cyq:继电器初始化
    LASER_Configuration();
    CAN1_Configuration();            //初始化CAN
    
	
    USART3_Configuration();          //串口1初始化
	USART1_Configuration();          //usart1 and DMA Configuration init.  遥控器
	CAN2_Configuration(); 
	//while(1);
}


