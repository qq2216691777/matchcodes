/**
  ******************************************************************************
  *@file      timer.c
  *@author    叶念西风
  *@version   2015.12.15
  *@brief     RoboMasters -- Cloud Terrace        
  *@HAUST - RobotLab           
  ******************************************************************************
**/
#include "main.h"
#include "stm32f4xx.h"                  // Device header



//通用定时器3中断初始化
//arr：自动重装值。
//psc：时钟预分频数
//定时器溢出时间计算方法:Tout=((arr+1)*(psc+1))/Ft us.
//Ft=定时器工作频率,单位:Mhz
//这里使用的是定时器3!
void TIM3_Int_Init( unsigned short arr, unsigned short psc )
{
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3,ENABLE);  ///使能TIM3时钟
	
	TIM_TimeBaseInitStructure.TIM_Period = arr; 	//自动重装载值
	TIM_TimeBaseInitStructure.TIM_Prescaler=psc;  //定时器分频
	TIM_TimeBaseInitStructure.TIM_CounterMode=TIM_CounterMode_Up; //向上计数模式
	TIM_TimeBaseInitStructure.TIM_ClockDivision=TIM_CKD_DIV1; 
	
	TIM_TimeBaseInit(TIM3,&TIM_TimeBaseInitStructure);//初始化TIM3
	
	TIM_ITConfig(TIM3,TIM_IT_Update,ENABLE); //允许定时器3更新中断
	TIM_Cmd(TIM3,ENABLE); //使能定时器3
	
	NVIC_InitStructure.NVIC_IRQChannel=TIM3_IRQn; //定时器3中断
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=0x01; //抢占优先级1
	NVIC_InitStructure.NVIC_IRQChannelSubPriority=0x03; //子优先级3
	NVIC_InitStructure.NVIC_IRQChannelCmd=ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
}

float Target_Pitch=110;
float Target_Yaw=110;



extern float Now_Yaw;
extern float Now_Pitch;



//extern MPU6050_REAL_DATA   MPU6050_Real_Data;
//定时器3中断服务函数
void TIM3_IRQHandler(void)
{
	short Yaw_temp;
	short Pitch_temp;
	
	if(TIM_GetITStatus(TIM3,TIM_IT_Update)==SET) //溢出中断
	{
		
		
		Yaw_temp = Motor_Yaw_Control( 0.0,Now_Yaw,0.0);
		Pitch_temp = Motor_Pitch_Control( 0.0, Now_Pitch, 0.0);
	//	Gimbal( Yaw_temp,Pitch_temp ); //YAW PITCH
		domain_control( 0,0,0,0);
	
		
		
	}
	TIM_ClearITPendingBit(TIM3,TIM_IT_Update);  //清除中断标志位
}

