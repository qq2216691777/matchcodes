#ifndef _PID_H__
#define _PID_H__

short Position_Control_Yaw( float target_position_Yaw, float current_position_Yaw );
short Position_Control_Pitch( float target_position_Pitch, float current_position_Pitch );

short Velocity_Control_Pitch( short target_position_Pitch, float current_position_Pitch );
short Velocity_Control_Yaw( short target_velocity_Yaw, float current_velocity_Yaw );

short Motor_Yaw_Control( float Tar_Angle_Y, float Now_Angle_Y, float groy_Y );
short Motor_Pitch_Control( float Tar_Angle_VP, float Now_Angle_P, float groy_P );



void  domain_control( short FB, short LR, short DIR_YAW );
int domain_1_pid( short target_M );
int domain_2_pid( short target_M );
int domain_3_pid( short target_M );
int domain_4_pid( short target_M );

#endif

