#include "pid.h"
#include "APP.h"

#define INIT_Y_POSITION 112.65f
#define INIT_P_POSITION 96.20f

short Motor_Yaw_Control( float Tar_Angle_Y, float Now_Angle_Y, float groy_Y )
{
	int output=0;
	float Tar_V;
	
	if( Tar_Angle_Y > 60 )
	{
		Tar_Angle_Y = 60;
	}
	else if( Tar_Angle_Y < -60 )
	{
		Tar_Angle_Y = -60;
	} 
	Tar_Angle_Y += INIT_Y_POSITION;
	
	Tar_V = Position_Control_Yaw( Tar_Angle_Y, Now_Angle_Y );
	
	output = Velocity_Control_Yaw( Tar_V, -groy_Y );
	
	return output + Tar_V;
}

short Position_Control_Yaw( float target_position_Yaw, float current_position_Yaw )
{
	const float ly_p = 100.0; 
	const float ly_i = 0.0; 
    const float ly_d = 0.0; 
	
	static double I_error = 0;
	static float error_py[2] = {0.0,0.0};
	
	error_py[1] = error_py[0];
	error_py[0] =  target_position_Yaw - current_position_Yaw;
	
	I_error += error_py[0];
	if( I_error>400 )
	{
		I_error = 400;
	}
	else if( I_error<-400 )
	{
		I_error = -400;
	}
	
	return error_py[0] * ly_p + I_error * ly_i + (error_py[0] - error_py[1]) * ly_d;
	
}

short Velocity_Control_Yaw( short target_velocity_Yaw, float current_velocity_Yaw )
{
	const float ly_p = 30.0; 
	const float ly_i = 1.30; 
    const float ly_d = 20.0; 
	
	static double I_error = 0;
	static float error_py[2] = {0.0,0.0};
	
	error_py[1] = error_py[0];
	error_py[0] =  target_velocity_Yaw/100.0 - current_velocity_Yaw;
	
	I_error += error_py[0];
	if( I_error>500 )
	{
		I_error = 500;
	}
	else if( I_error<-500 )
	{
		I_error = -500;
	}
	
	return error_py[0] * ly_p + I_error * ly_i + (error_py[0] - error_py[1]) * ly_d;
	
}

short Motor_Pitch_Control( float Tar_Angle_VP, float Now_Angle_P, float groy_P )
{
	int output=0;
	float Tar_V;
	static float Tar_angl=0.0;
	
	Tar_angl += Tar_Angle_VP;
	
	if( Tar_angl > 25 )
	{
		Tar_angl = 25;
	}
	else if( Tar_angl < -25 )
	{
		Tar_angl = -25;
	} 
	Tar_Angle_VP = Tar_angl + INIT_P_POSITION;
	
	Tar_V = Position_Control_Pitch( Tar_Angle_VP, Now_Angle_P );
	
	output = Velocity_Control_Pitch( Tar_V, -groy_P );
	return output + Tar_V;
}

short Position_Control_Pitch( float target_position_Pitch, float current_position_Pitch )
{
	const float ly_p = 80.0; 
	const float ly_i = 0.000; 
    const float ly_d = 0.0; 
	
	static double I_error = 0;
	static float error_py[2] = {0.0,0.0};
	
	error_py[1] = error_py[0];
	error_py[0] =  target_position_Pitch - current_position_Pitch;
	
	I_error += error_py[0];
	if( I_error>700 )
	{
		I_error = 700;
	}
	else if( I_error<-700 )
	{
		I_error = -700;
	}
	
	return error_py[0] * ly_p + I_error * ly_i + (error_py[0] - error_py[1]) * ly_d;
	
}

short Velocity_Control_Pitch( short target_velocity_Pitch, float current_velocity_Pitch )
{
	const float ly_p = 10.0; 
	const float ly_i = 0.000; 
    const float ly_d = 0.0; 
	
	static double I_error = 0;
	static float error_py[2] = {0.0,0.0};
	
	error_py[1] = error_py[0];
	error_py[0] =  target_velocity_Pitch/100 - current_velocity_Pitch;
	
	I_error += error_py[0];
	if( I_error>700 )
	{
		I_error = 700;
	}
	else if( I_error<-700 )
	{
		I_error = -700;
	}
	
	return error_py[0] * ly_p + I_error * ly_i + (error_py[0] - error_py[1]) * ly_d;
	
}

extern int M_num1;
extern int M_num2;
extern int M_num3;
extern int M_num4;

int S_num1;
int S_num2;
int S_num3;
int S_num4;

void  domain_control(  short FB, short LR, short DIR_YAW )
{
	static int T_num[4][5];
	static char ii=0;
	short M1;
	short M2;
	short M3;
	short M4;
		
	ii = (ii+1)%5;
	T_num[0][ii] = M_num1;
	T_num[1][ii] = M_num2;
	T_num[2][ii] = M_num3;
	T_num[3][ii] = M_num4;

	S_num1 = -(T_num[0][0] + T_num[0][1] + T_num[0][2] + T_num[0][3] + T_num[0][4])/5;
	S_num2 =  (T_num[1][0] + T_num[1][1] + T_num[1][2] + T_num[1][3] + T_num[1][4])/5;
	S_num3 =  (T_num[2][0] + T_num[2][1] + T_num[2][2] + T_num[2][3] + T_num[2][4])/5;
	S_num4 = -(T_num[3][0] + T_num[3][1] + T_num[3][2] + T_num[3][3] + T_num[3][4])/5;
	
	
	M1 = FB + LR + DIR_YAW;
	M2 = FB - LR - DIR_YAW;
	M3 = FB + LR - DIR_YAW;
	M4 = FB - LR + DIR_YAW;
	
	Motor( domain_1_pid(M1),domain_2_pid(M2),domain_3_pid(M3),domain_4_pid(M4) );
}

int domain_1_pid( short target_M )
{
	const float ly_p = 30.0; 
	const float ly_i = 1.500; 
    const float ly_d = 10.0; 
	
	static long I_error = 0;
	static int error_py[2] = {0.0,0.0};
	
	error_py[1] = error_py[0];
	error_py[0] =  target_M - S_num1;
	
	I_error += error_py[0];
	if( I_error>1500 )
	{
		I_error = 1500;
	}
	else if( I_error<-1500 )
	{
		I_error = -1500;
	}
	
	return error_py[0] * ly_p + I_error * ly_i + (error_py[0] - error_py[1]) * ly_d;
	
}

int domain_2_pid( short target_M )
{
	const float ly_p = 30.0; 
	const float ly_i = 1.500; 
    const float ly_d = 10.0; 
	
	static long I_error = 0;
	static int error_py[2] = {0.0,0.0};
	
	error_py[1] = error_py[0];
	error_py[0] =  target_M - S_num2;
	
	I_error += error_py[0];
	if( I_error>1500 )
	{
		I_error = 1500;
	}
	else if( I_error<-1500 )
	{
		I_error = -1500;
	}
	
	return error_py[0] * ly_p + I_error * ly_i + (error_py[0] - error_py[1]) * ly_d;
	
}

int domain_3_pid( short target_M )
{
	const float ly_p = 30.0; 
	const float ly_i = 1.500; 
    const float ly_d = 10.0; 
	
	static long I_error = 0;
	static int error_py[2] = {0.0,0.0};
	
	error_py[1] = error_py[0];
	error_py[0] =  target_M - S_num3;
	
	I_error += error_py[0];
	if( I_error>1500 )
	{
		I_error = 1500;
	}
	else if( I_error<-1500 )
	{
		I_error = -1500;
	}
	
	return error_py[0] * ly_p + I_error * ly_i + (error_py[0] - error_py[1]) * ly_d;
	
}

int domain_4_pid( short target_M )
{
	const float ly_p = 30.0; 
	const float ly_i = 1.500; 
    const float ly_d = 10.0; 
	
	static long I_error = 0;
	static int error_py[2] = {0.0,0.0};
	
	error_py[1] = error_py[0];
	error_py[0] =  target_M - S_num4;
	
	I_error += error_py[0];
	if( I_error>1500 )
	{
		I_error = 1500;
	}
	else if( I_error<-1500 )
	{
		I_error = -1500;
	}
	
	return error_py[0] * ly_p + I_error * ly_i + (error_py[0] - error_py[1]) * ly_d;
	
}




