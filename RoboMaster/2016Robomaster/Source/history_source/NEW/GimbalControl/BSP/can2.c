#include "can2.h"
#include "led.h"
#include "main.h"


/*----CAN2_TX-----PB13----*/
/*----CAN2_RX-----PB12----*/

void CAN2_Configuration(void)
{
    CAN_InitTypeDef        can;
    CAN_FilterInitTypeDef  can_filter;
    GPIO_InitTypeDef       gpio;
    NVIC_InitTypeDef       nvic;

    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN1, ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN2, ENABLE);

    GPIO_PinAFConfig(GPIOB, GPIO_PinSource13, GPIO_AF_CAN2);
    GPIO_PinAFConfig(GPIOB, GPIO_PinSource12, GPIO_AF_CAN2); 

    gpio.GPIO_Pin = GPIO_Pin_13 | GPIO_Pin_12 ;
    gpio.GPIO_Mode = GPIO_Mode_AF;
    GPIO_Init(GPIOB, &gpio);

    nvic.NVIC_IRQChannel = CAN2_RX0_IRQn;
    nvic.NVIC_IRQChannelPreemptionPriority = 0;
    nvic.NVIC_IRQChannelSubPriority = 1;
    nvic.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&nvic);

    CAN_DeInit(CAN2);
    CAN_StructInit(&can);

    can.CAN_TTCM = DISABLE;
    can.CAN_ABOM = DISABLE;    
    can.CAN_AWUM = DISABLE;    
    can.CAN_NART = DISABLE;    
    can.CAN_RFLM = DISABLE;    
    can.CAN_TXFP = ENABLE;     
    can.CAN_Mode = CAN_Mode_Normal; 
    can.CAN_SJW  = CAN_SJW_1tq;
    can.CAN_BS1 = CAN_BS1_9tq;
    can.CAN_BS2 = CAN_BS2_4tq;
    can.CAN_Prescaler = 3;   //CAN BaudRate 42/(1+9+4)/3=1Mbps
    CAN_Init(CAN2, &can);
    
    can_filter.CAN_FilterNumber=14;
    can_filter.CAN_FilterMode=CAN_FilterMode_IdMask;
    can_filter.CAN_FilterScale=CAN_FilterScale_32bit;
    can_filter.CAN_FilterIdHigh=0x0000;
    can_filter.CAN_FilterIdLow=0x0000;
    can_filter.CAN_FilterMaskIdHigh=0x0000;
    can_filter.CAN_FilterMaskIdLow=0x0000;
    can_filter.CAN_FilterFIFOAssignment=0;//the message which pass the filter save in fifo0
    can_filter.CAN_FilterActivation=ENABLE;
    CAN_FilterInit(&can_filter);
    
    CAN_ITConfig(CAN2,CAN_IT_FMP0,ENABLE);
}

void GYRO_RST(void)
{
    CanTxMsg tx_message;
    
    tx_message.StdId = 0x404;
    tx_message.IDE = CAN_Id_Standard;
    tx_message.RTR = CAN_RTR_Data;
    tx_message.DLC = 0x08;
    
    tx_message.Data[0] = 0x00;
    tx_message.Data[1] = 0x01;
    tx_message.Data[2] = 0x02;
    tx_message.Data[3] = 0x03;
    tx_message.Data[4] = 0x04;
    tx_message.Data[5] = 0x05;
    tx_message.Data[6] = 0x06;
    tx_message.Data[7] = 0x07;
    
    CAN_Transmit(CAN2,&tx_message);
}

void Encoder_sent(float encoder_angle)
{
    CanTxMsg tx_message;
    
    tx_message.StdId = 0x601;
    tx_message.IDE = CAN_Id_Standard;
    tx_message.RTR = CAN_RTR_Data;
    tx_message.DLC = 0x08;
    
    encoder_angle = encoder_angle * 100.0F; 
    tx_message.Data[0] = (uint8_t)((int32_t)encoder_angle >>24);
    tx_message.Data[1] = (uint8_t)((int32_t)encoder_angle >>16);
    tx_message.Data[2] = (uint8_t)((int32_t)encoder_angle >>8);
    tx_message.Data[3] = (uint8_t)((int32_t)encoder_angle);
    tx_message.Data[4] = 0x00;
    tx_message.Data[5] = 0x00;
    tx_message.Data[6] = 0x00;
    tx_message.Data[7] = 0x00;
    
    CAN_Transmit(CAN2,&tx_message);
}

void Radio_Sent(const uint16_t * radio_channel)
{
    CanTxMsg tx_message;
    
    tx_message.StdId = 0x402;
    tx_message.DLC = 0x08;
    tx_message.RTR = CAN_RTR_Data;
    tx_message.IDE = CAN_Id_Standard;
    
    tx_message.Data[0] = (uint8_t)(*(radio_channel+2)>>8);
    tx_message.Data[1] = (uint8_t)(*(radio_channel+2));
    tx_message.Data[2] = (uint8_t)(*(radio_channel+6)>>8);
    tx_message.Data[3] = (uint8_t)(*(radio_channel+6));
    tx_message.Data[4] = (uint8_t)(*(radio_channel+5)>>8);
    tx_message.Data[5] = (uint8_t)(*(radio_channel+5));
    tx_message.Data[6] = (uint8_t)(*(radio_channel+7)>>8);
    tx_message.Data[7] = (uint8_t)(*(radio_channel+7));
    
    CAN_Transmit(CAN2,&tx_message);
}


int8_t gyro_ok_flag = 0;

float YAW_Angle;
float this_yaw_angle;
float last_yaw_angle;
int32_t turn_cnt = 0;
float dipan_gyro_angle = 0.0;
int32_t temp_dipan_gyro = 0;
float temp_yaw_angle;
float temp_pitch = 0;
float temp_yaw = 0;
uint8_t shooting_flag = 0;
uint8_t mode_flag=0;
uint8_t ShootFlag=0;

float Now_Yaw;
float Now_Pitch;
int M_num1;
int M_num2;
int M_num3;
int M_num4;

/*************************************************************************
                          CAN2_RX0_IRQHandler
描述：单轴陀螺仪、底盘主控CAN数据接收中断
*************************************************************************/
/*
	新款24v电机YAW 0X205 PITCH 0X206
*/
void CAN2_RX0_IRQHandler(void)
{
    CanRxMsg rx_message;
	static int last_num[4][10];
	static int8_t ii1=0;
	static int8_t ii2=0;
	static int8_t ii3=0;
	static int8_t ii4=0;
	int TT_num1;
	int TT_num2;
	int TT_num3;
	int TT_num4;
	
    if (CAN_GetITStatus(CAN2,CAN_IT_FMP0)!= RESET) 
    {
       CAN_ClearITPendingBit(CAN2, CAN_IT_FMP0);
       CAN_Receive(CAN2, CAN_FIFO0, &rx_message);
       
		if(rx_message.StdId == 0x201)
		{
			ii1 = (ii1+1)%10;
			TT_num1 = ((rx_message.Data[0]*256 + rx_message.Data[1] ) - last_num[0][ii1] );
			last_num[0][ii1]	= rx_message.Data[0]*256 + rx_message.Data[1] ;	
			if( TT_num1 > -2000 && TT_num1<2000)
				M_num1=TT_num1;
		}			
			
       
	   if(rx_message.StdId == 0x202)
		{
			ii2 = (ii2+1)%10;
			TT_num2 = ((rx_message.Data[0]*256 + rx_message.Data[1] ) - last_num[1][ii2] );
			last_num[1][ii2]	= rx_message.Data[0]*256 + rx_message.Data[1] ;	
			if( TT_num2 > -2000 && TT_num2<2000)
				M_num2=TT_num2;
		}
		
		if(rx_message.StdId == 0x203)
		{
			ii3 = (ii3+1)%10;
			TT_num3 = ((rx_message.Data[0]*256 + rx_message.Data[1] ) - last_num[2][ii3] );
			last_num[2][ii3]	= rx_message.Data[0]*256 + rx_message.Data[1] ;			
			if( TT_num3 > -2000 && TT_num3<2000)
				M_num3=TT_num3;
		}
		
		if(rx_message.StdId == 0x204)
		{
			ii4 = (ii4+1)%10;
			TT_num4 = ((rx_message.Data[0]*256 + rx_message.Data[1] ) - last_num[3][ii4] );
			last_num[3][ii4]	= rx_message.Data[0]*256 + rx_message.Data[1];	
			if( TT_num4 > -2000 && TT_num4<2000)
				M_num4=TT_num4;
		}
			
       
	   
       //Yaw
       if(rx_message.StdId == 0x205)
       { 
			
			Now_Yaw = (rx_message.Data[0]*256 + rx_message.Data[1])/8191.0*360.0;   
			
       }
      
        //Pitch
        if(rx_message.StdId == 0x206) 
        { 
			Now_Pitch = (rx_message.Data[0]*256 + rx_message.Data[1])/8191.0*360.0;   
        }  
    }
}

void CAN2_TX_IRQHandler(void) //CAN TX
{
    if (CAN_GetITStatus(CAN2,CAN_IT_TME)!= RESET) 
	{
	   CAN_ClearITPendingBit(CAN2,CAN_IT_TME);
       
    }
}
