#ifndef _MOTOR_DRIVER_H__
#define  _MOTOR_DRIVER_H__



#define KEYBOARD_forward 		 0x01  //前进
#define KEYBOARD_retreat		 0x02  //后退
#define KEYBOARD_right  		 0x04  //左转
#define KEYBOARD_left 			 0x08  //右转
#define KEYBOARD_forward_right   0x09  //右上角
#define KEYBOARD_forward_left    0x05  //左上角

#define KEYBOARD_retreat_right   0x0a  //右下角
#define KEYBOARD_retreat_left    0x06  //左下角

#define KEYBOARD_left_forward    0x41  //左上旋转前进
#define KEYBOARD_right_forward   0x81  //右上旋转前进

#define KEYBOARD_Q				 0x40
#define KEYBOARD_E				 0x80
#define KEYBOARD_SHIFT           0x10

#define KEYBOARD_SHIFT_W         0x11
#define KEYBOARD_SHIFT_S         0x12
#define KEYBOARD_SHIFT_A         0x14
#define KEYBOARD_SHIFT_D         0x18

#define KEYBOARD_ctrl            0x20

void DRIVER_send_DATA(unsigned short RC_num1,unsigned short RC_num2,unsigned short RC_num3,unsigned short RC_num4);
void keyboard_control_function( RC_Ctl_t RC_Ctl_temp );
void robomasters_ctol(unsigned short RC_ch0,unsigned short RC_ch2,unsigned short RC_ch3);

#endif
