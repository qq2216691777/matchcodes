#include "main.h"

//改进方向：加速度滤波，在四元数中增加加速度的校正比重
//遥控器指令平滑处理

char id[3];
unsigned char USART_BUF[24] = {0};

extern float Now_Yaw;
extern float Now_Pitch;

extern short DIR_FB;
extern short DIR_LR;
extern short DIR_YAW;
extern float DIR_PITCH;

int main(void)
{
    int i = 0;
    BSP_Init();
	delay_ms(500); 
	Gimbal(0,0);
	domain_control( 0,0,0 );
	LED2_ON();
    delay_ms(100);  		//delay 500ms， 等待mpu6050上电稳定  
	LED2_OFF();
	
    while(MPU6050_Initialization() == 0xff) 
    {
        i++;     //如果一次初始化没有成功，那就再来一次                     
        if(i>10) //如果初始化一直不成功，那就没希望了，进入死循环，蜂鸣器一直叫
        {
            while(1) 
            {
                LED1_TOGGLE();
                delay_ms(50);
                
            }
        }  
    }    
    MPU6050_Gyro_calibration();
	
    MPU6050_Interrupt_Configuration(); 

	
    LED2_ON();
    delay_ms(100);  	
	LED2_OFF();
	
	TIM3_Int_Init( 1000,839);	//10ms 一次中断 100 839
	delay_ms(100); 
	USART1_Configuration();          //usart1 and DMA Configuration init.  遥控器
	
	
    while(1)
    {
		
//		printf("%f\r\n",Now_Pitch);
////		printf("%d  %d  %d  %d\r\n",S_num1,S_num2,S_num3,S_num4);
//		delay_ms(100);  
    }
}


