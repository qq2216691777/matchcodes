#ifndef __USART3_H__
#define __USART3_H__

#include <stm32f4xx.h>
#include <stdio.h>

void USART3_Configuration(void);
void USART3_SendChar(unsigned char b);
void RS232_Print( USART_TypeDef*, u8* );
void RS232_VisualScope( USART_TypeDef*, u8*, u16 );
#endif
