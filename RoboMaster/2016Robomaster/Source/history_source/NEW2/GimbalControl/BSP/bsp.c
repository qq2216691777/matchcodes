#include "bsp.h"

#include "global_define.h"

void BSP_Init(void){
    
    /* Configure the NVIC Preemption Priority Bits */
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);   
    LED_Configuration();            //指示灯初始化
    RELAY_Configuration();	    	//cyq:继电器初始化
    LASER_Configuration();
    CAN1_Configuration();            //初始化CAN
    USART3_Configuration();          //串口1初始化
	
	CAN2_Configuration(); 
	
	PWM_Configuration();        
    //设定占空比为1000，初始化摩擦轮电调
    PWM1 = 1000;
    PWM2 = 1;   //拨轮电机
	
//	
//	 //初始化送弹电机驱动
//    Motor_Reset(MOTOR_NUM1);  
//	Motor_sw();	
//    delay_ms(30);//延时        
//    Motor_Init(MOTOR_NUM1,PWM_MODE);	
//	delay_ms(30);//延时    
//	delay_ms(30);//延时    
//	Motor_PWM_Set( MOTOR_NUM1,1000 );
//	
//	GPIO_ResetBits(GPIOA,GPIO_Pin_2);
	
}


