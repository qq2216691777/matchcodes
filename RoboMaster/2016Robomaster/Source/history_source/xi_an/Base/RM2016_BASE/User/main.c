#include "main.h"
#include "interface.h"
#include <math.h>

uint32_t Upload_Speed = 1;   //数据上传速度  单位 Hz
#define upload_time (1000000/Upload_Speed)  //计算上传的时间。单位为us
uint16_t  power1=0,power2=0;
uint32_t system_micrsecond;   //系统时间 单位ms

Base_Inter *Inter;
short  pixel_X=0;
short  pixel_Y=0;
short  Temp_pixel_Y[3]=0;

unsigned char Remote111 = 0;
#define PIXEL_To_ANGLE_P	20.0 		// arctan(20)=0.36397  
#define PIXEL_To_ANGLE_Y	60.0		// arctan(60)=1.7321 

int main(void)
{     		
	const static float PIXEL_Buttom_P = 0.36397*360;
	const static float PIXEL_Buttom_Y = 1.7321*640;
	static int delay_time=0;
	unsigned char Temp_pixel_YI=0;
	
	ControtLoopTaskInit();   //app init
	RemoteTaskInit();
	delay_ms(800);     
	BSP_Init();	
	system_micrsecond = Get_Time_Micros();	
//	SetShootState(SHOOTING);
//	SetFrictionWheelSpeed(1300);
////	SetFrictionWheelSpeed(1700);
	//SetShootState(SHOOTING);
	Remote111 = 1;
	GimbalRef.pitch_angle_dynamic_ref = 0;
	while(1)
	{   
		IMU_getYawPitchRoll(angle);
		
#if UART3_DEBUG
		if((Get_Time_Micros() - system_micrsecond) > upload_time)
		{
			system_micrsecond = Get_Time_Micros();
			UploadParameter();   //upload data to the PC			
			delay_ms(1);
		}
#else
	if( Remote111 == 1)
	{
		if( USART_RX_STA & 0x8000 )
		{
			USART_RX_BUF[USART_RX_STA&0xfff] = 0;
			
			Inter = (Base_Inter *)USART_RX_BUF;
			if( Inter->sum == (uint8)(Inter->Flag + Inter->x + Inter->y) )
			{
				SetFrictionWheelSpeed(1300);
				pixel_X = ((short)Inter->x);
				pixel_Y = ((short)Inter->y)+20;
				
				Temp_pixel_YI = (Temp_pixel_YI+1)%3;
				Temp_pixel_Y[Temp_pixel_YI] = pixel_Y;
				pixel_Y = (Temp_pixel_Y[0]+Temp_pixel_Y[1]+Temp_pixel_Y[2])/3;
				
				if( Inter->Flag == 0x81)
					SetShootState(SHOOTING);
				else
					SetShootState(NOSHOOTING);				
				GimbalRef.yaw_angle_dynamic_ref = atan2( pixel_X,PIXEL_Buttom_Y )*52.0;
				GimbalRef.pitch_angle_dynamic_ref = atan2( pixel_Y,PIXEL_Buttom_P )*4.70;
			}
			USART_RX_STA= 0;
			delay_time=0;
		}
		else
		{
			delay_time++;
			if( delay_time>20 )
			{
				SetShootState(NOSHOOTING);	
				delay_time=20;
			}
		}
	}
#endif
		
    }
}
