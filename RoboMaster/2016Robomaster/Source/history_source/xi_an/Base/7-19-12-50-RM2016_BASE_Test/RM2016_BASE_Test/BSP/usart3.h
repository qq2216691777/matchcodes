#ifndef __USART3_H__
#define __USART3_H__
#include "main.h"
void USART3_Configuration(void);

#define USART_REC_LEN  			200  	//定义最大接收字节数 200
extern u16 USART_RX_STA;       //接收状态标记
extern u8 USART_RX_BUF[USART_REC_LEN];     //接收缓冲,最大USART_REC_LEN个字节.
#endif
