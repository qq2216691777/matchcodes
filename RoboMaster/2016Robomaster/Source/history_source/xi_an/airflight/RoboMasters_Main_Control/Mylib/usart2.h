#ifndef __USART2_H__
#define __USART2_H__
#include "stm32f4xx.h"


#define keyboard_forward 		 0x01  //前进
#define keyboard_retreat		 0x02  //后退
#define keyboard_right  		 0x04  //左转
#define keyboard_left 			 0x08  //右转
#define keyboard_forward_right   0x09  //右上角
#define keyboard_forward_left    0x05  //左上角

#define keyboard_retreat_right   0x0a  //右下角
#define keyboard_retreat_left    0x06  //左下角

#define keyboard_left_forward    0x41  //左上旋转前进
#define keyboard_right_forward   0x81  //右上旋转前进

#define keyboard_Q				 0x40
#define keyboard_E				 0x80
#define keyboard_shift           0x10

#define keyboard_shift_W         0x11
#define keyboard_shift_S         0x12
#define keyboard_shift_A         0x14
#define keyboard_shift_D         0x18

#define keyboard_ctrl            0x20




void USART2_Configuration(void);

/*************2.4G无线传输协议************/ 
typedef struct 
{ 
    struct 
    { 
        uint16_t ch0; 
        uint16_t ch1; 
        uint16_t ch2; 
        uint16_t ch3; 
        uint8_t  s1; 
        uint8_t  s2; 
    }rc; 
 
    struct 
    { 
        int16_t x; 
        int16_t y; 
        int16_t z; 
        uint8_t press_l; 
        uint8_t press_r; 
    }mouse; 
 
    struct 
    { 
        uint16_t v; 
    }key; 
}RC_Ctl_t;  




extern RC_Ctl_t RC_Ctl; 
void robomasters_ctol(unsigned short RC_ch0,unsigned short RC_ch2,unsigned short RC_ch3);
void keyboard_control_function(void);

void RC_Init(void) ;


#endif
