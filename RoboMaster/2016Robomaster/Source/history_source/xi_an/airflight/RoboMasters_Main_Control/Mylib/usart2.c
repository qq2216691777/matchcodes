#include "main.h"


/*-----USART2_RX-----PA3----*/ 
//for D-BUS

volatile unsigned char sbus_rx_buffer[18]; //volatile 
 RC_Ctl_t RC_Ctl; 

void USART2_Configuration(void)
{
    USART_InitTypeDef usart2;
	GPIO_InitTypeDef  gpio;
    NVIC_InitTypeDef  nvic;
    DMA_InitTypeDef   dma;
	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA | RCC_AHB1Periph_DMA1,ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2,ENABLE);
	
	GPIO_PinAFConfig(GPIOA,GPIO_PinSource3 ,GPIO_AF_USART2);
	
	gpio.GPIO_Pin = GPIO_Pin_3 ;
	gpio.GPIO_Mode = GPIO_Mode_AF;
    gpio.GPIO_OType = GPIO_OType_PP;
    gpio.GPIO_Speed = GPIO_Speed_100MHz;
    gpio.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOA,&gpio);
    
    USART_DeInit(USART2);
	usart2.USART_BaudRate = 100000;   //SBUS 100K baudrate
	usart2.USART_WordLength = USART_WordLength_8b;
	usart2.USART_StopBits = USART_StopBits_1;
	usart2.USART_Parity = USART_Parity_Even;
	usart2.USART_Mode = USART_Mode_Rx;
    usart2.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_Init(USART2,&usart2);
    
	USART_Cmd(USART2,ENABLE);
    USART_DMACmd(USART2,USART_DMAReq_Rx,ENABLE);
    
    nvic.NVIC_IRQChannel = DMA1_Stream5_IRQn;
    nvic.NVIC_IRQChannelPreemptionPriority = 1;
    nvic.NVIC_IRQChannelSubPriority = 1;
    nvic.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&nvic);
    
    DMA_DeInit(DMA1_Stream5);
    dma.DMA_Channel= DMA_Channel_4;
    dma.DMA_PeripheralBaseAddr = (uint32_t)&(USART2->DR);
    dma.DMA_Memory0BaseAddr = (uint32_t)sbus_rx_buffer;
    dma.DMA_DIR = DMA_DIR_PeripheralToMemory;
    dma.DMA_BufferSize = 18;
    dma.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    dma.DMA_MemoryInc = DMA_MemoryInc_Enable;
    dma.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
    dma.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
    dma.DMA_Mode = DMA_Mode_Circular;
    dma.DMA_Priority = DMA_Priority_VeryHigh;
    dma.DMA_FIFOMode = DMA_FIFOMode_Disable;
    dma.DMA_FIFOThreshold = DMA_FIFOThreshold_1QuarterFull;
    dma.DMA_MemoryBurst = DMA_Mode_Normal;
    dma.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
    DMA_Init(DMA1_Stream5,&dma);

    DMA_ITConfig(DMA1_Stream5,DMA_IT_TC,ENABLE);
    DMA_Cmd(DMA1_Stream5,ENABLE);
}
/********************************************************************
                         遥控器控制函数
********************************************************************/
void robomasters_ctol(unsigned short RC_ch0,unsigned short RC_ch2,unsigned short RC_ch3)
{
	if(RC_ch0 > 1644) 
	{	
		DRIVER_send_DATA(1025+40,1025+40,1023-40,1023-40);
		return;
	}  //右旋
	
	if(RC_ch0 < 404) 
	{
		DRIVER_send_DATA(1023-40,1023-40,1025+40,1025+40);
		return;
	}
	
	if(RC_ch2 <= 1074 && RC_ch2 >=974 && RC_ch3 <= 1074 && RC_ch3 >=974) //死区
	{
		DRIVER_send_DATA(1024,1024,1024,1024);
		return;
	}  
	
	if(RC_ch3 > 1074 )  //前进
	{
		DRIVER_send_DATA(1025+40,1025+40,1025+40,1025+40);
		return;
	}

	if( RC_ch3 < 974)  //后退
	{
		DRIVER_send_DATA(1023-40,1023-40,1023-40,1023-40);
		return;
	}

	if(RC_ch2 > 1074) //右移动
	{
		DRIVER_send_DATA(1025+40,1023-40,1023-40,1025+40);
		return;
	}

	if(RC_ch2 < 974) //左移
	{
		DRIVER_send_DATA(1023-40,1025+40,1025+40,1023-40);
		return;
	}
}
static unsigned char last_mouse_output = 0;
unsigned char mouse_MAX_VAL(unsigned char mouse_val_now)
{
	unsigned char MAX_MOUSE_VALUE = 0;
	if(last_mouse_output < mouse_val_now)
	{
		last_mouse_output = mouse_val_now;
		MAX_MOUSE_VALUE = 0;
	}
	if(last_mouse_output > mouse_val_now)
	{
		MAX_MOUSE_VALUE = last_mouse_output;
	}
	
	return MAX_MOUSE_VALUE;
}

/********************************************************************
                       keyboard cotorl function
********************************************************************/
static unsigned char keyboard_val = 0;
unsigned char CAN_send = 0;
#define zheng_SPEED 1024+50
#define   fan_SPEED 1024-50
#define stop_SPEED  1024
#define MIX_keyboard_value1 1024+90
#define MIX_keyboard_value2 1024-90



void keyboard_control_function()
{
	unsigned short SEND_SPEED_DATA[4] = {0};

	if(keyboard_val == 1)
		{
			keyboard_val = 0;
		}
	switch(RC_Ctl.key.v)      //unsigned short --> unsigned char
		{
			case 0x00:
				
				SEND_SPEED_DATA[0]  =  stop_SPEED;
				SEND_SPEED_DATA[1]  =  stop_SPEED;
				SEND_SPEED_DATA[2]  =  stop_SPEED;
				SEND_SPEED_DATA[3]  =  stop_SPEED;
				break;
			case  keyboard_forward:	//前进   1,2,3,4 zheng
			
				SEND_SPEED_DATA[0]  =  zheng_SPEED;
				SEND_SPEED_DATA[1]  =  zheng_SPEED;
				SEND_SPEED_DATA[2]  =  zheng_SPEED;
				SEND_SPEED_DATA[3]  =  zheng_SPEED;
				break;
			case  keyboard_retreat:		//后退   1,2,3,4 fan
			
				SEND_SPEED_DATA[0]  =  fan_SPEED;
				SEND_SPEED_DATA[1]  =  fan_SPEED;
				SEND_SPEED_DATA[2]  =  fan_SPEED;
				SEND_SPEED_DATA[3]  =  fan_SPEED;
				break;
			case  keyboard_right:	//右平移 1,4 zheng  2,3 fan
			
				SEND_SPEED_DATA[0]  =  fan_SPEED;
				SEND_SPEED_DATA[1]  =  zheng_SPEED;
				SEND_SPEED_DATA[2]  =  zheng_SPEED;
				SEND_SPEED_DATA[3]  =  fan_SPEED;
				break;
			case  keyboard_left:	//左平移 2,3zheng 1,4 fan

				SEND_SPEED_DATA[0]  =  zheng_SPEED;
				SEND_SPEED_DATA[1]  =  fan_SPEED;
				SEND_SPEED_DATA[2]  =  fan_SPEED;
				SEND_SPEED_DATA[3]  =  zheng_SPEED;
				break;
			case  keyboard_forward_right:   //右前  1,4 zheng
				keyboard_val = 1;	
			
				SEND_SPEED_DATA[0]  =  zheng_SPEED;
				SEND_SPEED_DATA[1]  =  stop_SPEED;
				SEND_SPEED_DATA[2]  =  stop_SPEED;
				SEND_SPEED_DATA[3]  =  zheng_SPEED;
				break;
			
			case keyboard_retreat_left:   
				
				SEND_SPEED_DATA[0]  =  fan_SPEED;
				SEND_SPEED_DATA[1]  =  stop_SPEED;
				SEND_SPEED_DATA[2]  =  stop_SPEED;
				SEND_SPEED_DATA[3]  =  fan_SPEED;
				break;
			
			case  keyboard_forward_left:   //左前  2,3 zheng
				keyboard_val = 1;	
			
				SEND_SPEED_DATA[0]  =  stop_SPEED;
				SEND_SPEED_DATA[1]  =  zheng_SPEED;
				SEND_SPEED_DATA[2]  =  zheng_SPEED;
				SEND_SPEED_DATA[3]  =  stop_SPEED;
				break;
			
			case keyboard_retreat_right :
				
				SEND_SPEED_DATA[0]  =  stop_SPEED;
				SEND_SPEED_DATA[1]  =  fan_SPEED;
				SEND_SPEED_DATA[2]  =  fan_SPEED;
				SEND_SPEED_DATA[3]  =  stop_SPEED;
				break;
				
			case  keyboard_Q:    //左旋转  2.4 zheng 1,3 fan
				
				SEND_SPEED_DATA[0]  =  fan_SPEED;
				SEND_SPEED_DATA[1]  =  fan_SPEED;
				SEND_SPEED_DATA[2]  =  zheng_SPEED;
				SEND_SPEED_DATA[3]  =  zheng_SPEED;
				break;
			
			case  keyboard_left_forward:   //左上旋转前进
				
				SEND_SPEED_DATA[0]  =  stop_SPEED;
				SEND_SPEED_DATA[1]  =  stop_SPEED;
				SEND_SPEED_DATA[2]  =  zheng_SPEED;
				SEND_SPEED_DATA[3]  =  zheng_SPEED;
				break;
			
			case  keyboard_E:   //右旋转 1,3 zheng 2,4 fan
				
				SEND_SPEED_DATA[0]  =  zheng_SPEED;
				SEND_SPEED_DATA[1]  =  zheng_SPEED;
				SEND_SPEED_DATA[2]  =  fan_SPEED;
				SEND_SPEED_DATA[3]  =  fan_SPEED;
				break;
			
			case  keyboard_right_forward:               //右上旋转前进
				
				SEND_SPEED_DATA[0]  =  zheng_SPEED;
				SEND_SPEED_DATA[1]  =  zheng_SPEED;
				SEND_SPEED_DATA[2]  =  stop_SPEED;
				SEND_SPEED_DATA[3]  =  stop_SPEED;
				break;
			
			case keyboard_shift_W:
				SEND_SPEED_DATA[0]  =  MIX_keyboard_value1;
				SEND_SPEED_DATA[1]  =  MIX_keyboard_value1;
				SEND_SPEED_DATA[2]  =  MIX_keyboard_value1;
				SEND_SPEED_DATA[3]  =  MIX_keyboard_value1;
			    break;
			
			case keyboard_shift_S:
				SEND_SPEED_DATA[0]  =  MIX_keyboard_value2;
				SEND_SPEED_DATA[1]  =  MIX_keyboard_value2;
				SEND_SPEED_DATA[2]  =  MIX_keyboard_value2;
				SEND_SPEED_DATA[3]  =  MIX_keyboard_value2;
			    break;
			
			case keyboard_shift_A:
				SEND_SPEED_DATA[0]  =  MIX_keyboard_value2;
				SEND_SPEED_DATA[1]  =  MIX_keyboard_value1;
				SEND_SPEED_DATA[2]  =  MIX_keyboard_value1;
				SEND_SPEED_DATA[3]  =  MIX_keyboard_value2;
			    break;
			
			case keyboard_shift_D:
				SEND_SPEED_DATA[0]  =  MIX_keyboard_value1;
				SEND_SPEED_DATA[1]  =  MIX_keyboard_value2;
				SEND_SPEED_DATA[2]  =  MIX_keyboard_value2;
				SEND_SPEED_DATA[3]  =  MIX_keyboard_value1;
			    break;
			
			default :
				SEND_SPEED_DATA[0]  =  stop_SPEED;
				SEND_SPEED_DATA[1]  =  stop_SPEED;
				SEND_SPEED_DATA[2]  =  stop_SPEED;
				SEND_SPEED_DATA[3]  =  stop_SPEED;
			break;
		}	
		if(RC_Ctl.mouse.x != 0)
		{
			SEND_SPEED_DATA[0]  +=  RC_Ctl.mouse.x*1.2;
			SEND_SPEED_DATA[1]  +=  RC_Ctl.mouse.x*1.2;
			SEND_SPEED_DATA[2]  -=  RC_Ctl.mouse.x*1.2;
			SEND_SPEED_DATA[3]  -=  RC_Ctl.mouse.x*1.2;
		}
		
		DRIVER_send_DATA(SEND_SPEED_DATA[0],SEND_SPEED_DATA[1],SEND_SPEED_DATA[2],SEND_SPEED_DATA[3]);

			SEND_SPEED_DATA[0]  = 0;
			SEND_SPEED_DATA[1]  = 0;
			SEND_SPEED_DATA[2]  = 0;
			SEND_SPEED_DATA[3]  = 0;
}


void DMA1_Stream5_IRQHandler(void)
{
    if(DMA_GetITStatus(DMA1_Stream5, DMA_IT_TCIF5))
    {
        DMA_ClearFlag(DMA1_Stream5, DMA_FLAG_TCIF5);
        DMA_ClearITPendingBit(DMA1_Stream5, DMA_IT_TCIF5);
		
		RC_Ctl.rc.ch0 = (sbus_rx_buffer[0]| (sbus_rx_buffer[1] << 8)) & 0x07ff;           //!< Channel 0 
        RC_Ctl.rc.ch1 = ((sbus_rx_buffer[1] >> 3) | (sbus_rx_buffer[2] << 5)) & 0x07ff;   //!< Channel 1 
        RC_Ctl.rc.ch2 = ((sbus_rx_buffer[2] >> 6) | (sbus_rx_buffer[3] << 2) |            //!< Channel 2 
                         (sbus_rx_buffer[4] << 10)) & 0x07ff;  
        RC_Ctl.rc.ch3 = ((sbus_rx_buffer[4] >> 1) | (sbus_rx_buffer[5] << 7)) & 0x07ff;   //!< Channel 3 
        RC_Ctl.rc.s1  = ((sbus_rx_buffer[5] >> 4)& 0x000C) >> 2;                          //!< Switch left 
        RC_Ctl.rc.s2  = ((sbus_rx_buffer[5] >> 4)& 0x0003);                               //!< Switch right 
//        RC_Ctl.mouse.x = sbus_rx_buffer[6] | (sbus_rx_buffer[7] << 8);                    //!< Mouse X axis 
//        RC_Ctl.mouse.y = sbus_rx_buffer[8] | (sbus_rx_buffer[9] << 8);                    //!< Mouse Y axis 
//        RC_Ctl.mouse.z = sbus_rx_buffer[10] | (sbus_rx_buffer[11] << 8);                  //!< Mouse Z axis 
//        RC_Ctl.mouse.press_l = sbus_rx_buffer[12];                                        //!< Mouse Left Is Press ? 
//        RC_Ctl.mouse.press_r = sbus_rx_buffer[13];                                        //!< Mouse Right Is Press ? 
// 
//        RC_Ctl.key.v = sbus_rx_buffer[14] | (sbus_rx_buffer[15] << 8);                    //!< KeyBoard value 
		
		PWM_CH1 = ((RC_Ctl.rc.ch0-364)*1000/1320+1000);
		PWM_CH2 = 3000 - ((RC_Ctl.rc.ch1-364)*1000/1320+1000);
		PWM_CH3 = ((RC_Ctl.rc.ch3-364)*1000/1320+1000);
		PWM_CH4 = ((RC_Ctl.rc.ch2-364)*1000/1320+1000);
		if(RC_Ctl.rc.s1 == 3 )
		{
			PWM_CH6 = 1000;
		}
		else if( RC_Ctl.rc.s1 == 1 )
		{
			PWM_CH6 = 2000;
		}else if(RC_Ctl.rc.s1 == 2 )
		{
			PWM_CH6 = 1000;
		}
		
		if(RC_Ctl.rc.s2 == 3 )
		{
			PWM_CH7 = 1870;
			PWM_CH8 = 760;
		}
		else if( RC_Ctl.rc.s2 == 1 )
		{
			PWM_CH7 = 1000;
			PWM_CH8 = 760;
		}else if( RC_Ctl.rc.s2 == 2 )
		{
			
			PWM_CH7 = 1870;
			PWM_CH8 = 1550;
		}
		
		
//		if(RC_Ctl.rc.s2 == 1)  //2:遥控器
//		{
//			PWM1 = RC_Ctl.rc.ch2;
//			PWM2 = RC_Ctl.rc.ch2;
//			PWM3 = RC_Ctl.rc.ch2;
//			PWM4 = RC_Ctl.rc.ch2;
//			PWM5 = RC_Ctl.rc.ch2;
//			PWM6 = RC_Ctl.rc.ch2;
//			PWM7 = RC_Ctl.rc.ch2;
//			PWM8 = RC_Ctl.rc.ch2;
//			
////			YUNTAI_send_DATA(RC_Ctl.rc.ch0,RC_Ctl.rc.ch1,RC_Ctl.rc.s1,1,0);  //send yuntai data 使用的是遥控器
////			robomasters_ctol(RC_Ctl.rc.ch0,RC_Ctl.rc.ch2,RC_Ctl.rc.ch3);   //使用的是遥控器
//		}
//		 if(RC_Ctl.rc.s2 == 2)	//键盘
//			{
////				YUNTAI_send_DATA(RC_Ctl.mouse.x+1024,RC_Ctl.mouse.y+1024,RC_Ctl.mouse.press_l,2, RC_Ctl.mouse.press_r);  //send yuntai data 使用的是键盘
////				keyboard_control_function();   		//使用的是键盘
//			}
		
    }
	
}


