#include "main.h"

void BSP_Init(void)
{
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);     
	PWM_Configuration();
	Led_Configuration();  
	USART3_Configuration(); 	
	CAN1_Configuration(); 
	CAN2_Configuration();         
}

