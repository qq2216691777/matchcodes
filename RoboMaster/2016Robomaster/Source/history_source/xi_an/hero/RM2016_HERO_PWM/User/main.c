#include "main.h"
uint32_t Upload_Speed = 1;   //数据上传速度  单位 Hz
#define upload_time (1000000/Upload_Speed)  //计算上传的时间。单位为us
uint16_t  power1=0,power2=0;
uint32_t system_micrsecond;   //系统时间 单位ms

uint16_t PWM1_Value=1000;
uint16_t PWM2_Value=1000;
int main(void)
{     			
   
	BSP_Init();	
	PWM1 = 1000;
	PWM2 = 1650;
	delay_ms(500);  	
	GREEN_LED_OFF();
    RED_LED_ON();
	while(1)
	{   
		if( PWM1_Value>1250)
		{
			PWM2 = 850;
		}
		else
		{
			PWM2 = 1800;
		}
		PWM1 = PWM1_Value;
		delay_ms(50);
    }
}
