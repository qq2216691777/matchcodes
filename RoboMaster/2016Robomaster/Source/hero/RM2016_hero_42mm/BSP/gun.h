#ifndef __GUN_H__
#define __GUN_H__

#ifndef FRICTION_WHEEL
#define FRICTION_WHEEL
#endif 

#if defined(FRICTION_WHEEL)

#define PWM1  TIM5->CCR1		//P9 ���
#define PWM2  TIM5->CCR2		//P8
#define PWM3  TIM9->CCR1

#define InitFrictionWheel()     \
        PWM2 = 1000;             
#define SetFrictionWheelSpeed(x) \
        PWM2 = x;                

#endif 

void PWM_Configuration(void);
 
#endif /* __GUN_H__*/

