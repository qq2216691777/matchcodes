#include "stm32f10x.h"
#include "LED.h"
#include "servo.h"
#include "Delay.h"
#include "step_motor.h"
#include "key.h"
#include "can.h"

//Fpclk1的时钟在初始化的时候设置为36M,如果设置CAN_Normal_Init(1,8,7,5,1);
//则波特率为:36M/((1+8+7)*5)=450Kbps

extern int capture_num;
extern u8 can_flag;
uint32_t *BUFId;
uint8_t DATA[8];
int main(void)
{	
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);	//设置NVIC中断分组2:2位抢占优先级，2位响应优先级
	LED_GPIO_Init();      //指示灯初始化
	CAN_Configuration();
	TIM2_init(2000,719);   //定时器2初始化周期20ms
	Cleaner_init();   //吸弹涵道初始化
	Init_key();
	Step_motor_init(100,719);   //步进电机复位    达到最上边
	
	
//	Cleaner_work();
//	Delay_ms(5000); 
	Cleaner_no_work();
	while (1)
	{
		//DATA[0] = 11;
	
		
		if(can_flag == 1)   //吸弹
		{
			
		//	GPIO_ResetBits(GPIOB,GPIO_Pin_12);
			Cleaner_work();
			while(can_flag == 1);
			Cleaner_no_work();
		//	GPIO_SetBits(GPIOB,GPIO_Pin_12);
		}
	}
}



/*********************************************END OF FILE**********************/
