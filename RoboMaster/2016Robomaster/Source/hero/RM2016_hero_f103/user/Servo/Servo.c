#include "servo.h"
#include "led.h"
#include "Delay.h"


static void TIM2_GPIO_init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;

 	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE); 		//使能定时器2
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE); 
	GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3;		
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;		    // 复用推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA , &GPIO_InitStructure);	
}


static void TIM2_PWM_config(int arr,int psc)
{
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_OCInitTypeDef  TIM_OCInitStructure;
	//NVIC_InitTypeDef NVIC_InitStructure;
 
	TIM_TimeBaseStructure.TIM_Period =arr;       //当定时器从0计数到1999，即为1000次，为一个定时周期
	TIM_TimeBaseStructure.TIM_Prescaler =psc;	    //设置预分频：不预分频，即为72MHz
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1 ;	
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  //向上计数模式			
	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);

	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
	TIM_OC1Init(TIM2, &TIM_OCInitStructure);	      //使能通道1
	TIM_OC1PreloadConfig(TIM2, TIM_OCPreload_Enable);
	
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
	TIM_OC2Init(TIM2, &TIM_OCInitStructure);	      //使能通道2
	TIM_OC2PreloadConfig(TIM2, TIM_OCPreload_Enable);
	
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
	TIM_OC3Init(TIM2, &TIM_OCInitStructure);	      //使能通道3
	TIM_OC3PreloadConfig(TIM2, TIM_OCPreload_Enable);
	
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
	TIM_OC4Init(TIM2, &TIM_OCInitStructure);	//使能通道4
	TIM_OC4PreloadConfig(TIM2, TIM_OCPreload_Enable);
	
	TIM_ARRPreloadConfig(TIM2, ENABLE);			 // 使能TIM3重载寄存器ARR
	
//	TIM_ITConfig(TIM2,TIM_IT_Update,ENABLE ); //使能指定的TIM2中断,允许更新中断
//	NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;  //TIM2中断
//	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;  //先占优先级0级
//	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;  //从优先级3级
//	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE; //IRQ通道被使能
//	NVIC_Init(&NVIC_InitStructure);  //根据NVIC_InitStruct中指定的参数初始化外设NVIC寄存器
	
	TIM_Cmd(TIM2, ENABLE);   
}

void TIM2_init(int arr_tim2,int psc_tim2)
{
	TIM2_GPIO_init();
	TIM2_PWM_config(arr_tim2,psc_tim2);
}

void Servo_angle(char channel,float angle)
{
	switch (channel)
	{
		case 1:
			TIM_SetCompare1(TIM2,angle); break;   //用于控制吸弹舵机
		case 2:
			TIM_SetCompare2(TIM2,angle); break;
		case 3:
			TIM_SetCompare3(TIM2,angle); break;
		case 4:
			TIM_SetCompare4(TIM2,angle); break;
		default:   break;
	}
}

void Cleaner_init()
{
		Servo_angle(3,100);   //涵道初始化脉冲
		Delay_ms(3000);     //初始化信号延时
	  Servo_angle(1,90);    //关闭下弹舵机口
}

void  Cleaner_work()
{
	GPIO_SetBits(GPIOA,GPIO_Pin_7);  //控制步进电机运动方向  丝杠向下
	TIM_SetCompare1(TIM3,80);          //步进电机脉冲
	while(GPIO_ReadInputDataBit(GPIOB,GPIO_Pin_7)==1);
	TIM_SetCompare1(TIM3,0); 
	Servo_angle(1,205);      //关闭舵机口
	Servo_angle(3,200);   //涵道工作脉冲
}


void  Cleaner_no_work()
{
	Servo_angle(3,100);   //涵道工作脉冲
	Servo_angle(1,90);      //打开舵机口
	
	GPIO_ResetBits(GPIOA,GPIO_Pin_7);  //控制步进电机运动方向  丝杠向上
	TIM_SetCompare1(TIM3,80);          //步进电机脉冲
	while(GPIO_ReadInputDataBit(GPIOB,GPIO_Pin_6)==1);
	TIM_SetCompare1(TIM3,0); 
}














