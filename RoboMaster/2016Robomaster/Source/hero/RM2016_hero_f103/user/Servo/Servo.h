
#ifndef  _SERVO_H
#define  _SERVO_H

#include "stm32f10x.h"

void TIM2_init(int arr_tim2,int psc_tim2);
void Servo_angle(char channel,float angle);
void Cleaner_init(void);

void  Cleaner_work(void);   //涵道开始工作函数
void  Cleaner_no_work(void);	  //涵道停止工作函数
#endif



