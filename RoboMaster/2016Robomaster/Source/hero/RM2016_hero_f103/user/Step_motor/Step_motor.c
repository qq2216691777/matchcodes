#include "step_motor.h"

int capture_num = 0;
static void TIM3_GPIO_init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;

 	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE); 		//使能定时器3
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE); 
	GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_6 | GPIO_Pin_7;		
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;		    // 复用推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA , &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_7;		
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;		    // 复用推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA , &GPIO_InitStructure);
}

static void TIM3_PWM_config(int arr,int psc)
{
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_OCInitTypeDef  TIM_OCInitStructure;
	
	TIM_TimeBaseStructure.TIM_Period =arr;      //定时周期
	TIM_TimeBaseStructure.TIM_Prescaler =psc;	    //设置预分频：不预分频，即为72MHz
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1 ;	
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  //向上计数模式			
	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);
	
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
	TIM_OC1Init(TIM3, &TIM_OCInitStructure);	      //使能通道1
	TIM_OC1PreloadConfig(TIM3, TIM_OCPreload_Enable);
	TIM_Cmd(TIM3, ENABLE); 
}

void Step_motor_init(int arr_tim3,int psc_tim3)
{
	TIM3_GPIO_init();
	TIM3_PWM_config(arr_tim3,psc_tim3);
	GPIO_ResetBits(GPIOA,GPIO_Pin_7);  //控制步进电机运动方向  丝杠向上
	TIM_SetCompare1(TIM3,80);          //步进电机脉冲
	while(GPIO_ReadInputDataBit(GPIOB,GPIO_Pin_6)==1);
	TIM_SetCompare1(TIM3,0); 
}











