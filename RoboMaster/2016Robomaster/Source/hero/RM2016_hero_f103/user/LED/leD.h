#ifndef __LED_H
#define __LED_H

#define ON  1
#define OFF 0

#define digitalToggle(p,i) {p->ODR^=i;}

#define LED_Toggle digitalToggle(GPIOB,GPIO_Pin_12)

#define LED(a) if(a)  GPIO_SetBits(GPIOB,GPIO_Pin_12);\
				else		GPIO_ResetBits(GPIOB,GPIO_Pin_12)

void LED_GPIO_Init(void);
#endif
				
				
				
