#include "stm32f10x.h"
#include "servo.h"
#include "Delay.h"
//HVD232---CAN_TX---PA12
//HVD232---CAN_RX---PA11


void CAN_Configuration(void)
{
	CAN_InitTypeDef        can;
	CAN_FilterInitTypeDef  can_filter;
	GPIO_InitTypeDef 	   gpio;
	NVIC_InitTypeDef   	   nvic;

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN1,  ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA , ENABLE);
    
 //   GPIO_PinRemapConfig(GPIO_Remap1_CAN1,ENABLE);
   
	
	gpio.GPIO_Pin = GPIO_Pin_11;
	gpio.GPIO_Mode = GPIO_Mode_IPU;
    gpio.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &gpio);	//CAN_RX
	
	gpio.GPIO_Pin = GPIO_Pin_12;	   
    gpio.GPIO_Mode = GPIO_Mode_AF_PP;
	gpio.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &gpio);    //CAN_TX

    nvic.NVIC_IRQChannel = USB_HP_CAN1_TX_IRQn;
    nvic.NVIC_IRQChannelPreemptionPriority = 1;
    nvic.NVIC_IRQChannelSubPriority = 0;
    nvic.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&nvic);
    
    nvic.NVIC_IRQChannel = USB_LP_CAN1_RX0_IRQn;  
    nvic.NVIC_IRQChannelPreemptionPriority = 1;
    nvic.NVIC_IRQChannelSubPriority = 1;
    nvic.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&nvic);

	CAN_DeInit(CAN1);

	can.CAN_TTCM = DISABLE;
	can.CAN_ABOM = DISABLE;
	can.CAN_AWUM = DISABLE;
	can.CAN_NART = DISABLE;	  
	can.CAN_RFLM = DISABLE;																
	can.CAN_TXFP = ENABLE;		
 	can.CAN_Mode = CAN_Mode_Normal;
	can.CAN_SJW = CAN_SJW_1tq;
	can.CAN_BS1 = CAN_BS1_5tq;
	can.CAN_BS2 = CAN_BS2_3tq;
	can.CAN_Prescaler = 4;     //CAN BaudRate 36/(1+5+3)/4=1Mbps
	CAN_Init(CAN1, &can);
	
	 can_filter.CAN_FilterNumber=0;
    can_filter.CAN_FilterMode=CAN_FilterMode_IdMask;
    can_filter.CAN_FilterScale=CAN_FilterScale_32bit;
    can_filter.CAN_FilterIdHigh=0x0000;
    can_filter.CAN_FilterIdLow=0x0000;
    can_filter.CAN_FilterMaskIdHigh=0x0000;
    can_filter.CAN_FilterMaskIdLow=0x0000;
    can_filter.CAN_FilterFIFOAssignment=0;//the message which pass the filter save in fifo0
    can_filter.CAN_FilterActivation=ENABLE;
	CAN_FilterInit(&can_filter);
    
    CAN_ITConfig(CAN1,CAN_IT_FMP0,ENABLE);
    CAN_ITConfig(CAN1,CAN_IT_TME,ENABLE);
}


void CAN1_sent(short Data1,short Data2,short Data3,short Data4 )
{
    CanTxMsg tx_message;
    
    tx_message.StdId = 0x556;
    tx_message.IDE = CAN_Id_Standard;
    tx_message.RTR = CAN_RTR_Data;
    tx_message.DLC = 0x08;
    
    tx_message.Data[0] = (uint8_t)(Data1>>8);
    tx_message.Data[1] = (uint8_t)(Data1);
    tx_message.Data[2] = (uint8_t)(Data2>>8);
    tx_message.Data[3] = (uint8_t)(Data2);
    tx_message.Data[4] = (uint8_t)(Data3>>8);
    tx_message.Data[5] = (uint8_t)(Data3);
    tx_message.Data[6] = (uint8_t)(Data4>>8);
    tx_message.Data[7] = (uint8_t)(Data4);
    
    CAN_Transmit(CAN1,&tx_message);
}


/***********************************************
*CAN1 reciver data interrupt
***********************************************/
unsigned char moto_start_flag=0;   			//==1:MOTO_foreward     ==2:MOTO_inversion
unsigned char can_get_flag =0; 
unsigned char Receive_Attack = 0;
u16 Change_Blood = 0;
u8  Change_Ways = 0;
u8 can_flag=0;
u8 Status = 0;  //1 ң��������  0��ң����δ����
void USB_LP_CAN1_RX0_IRQHandler(void)
{
    CanRxMsg rx_message;
	
    if (CAN_GetITStatus(CAN1,CAN_IT_FMP0)!= RESET) 
	{
        CAN_ClearITPendingBit(CAN1, CAN_IT_FMP0);
        CAN_Receive(CAN1, CAN_FIFO0, &rx_message);
		if(rx_message.StdId == 0x308)
		{
			//can_flag = 1;
			
			if( rx_message.Data[0]==2 )
			{

				can_flag = 0;
				GPIO_SetBits(GPIOB,GPIO_Pin_12);

			}
			else if(  rx_message.Data[0]==1 )		//����
			{
				can_flag = 1;
				GPIO_ResetBits(GPIOB,GPIO_Pin_12);
			}
		 
		}

    }
}

void USB_HP_CAN1_TX_IRQHandler(void)
{
    if (CAN_GetITStatus(CAN1,CAN_IT_TME)!= RESET) 
	{
	   CAN_ClearITPendingBit(CAN1,CAN_IT_TME);

    }
}

