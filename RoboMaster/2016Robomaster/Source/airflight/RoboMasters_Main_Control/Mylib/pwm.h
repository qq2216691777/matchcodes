#ifndef __PWM_H__
#define __PWM_H__

void PWM1_2_Configuration(void);
void PWM3_4_Configuration(void);
void PWM5_6_Configuration( void );
void PWM7_8_Configuration( void );

#define PWM_CH6  TIM1->CCR1
#define PWM_CH5  TIM1->CCR2

#define PWM_CH1  TIM5->CCR1
#define PWM_CH2  TIM5->CCR2

#define PWM_CH3  TIM3->CCR1
#define PWM_CH4  TIM3->CCR2


#define PWM_CH8  TIM2->CCR3
#define PWM_CH7  TIM2->CCR4

#endif
