RoboMasters客户端简要说明

vcredist_x86.exe	visual studio 2008 redist 无法运行客户端时，需安装这个，安装一次就行。
RoboWarClient.exe	客户端程序，可以用这个调试鼠标键盘。
RoboWarServer.exe	服务器端程序。
videoPreview.exe	视频显示程序，之前的比赛用的是天创恒达的TC-1000DVI采集卡，这个程序是调用了那个采集卡的SDK，如果你要用其他采集卡，需自己开发个显示视频的程序，改名为“videoPreview.exe”放到程序目录覆盖就行。