#ifndef __USART1_H__
#define __USART1_H__

#include <stm32f4xx.h>
#include <stdio.h>

void USART1_Configuration(void);
void USART1_SendChar(unsigned char b);
void RS232_Print( USART_TypeDef*, u8* );
void RS232_VisualScope( USART_TypeDef*, u8*, u16 );
void Usart_SendString(unsigned char *p);
void os_float(uint8_t *wareaddr, uint32_t waresize);
void os_CAN(CanRxMsg rx_message);
#endif
