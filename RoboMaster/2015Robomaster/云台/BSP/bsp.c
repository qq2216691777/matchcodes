#include "bsp.h"

#include "global_define.h"

void BSP_Init(void){
    RCC_PCLK1Config(RCC_HCLK_Div2);
    /* Configure the NVIC Preemption Priority Bits */
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);	
    LED_Configuration();              //指示灯初始化
//     RELAY_Configuration();	    	    //cyq:继电器初始化
//     LASER_Configuration();						//激光
    CAN1_Configuration();             //初始化CAN1
    CAN2_Configuration(); 						//初始化CAN2
    USART1_Configuration();           //串口1初始化
// 		CARRY_Configuration();            //策略函数；
}


