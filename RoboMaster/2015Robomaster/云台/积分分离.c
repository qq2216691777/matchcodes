#include <stdio.h>
#include <math.h>

struct _pid{

	float setspeed;
	float actualaspeed;
	float err;
	float err_last;
	float Kp,Ki,Kd;
	float voltage;
	float integral;
	}PID;
	
void PID_init()
{
	PID.setspeed = 0.0;	
	PID.actualaspeed = 0.0;
	PID.err = 0.0;
	PID.err_last = 0.0;
	PID.voltage = 0.0;
	PID.integral = 0.0;
	PID.Kp = 0.2;
	PID.Ki = 0.015;
	PID.Kd = 0.2;
}

float PID_realize(float speed)
{
	PID.setspeed = speed;
	PID.err = PID.setspeed - PID.actualaspeed;	
	if(PID.err>200){}  //积分分离控制算法 
	else
		{
		PID.integral +=  PID.err;	
		}
	PID.voltage = PID.Kp*PID.err + PID.Ki*PID.integral + PID.Kd*(PID.err - PID.err_last);
	PID.err_last = PID.err;
	PID.actualaspeed = PID.voltage * 1.0;
	return PID.actualaspeed;
}

int main()
{
	float speed_pid = 0.0;
	int count = 0;
	printf("PID result!\n");
	PID_init();
	while(count<1000)
	{
		speed_pid = PID_realize(200.0);
		printf("   %f\n",speed_pid);
		//count ++;	
	}
	
	return 0;	
}
