#include "bsp.h"

#include "global_define.h"
#include "main.h"
extern float target_pitch_angle;
extern float target_yaw_angle;

void BSP_Init(void){
    
    unsigned char i=0;  
	/* Configure the NVIC Preemption Priority Bits */
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
    USART1_Configuration();          //串口1初始化
//     LED_Configuration();            //指示灯初始化
//     RELAY_Configuration();	    	//cyq:继电器初始化
//     LASER_Configuration(); 
	  PWM_Configuration(); 
		PWM1=0;//送弹 电机
	  //////摩擦轮初始化///////
		PWM2=200;//摩擦轮 电机
	  target_pitch_angle=PITCH_MID;
		target_yaw_angle=YAW_MID;//test wxp 
	  delay_ms(500);delay_ms(500);
		//////////////////MPU6050//////////
    while(MPU6050_Initialization() == 0xff) 
		{
			i++;     //如果一次初始化没有成功，那就再来一次                     
			if(i>10) //如果初始化一直不成功，那就没希望了，进入死循环，蜂鸣器一直叫
			{
					while(1) 
					{
							LED1_TOGGLE();
							delay_ms(10);
							printf("MPU6050 Error!\n");							
					}
			}  
		} 	
		//MPU6050 校准函数
		MPU6050_Gyro_calibration();
		//MPU6050 外部中断处理函数
 		MPU6050_Interrupt_Configuration();	      
		//设定占空比为200-250，摩擦轮运行
		//初始化CAN  
	  CAN1_Configuration();            
    CAN2_Configuration();	
		PWM2 = 230;
		PWM1 = 0;//送弹
		//////////////////	  
}


