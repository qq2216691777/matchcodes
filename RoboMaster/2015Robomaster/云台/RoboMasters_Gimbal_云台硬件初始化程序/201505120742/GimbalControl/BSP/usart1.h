#ifndef __USART1_H__
#define __USART1_H__

#include <stm32f4xx.h>
#include <stdio.h>

void USART1_Configuration(void);
void USART1_SendChar(unsigned char b);
void RS232_Print( USART_TypeDef*, u8* );
void RS232_VisualScope( USART_TypeDef*, u8*, u16 );
void shanwai_send_data1(uint8_t *value,uint32_t size );  //send PID data
void SDS_OutPut_Data(float S_Out[]);//虚拟示波器调试
/******************山外串口调试助手***********************/
void VCAN_SendData(uint8_t *addr,uint32_t size);
#endif
