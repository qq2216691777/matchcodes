#include "usart1.h"
#include "laser.h"

/*-----USART1_TX-----PA9-----*/
/*-----USART1_RX-----PA10----*/
//cyq: for test

void USART1_Configuration(void)
{
    USART_InitTypeDef usart1;
	  GPIO_InitTypeDef  gpio;
    NVIC_InitTypeDef  nvic;
	
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA,ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1,ENABLE);
    
    GPIO_PinAFConfig(GPIOA,GPIO_PinSource9 ,GPIO_AF_USART1);
    GPIO_PinAFConfig(GPIOA,GPIO_PinSource10,GPIO_AF_USART1); 
	
    gpio.GPIO_Pin = GPIO_Pin_9 | GPIO_Pin_10;
    gpio.GPIO_Mode = GPIO_Mode_AF;
    gpio.GPIO_OType = GPIO_OType_PP;
    gpio.GPIO_Speed = GPIO_Speed_100MHz;
    gpio.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_Init(GPIOA,&gpio);

    usart1.USART_BaudRate = 115200;
    usart1.USART_WordLength = USART_WordLength_8b;
    usart1.USART_StopBits = USART_StopBits_1;
    usart1.USART_Parity = USART_Parity_No;
    usart1.USART_Mode = USART_Mode_Tx|USART_Mode_Rx;
    usart1.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_Init(USART1,&usart1);

    USART_ITConfig(USART1,USART_IT_RXNE,ENABLE);
    USART_Cmd(USART1,ENABLE);
    
    nvic.NVIC_IRQChannel = USART1_IRQn;
    nvic.NVIC_IRQChannelPreemptionPriority = 1;
    nvic.NVIC_IRQChannelSubPriority = 1;
    nvic.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&nvic);
}

void USART1_SendChar(unsigned char b)
{
    while (USART_GetFlagStatus(USART1,USART_FLAG_TC) == RESET);
    USART_SendData(USART1,b);
}

int fputc(int ch, FILE *f)
{
    while (USART_GetFlagStatus(USART1,USART_FLAG_TC) == RESET);
    USART_SendData(USART1, (uint8_t)ch);    
    return ch;
}

u8 USART_RX_BUF[200];     //接收缓冲,最大USART_REC_LEN个字节.
//接收状态
//bit15，	接收完成标志
//bit14，	接收到0x0d
//bit13~0，	接收到的有效字节数目
u16 USART_RX_STA=0;       //接收状态标记
u8 Res = 0; 
extern float v_p;
extern float v_d;
extern float  target_pitch_angle;
extern float  target_yaw_angle;
void PID_paimition()
{
// 	static u8 flag=0;
// 	switch(Res)
// 	{
// 		case 'p': flag=0x01;//调节p
// 			break;
// 		case 'd': flag=0x02;//调节d
// 			break;
// 		case 'a': flag=0x04;//调节目标角度
// 			break;
// 		case 'x': flag|=0x08;//第4位为高
// 			break;
// 		case 'y': flag&=~0x08;//第4位为置低
// 			break;
// 		case '#': flag|=0x80;
// 	}
// 	switch(flag)
// 	{
// 		case 0x09:
// 	}
// 	Res=0;
}
/******************山外串口调试助手***********************/
#define CMD 3
void VCAN_SendData(uint8_t *addr,uint32_t size)
{
	uint32_t i;
	uint8_t cmdf[2]={CMD,~CMD};	
	USART1_SendChar(cmdf[0]);
	USART1_SendChar(cmdf[1]);
	for(i=size;i>0;i--)
	{
		USART1_SendChar(*addr);
		addr++;
	}
	USART1_SendChar(cmdf[1]);
	USART1_SendChar(cmdf[0]);
}
/*********************************************************/


/**********************usart send data ************************/
float SDS_OutData[4] = { 0 };

unsigned short CRC_CHECK(unsigned char *Buf, unsigned char CRC_CNT)
{
    unsigned short CRC_Temp;
    unsigned char i,j;
    CRC_Temp = 0xffff;

    for (i=0;i<CRC_CNT; i++){      
        CRC_Temp ^= Buf[i];
        for (j=0;j<8;j++) {
            if (CRC_Temp & 0x01)
                CRC_Temp = (CRC_Temp >>1 ) ^ 0xa001;
            else
                CRC_Temp = CRC_Temp >> 1;
        }
    }
    return(CRC_Temp);
}

void SDS_OutPut_Data(float S_Out[])
{
  int temp[4] = {0};
  unsigned int temp1[4] = {0};
  unsigned char databuf[10] = {0};
  unsigned char i;
  unsigned short CRC16 = 0; 
  float SDS_OutData[4];
  for(i=0;i<4;i++) {
  SDS_OutData[i]=S_Out[i];
  }
  for(i=0;i<4;i++)
   {
			temp[i]  = (int)SDS_OutData[i];
			temp1[i] = (unsigned int)temp[i];
   } 
  for(i=0;i<4;i++) 
  {
			databuf[i*2]   = (unsigned char)(temp1[i]%256);
			databuf[i*2+1] = (unsigned char)(temp1[i]/256);
  }
  CRC16 = CRC_CHECK(databuf,8);
  databuf[8] = CRC16%256;
  databuf[9] = CRC16/256;
  
  for(i=0;i<10;i++)
    USART1_SendChar(databuf[i]);  //LINK TO UART 
}

/****************************************************************/
void USART1_IRQHandler(void)
{
	if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
    {
				Res =USART_ReceiveData(USART1);//(USART1->DR);	//读取接收到的数据	
				//PID_paimition();//串口控制命令
				USART_ClearITPendingBit(USART1,USART_IT_RXNE);
    }
}
