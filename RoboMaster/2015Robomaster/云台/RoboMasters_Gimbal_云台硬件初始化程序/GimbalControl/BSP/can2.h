#ifndef __CAN2_H__
#define __CAN2_H__

#include <stm32f4xx.h>
#include "laser.h"
#include "can1.h"

#define PITCH_MID 	190.0
#define YAW_MID 		169.0

#define PITCH_MAX 	(PITCH_MID+20.0)
#define YAW_MAX 		(YAW_MID+89.0)

#define PITCH_MIN 	(PITCH_MID-20.0)
#define YAW_MIN 		(YAW_MID-89.00)

extern float YAW_Angle;
extern float dipan_gyro_angle;
extern uint8_t shooting_flag;
extern int8_t gyro_ok_flag;

void CAN2_Configuration(void);
void GYRO_RST(void);
void Encoder_sent(float encoder_angle);

#endif 
