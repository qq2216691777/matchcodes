#ifndef __MAIN_H__
#define __MAIN_H__

#include "stm32f4xx.h"

#include "mpu6050_i2c.h"
#include "mpu6050_interrupt.h"
#include "mpu6050_driver.h"
#include "mpu6050_process.h"
#include "app.h"
#include "bsp.h"
#include "module_rs232.h"

#include <stdio.h>
#include <string.h>
#include <math.h>
extern int16_t angle_201;//pitch的 机械角度
extern int16_t angle_203;//yaw  的 机械角度
extern float target_pitch_angle;//pitch的 目标角度
extern float target_yaw_angle;//yaw的 目标角度
extern float SDS_OutData[4];//[0]PITCH实际角度//[1]PITCH结果输出//[2]YAW实际角度//[3]YAW结果输出


#endif
