#ifndef __APP_H__
#define __APP_H__
#include <stm32f4xx.h>

#define ESC_MAX 5000.0
#define abs(x) ((x)>0? (x):(-(x)))
//################################################
// extern int16_t last_201_angle;
// extern int16_t this_201_angle;
// extern int16_t this_203_angle;
// extern int16_t last_203_angle;
//################################################
extern float this_yaw_angle;//当前   yaw 角度值
extern float last_yaw_angle;//上次   yaw 角度值
extern float this_pitch_angle;//当前   pitch 角度值
extern float last_pitch_angle;//上次   pitch 角度值
extern float target_pitch_angle;//目标 pitch 角度
extern float target_yaw_angle;  //目标 yaw   角度
// extern static int16_t current_201;//201 实际电流
// extern static int16_t current_203;//203 实际电流
void Cmd_ESC(int16_t current_201,int16_t current_202,int16_t current_203);
void APP_Chassis_AngleControl(void);
float Velocity_Control_201(float current_velocity_201,float target_velocity_201);
float Position_Control_201(float current_position_201,float target_position_201);

float Velocity_Control_203(float current_velocity_203,float target_velocity_203);
float Position_Control_203(float current_position_203,float target_position_203);

#endif
