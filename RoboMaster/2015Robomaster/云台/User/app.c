#include "main.h"

#define GAP 0.0 //限制 最小值 为 0


//##############################################################
// int16_t last_201_angle=0;			//ID：0x201 电调上次的 角度值
// int16_t this_201_angle=0;			//ID：0x201 电调返回的 角度值
// int16_t this_203_angle=0;			//ID：0x203 电调返回的 角度值
// int16_t last_203_angle=0;			//ID：0x203 电调上次的 角度值
float target_pitch_angle=0;   // pitch 轴 目标角度
float target_yaw_angle=0;			// yaw   轴 目标角度
float this_pitch_angle=0;			// pitch 轴 当前角度
float last_pitch_angle=0;			// pitch 轴 上次角度
float this_yaw_angle=0;				// yaw   轴 当前角度
float last_yaw_angle=0;				// yaw   轴 上次角度
float velocity_201_output=0;  //pitch轴速度环函数的输出值
float position_201_output=0;	//pitch轴位置环函数的输出值
float velocity_203_output=0;  //yaw轴速度环函数的输出值
float position_203_output=0;	//yaw轴位置环函数的输出值
//##############################################################


/********************************************************************************
   给电调板发送指令，ID号为0x200，只用两个电调板，数据回传ID为0x201和0x202
	 cyq:更改为发送三个电调的指令。
*********************************************************************************/
void Cmd_ESC(int16_t current_201,int16_t current_202,int16_t current_203)
{
    CanTxMsg tx_message;
    
    tx_message.StdId = 0x200;
    tx_message.IDE = CAN_Id_Standard;
    tx_message.RTR = CAN_RTR_Data;
    tx_message.DLC = 0x08;
    
    tx_message.Data[0] = (uint8_t)(current_201 >> 8); //高8位
    tx_message.Data[1] = (uint8_t)current_201;				//低8位
    tx_message.Data[2] = (uint8_t)(current_202 >> 8);
    tx_message.Data[3] = (uint8_t)current_202;
    tx_message.Data[4] = (uint8_t)(current_203 >> 8);
    tx_message.Data[5] = (uint8_t)current_203;
    tx_message.Data[6] = 0x00;
    tx_message.Data[7] = 0x00;
    
    CAN_Transmit(CAN1,&tx_message);
}

/********************************************************************************
                         pitch轴电调板的速度环控制
                    输入 pitch轴当前速度 pitch轴目标速度
*********************************************************************************/
float Velocity_Control_201(float current_velocity_201,float target_velocity_201)
{
    const float v_p = 50.0;
    const float v_d = 0;
    
    static float error_v[2] = {0.0,0.0};
    static float output = 0;
    
    if(abs(current_velocity_201) < GAP)
    {
        current_velocity_201 = 0.0;
    }
    
    error_v[0] = error_v[1];//上次的误差值
    error_v[1] = target_velocity_201 - current_velocity_201;//当前的误差值
    
    output = error_v[1] * v_p    //比例项           
             + (error_v[1] - error_v[0]) * v_d;//微分项
     
    if(output > ESC_MAX)//限定输出最大值
    {
        output = ESC_MAX;
    }
    
    if(output < -ESC_MAX)//限定输出最小值
    {
        output = -ESC_MAX;
    }
    velocity_201_output = -output;
    return -output;//cyq:for6015 反向
}


/********************************************************************************
                         pitch轴电调板的位置环控制 
                    输入 pitch轴当前位置 pitch轴目标位置
*********************************************************************************/
float Position_Control_201(float current_position_201,float target_position_201)
{
    
    const float l_p = 20.5;
    const float l_i = 0.0;
    const float l_d = 0.0;

    static float error_l[2] = {0.0,0.0};
    static float output = 0;
    static float inte = 0;//用于  积分
    
    error_l[0] = error_l[1];
    error_l[1] = target_position_201 - current_position_201;
    inte += error_l[1]; 
    
    output = error_l[1] * l_p 
            + inte * l_i 
            + (error_l[1] - error_l[0]) * l_d;
    
    if(output > ESC_MAX)
    {
        output = ESC_MAX;
    }
    
    if(output < -ESC_MAX)
    {
        output = -ESC_MAX;
    }
    position_201_output = output;		
    return output;
}
/********************************************************************************
                           yaw轴电调板的速度环控制
                      输入 yaw轴当前速度 yaw轴目标速度
*********************************************************************************/
float Velocity_Control_203(float current_velocity_203,float target_velocity_203)
{
    const float v_p = 50.0;
    const float v_d = 0.0;
    
    static float error_v[2] = {0.0,0.0};
    static float output = 0;
		
    if(abs(current_velocity_203) < GAP)
    {
        current_velocity_203 = 0.0;
    }
    
    error_v[0] = error_v[1];
    error_v[1] = target_velocity_203 - current_velocity_203;
    
    output = error_v[1] * v_p
             + (error_v[1] - error_v[0]) * v_d;
     
    if(output > ESC_MAX)
    {
        output = ESC_MAX;
    }
    
    if(output < -ESC_MAX)
    {
        output = -ESC_MAX;
    }
    velocity_203_output = -output;
    return -output;//cyq:for6015 反向
}

/********************************************************************************
                           yaw轴电调板的位置环控制
                      输入 yaw轴当前位置 yaw轴目标位置
*********************************************************************************/
float Position_Control_203(float current_position_203,float target_position_203)
{
    const float l_p = 30.010;//3#5#:0.760
		const float l_i = 0.0;//0.000035;
    const float l_d = 0.5;//3.5;
    
    static float error_l[3] = {0.0,0.0,0.0};
    static float output = 0;
    
    error_l[0] = error_l[1];
    error_l[1] = error_l[2];    
    error_l[2] = target_position_203 - current_position_203;
 
    output = error_l[2] * l_p 
							+ error_l[2] * l_i 
							+ (error_l[2] - error_l[1]) * l_d;
    
    if(output > ESC_MAX)
    {
        output = ESC_MAX;
    }
    
    if(output < -ESC_MAX)
    {
        output = -ESC_MAX;
    }
    position_201_output = -output;
    return -output;
}

/********************************************************************************
                  AngleControl_Chassis()
函数：           底盘运行位置环控制的执行函数
输入：					 当前底盘角度  目标底盘角度
********************************************************************************
void AngleControl_Chassis(float current_angle,float target_angle)
{
		const float c_p = 30.010;//3#5#:0.760
		const float c_i = 0.0;//0.000035;
    const float c_d = 0.5;//3.5;
    
    static float error_c[3] = {0.0,0.0,0.0};
    static float output = 0;
    
    error_c[0] = error_c[1];
    error_c[1] = error_c[2];    
    error_c[2] = target_angle - current_angle;
 
    output = error_c[2] * c_p 
							+ error_c[2] * c_i 
							+ (error_c[2] - error_c[1]) * c_d;
    
    if(output > ESC_MAX)
    {
        output = ESC_MAX;
    }
    
    if(output < -ESC_MAX)
    {
        output = -ESC_MAX;
    }
    position_201_output = -output;
    
}*/
/********************************************************************************
                           APP_Runing()
函数：           运行位置环控制的执行函数
*********************************************************************************/
unsigned char APP_Runing(void)
{
	Position_Control_201(this_pitch_angle,target_pitch_angle);
	Position_Control_203(this_yaw_angle,target_yaw_angle);
	Velocity_Control_201(1,position_201_output);
	Cmd_ESC(velocity_203_output,0x00,velocity_203_output);
	return 0;
}


