#ifndef _LINKLIST_H_
#define _LINKLIST_H_

typedef struct __tag_LinkListNode LinkListNode;

struct __tag_LinkListNode
{
	unsigned char From;
	unsigned char to;
    LinkListNode* next;
	unsigned int Num;
	long long Order_Num;	
};

typedef struct __tag_LinkList
{
    LinkListNode header;
    int length;
} TLinkList;

TLinkList* LinkList_Create( void );

void LinkList_Clear(TLinkList* list);

int LinkList_Length(TLinkList* list);

int LinkList_Insert(TLinkList* list, LinkListNode* node, int pos);



#endif
