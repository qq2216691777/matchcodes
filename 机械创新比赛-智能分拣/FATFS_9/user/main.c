/* STM32F10X_HD,USE_STDPERIPH_DRIVER */

/**
	***************************************
			@作者     叶念西风 
			@作用     工程模板   
			@日期     2015—6-23
			@备注   
	***************************************
**/

#include "stm32f10x.h"
#include "delay.h"
#include "usart.h"
#include "fatfs.h"
#include "ili9320.h"
#include "timer.h"
 
extern TLinkList* list;
extern FIL fdst;
extern char dataa[FIFO_CHAR_NUM+4];

void LED_Init(void);
int main( void )
{
	LinkListNode* tar;
	u8 i=0;
	u8 US_TEMP=0;
	long long Uar_Temp=0;
	
	delay_init();
	NVIC_Configuration(); //设置中断分组
	uart_init( 9600 );
	LED_Init();
	TIM3_Int_Init( 4999, 7199 );
	
	Ili9320_Init();
	LCD_Clear( 0, 0, 320, 240, Red );
	GUI_Text(60, 20,"Robot Innovation Studio", Yellow, Red );
	
	Data_Init();
	delay_ms(1000);

	LCD_Clear( 0, 40, 320, 240, Red );
//	GUI_Text_Num( 270, 25, list->length, White, Red);
	
	GUI_Text(50, 70,"NO.", Yellow, Red );
	LCD_Clear( 120, 60, 280, 90, White );
	GUI_Text(50, 110,"Or_Num", Yellow, Red );
	LCD_Clear( 120, 100, 280, 130, White );
	GUI_Text(50, 150,"FROM", Yellow, Red );
	LCD_Clear( 120, 140, 280, 170, White );
	GUI_Text(50, 190,"TO", Yellow, Red );
	LCD_Clear( 120, 180, 280, 210, White );
	
	while(1)
	{
		if(USART_RX_STA&0x8000)
		{
			USART_RX_STA=0;
			Uar_Temp = Char_To_Number( USART_RX_BUF );
			tar = Find_Position( Uar_Temp );
			if( tar!= NULL ) 
			{
							
				LCD_Clear( 120, 60, 280, 90, White );		
				LCD_Clear( 120, 100, 280, 130, White );		
				LCD_Clear( 120, 140, 280, 170, White );	
				LCD_Clear( 120, 180, 280, 210, White );
				GUI_Text_Num( 180, 68, tar->Num, Black, White);
				GUI_Text_Num( 140, 108, tar->Order_Num , Black, White); //找到数据
				GUI_Text( 170, 148, "not set", Black, White);
				GUI_Text( 180, 188, "not set", Black, White);
			}
			else if( list->length>9 )
			{
				f_open( &fdst,"0:/ISS/ISS_data.txt",FA_READ );
				i=0;
				while(i<10)
				{
					f_gets( dataa, FIFO_CHAR_NUM, &fdst );	
					i++;
				}
				US_TEMP = 1;
				while( tar== NULL )
				{
					f_gets( dataa, FIFO_CHAR_NUM, &fdst );
					if( !Check_Form(dataa) )
					{
						LCD_Clear( 120, 60, 280, 90, White );		
						LCD_Clear( 120, 100, 280, 130, White );		
						LCD_Clear( 120, 140, 280, 170, White );	
						LCD_Clear( 120, 180, 280, 210, White );	
						GUI_Text( 140, 108, "No Found", Black, White);
						US_TEMP = 0;
						break;
					}						
					Char_To_Num( dataa, &(list->header) );
					tar = Find_Position( Uar_Temp );
					
					if( f_eof(&fdst) && tar==NULL )  //没有查询到相关物流信息
					{
						LCD_Clear( 120, 60, 280, 90, White );		
						LCD_Clear( 120, 100, 280, 130, White );		
						LCD_Clear( 120, 140, 280, 170, White );	
						LCD_Clear( 120, 180, 280, 210, White );	
						GUI_Text( 140, 108, "No Found", Black, White);
						US_TEMP = 0;
						break;
					}
				}
				f_close( &fdst);
				if( US_TEMP )
				{
					LCD_Clear( 120, 60, 280, 90, White );		
					LCD_Clear( 120, 100, 280, 130, White );		
					LCD_Clear( 120, 140, 280, 170, White );	
					LCD_Clear( 120, 180, 280, 210, White );	
				
					GUI_Text_Num( 180, 68, tar->Num, Black, White);
					GUI_Text_Num( 140, 108, tar->Order_Num , Black, White); //找到数据
					GUI_Text( 170, 148, "not set", Black, White);
					GUI_Text( 180, 188, "not set", Black, White);
				}
			}
			else
			{
				GUI_Text( 140, 108, "No Found", Black, White);
			}
			
		}
		
	}
}

void LED_Init(void)
{
 
	GPIO_InitTypeDef  GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOD, ENABLE);	 //使能PB,PE端口时钟

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;				 //LED0-->PB.5 端口配置
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		 //推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;		 //IO口速度为50MHz
	GPIO_Init(GPIOD, &GPIO_InitStructure);					 //根据设定参数初始化GPIOB.5
 
}

