#include "robot_go.h"

MoTor Motor;
u8 t;
void Stand( void )
{
		Motor.Right_Up=1500;
		Motor.Right_Mid=1500;		
		Motor.Right_Down = 1500;	
		Motor.Left_Up = 1500;	 
		Motor.Left_Mid = 1500 ;
		Motor.Left_Down = 1500 ;
	
		Motor.RU_Speed=100;
		Motor.RM_Speed=100;
		Motor.RD_Speed=100;
		Motor.LU_Speed=100;
		Motor.LM_Speed=100;
		Motor.LD_Speed=100;
		
		Leg_Move(&Motor);	
}


void Setp_3( u8 tt )		//向前走3步
{
	
		/***************抬左脚**********************/
	Motor.Left_Down=1710;
	Motor.LD_Speed=80;
	
	Motor.Left_Mid=1450 ;//1690
	Motor.LM_Speed=20; 	//30
	
	Motor.Right_Down=1610;	
	Motor.RD_Speed=40;

	Leg_Move(&Motor);
	     
	/**********************第一步  迈到中间**********************/	

	Motor.Left_Up=1000 ;   //1040
	Motor.LU_Speed=40;   //30

	Motor.Left_Mid=1690 ;//1690
	Motor.LM_Speed=20; 	//30

	Motor.Left_Down=1820 ;
	Motor.LD_Speed=90; 
	
	Motor.Right_Up=1500;
	Motor.RU_Speed=20;   //35 

	Motor.Right_Mid=1255;	
	Motor.RM_Speed=20;    //  30 
	
	Motor.Right_Down=1670;	
	Motor.RD_Speed=60;  //25

	Leg_Move(&Motor);	
	//放腿
	
	Motor.Left_Up=1000 ;   //1040
	Motor.LU_Speed=40;   //30

	Motor.Left_Mid=1690 ;//1690
	Motor.LM_Speed=20; 	//30

	Motor.Left_Down=1560 ;
	Motor.LD_Speed=90; 
	
	Motor.Right_Up=1500;
	Motor.RU_Speed=20;   //35 

	Motor.Right_Mid=1255;	
	Motor.RM_Speed=20;    //  30 
	
	Motor.Right_Down=1590;	
	Motor.RD_Speed=60;  //25


	Leg_Move(&Motor);	
	
//	return;抬腿

while( tt-- )
{
	/********左腿放下并抬右腿***********/
	
	Motor.Left_Up=1000 ;
	Motor.LM_Speed=30; // 35
			
	Motor.Left_Mid=1720;//1770 ;
	Motor.LM_Speed=30;  //35

	Motor.Left_Down=1400 ;   ////
	Motor.LM_Speed=60;  //  40 

	Motor.Right_Up=1545 ;
	Motor.RM_Speed=45;

	Motor.Right_Mid=1255;	
	Motor.RM_Speed=30;

	Motor.Right_Down=1205;		
	Motor.RD_Speed=30;

	Leg_Move(&Motor);	 

//	 return ;
	/****************迈到中间***************************/	
	Motor.Left_Up = 1500;   //1570
	Motor.LU_Speed = 20;

	Motor.Left_Mid = 1255;	 //1210
	Motor.LM_Speed = 20; 

	Motor.Left_Down=1335 ;    //1335
	Motor.LD_Speed=45;  //20
 
	Motor.Right_Up = 1000 ;    //970
	Motor.RU_Speed = 40;  // 40

	Motor.Right_Mid = 1710;	  //1695
	Motor.RM_Speed = 25;  //  35

	Motor.Right_Down = 1300 ;   //1400
	Motor.RD_Speed = 60;  //40

	Leg_Move(&Motor);
	
//	return ; 
	/*************伸右腿并放下******第二步结束*********/	

	Motor.Left_Up = 1670;
	Motor.LU_Speed = 30;		
	Motor.Left_Mid = 1240;	
	Motor.LM_Speed = 25;		
	Motor.Left_Down = 1470;		
	Motor.LD_Speed = 20;

	Motor.Right_Up = 1030 ;
	Motor.RU_Speed = 30;
	Motor.Right_Mid = 1825;	
	Motor.RM_Speed = 20;
	Motor.Right_Down=1485 ;
	Motor.RD_Speed=15;

	Leg_Move(&Motor);	
	
//	return ;

	/***************抬左脚**********************/

// 	Motor.Left_Up = 1710;
// 	Motor.LU_Speed = 30;
	
	Motor.Left_Down=1710 ;
	Motor.LD_Speed=80;	//60

	Motor.Right_Down=1610;	
	Motor.RD_Speed=40;  //18

	Leg_Move(&Motor);
	

       
	/**********************第三步  迈到中间**********************/	

	Motor.Left_Up=1000 ;
	Motor.LU_Speed=40;  //30

	Motor.Left_Mid=1690 ;
	Motor.LM_Speed = 20;

	Motor.Left_Down=1820 ;
	Motor.LD_Speed=90;  //40

	Motor.Right_Up=1500 ;
	Motor.RU_Speed=20;

	Motor.Right_Mid=1255;	
	Motor.RM_Speed=20;

	Motor.Right_Down=1670;		//数值越大  越往右偏   1675
	Motor.RD_Speed=60;  //50
				
	Leg_Move(&Motor);
      
//return ;			
		 
	/*****************伸左腿并放下*****第三步结束*********************/

	Motor.Left_Up=1020 ;
	Motor.LU_Speed=35;

	Motor.Left_Mid=1715 ;
	Motor.LM_Speed=20;

	Motor.Left_Down=1515 ;
	Motor.LD_Speed=30;

	Motor.Right_Up=1480 ;
	Motor.RU_Speed=35;

	Motor.Right_Mid=1210;	
	Motor.RM_Speed=28;

	Motor.Right_Down=1520;	
	Motor.RD_Speed=40;  //20

	Leg_Move(&Motor);	
	   
}
	/**************抬右脚********************/

	Motor.Left_Up=1020 ;
	Motor.LU_Speed=30;

	Motor.Left_Mid=1715 ;
	Motor.LM_Speed=20;

	Motor.Left_Down=1375 ;
	Motor.LD_Speed=20;

	Motor.Right_Up=1535 ;
	Motor.RU_Speed=30;

	Motor.Right_Mid=1215;	
	Motor.RM_Speed=30;

	Motor.Right_Down=1010;	
	Motor.RD_Speed=40;  //60

	Leg_Move(&Motor);
	

	/****************迈到中间***************************/	
	Motor.Left_Up=1500;
	Motor.LU_Speed=20;  //30

	Motor.Left_Mid=1500;	
	Motor.LM_Speed=15;    //35

	Motor.Left_Down=1380 ;
	Motor.LD_Speed=40;   //  30

	Motor.Right_Up=1500 ;
	Motor.RU_Speed=10;  //40

	Motor.Right_Mid=1475 ; // 1465
	Motor.RM_Speed=10;   //20

	Motor.Right_Down=1350 ;
	Motor.RD_Speed=90; //20
			
	Leg_Move(&Motor);
   
	/***************立正*********/

	Motor.Right_Down=1500 ;
	Motor.RD_Speed=20;

	Motor.Left_Down=1500 ;
	Motor.LD_Speed=20;

	Leg_Move(&Motor);

/*************************抬起左脚***************************************/
	
//	Motor.Left_Down = 1720 ;
//	Motor.LD_Speed = 90;
//	
//	Motor.Right_Down=1650;	
//	Motor.RD_Speed=70;
//	
//	Leg_Move(&Motor);


///********************** 第一步 迈左脚***** end *****************/	
//		
//	Motor.Left_Up = 1200 ;
//	Motor.LU_Speed = 30;

//	Motor.Left_Mid = 1800 ;
//	Motor.LM_Speed = 30;
//	
//	Motor.Left_Down = 1480 ;
//	Motor.LD_Speed = 80;
//	
//	Motor.Right_Up = 1800;
//	Motor.RU_Speed = 30;

//	Motor.Right_Mid = 1200;	
//	Motor.RM_Speed = 30;
//	
//	Motor.Right_Down=1500;	
//	Motor.RD_Speed=15;
//	
//	Leg_Move(&Motor);
	
//	return ;	
	
	while( tt-- )
	{
	
/***************** 抬右脚**************************/  //【抬低右偏】
		Motor.Left_Up = 1500;	 
		Motor.Left_Mid = 1500 ;
 		Motor.Left_Down = 1410 ;  ////////////********//////////
		Motor.Right_Up = 1500;
		Motor.Right_Mid = 1535;		
		Motor.Right_Down = 1455;	

		Motor.LU_Speed=30;
		Motor.LM_Speed=30;
		Motor.LD_Speed=80;
		Motor.RU_Speed=30;
		Motor.RM_Speed=30;
		Motor.RD_Speed=50;
		
		Leg_Move(&Motor);
		
	  	
/******************   放下右脚  ********************/	
		Motor.Left_Up = 1895 ;
		Motor.LU_Speed = 30;     //30

		Motor.Left_Mid = 1170 ;
		Motor.LM_Speed = 30;    //30
		
		Motor.Left_Down = 1465 ;  /////改变此值可改变方向  高 右  低  左 注意电压影响。
		Motor.LD_Speed = 10;      //25 
		
		Motor.Right_Up = 1100;
		Motor.RU_Speed = 30;

		Motor.Right_Mid = 1890;	
		Motor.RM_Speed = 30;
		
		Motor.Right_Down=1525;	
		Motor.RD_Speed=8;
		
		Leg_Move(&Motor);
			
		
/*************************抬起左脚***************************************/  //【抬低左偏】
//		Motor.Right_Down = 1700;	
//		Motor.RD_Speed=50;
//		Motor.Left_Down = 1800 ;
//		Motor.LD_Speed=80;
//		Leg_Move(&Motor);
//			
//		
	
		Motor.Left_Up = 1500;	 
		Motor.Left_Mid = 1470 ;
		Motor.Left_Down = 1550 ;
		Motor.Right_Up = 1500;
		Motor.Right_Mid = 1500;		
		Motor.Right_Down = 1630;	

		Motor.LU_Speed=30;
		Motor.LM_Speed=30;
		Motor.LD_Speed=60;
		Motor.RU_Speed=30;
		Motor.RM_Speed=30;
		Motor.RD_Speed=80;
		
		Leg_Move(&Motor);
	 
//		return ;
/********************** 第一步 迈左脚***** end *****************/	
		
		Motor.Left_Up = 1150 ;
		Motor.LU_Speed = 30;

		Motor.Left_Mid = 1850 ;
		Motor.LM_Speed = 20;
		
		Motor.Left_Down = 1500 ;
		Motor.LD_Speed = 70;
		
		Motor.Right_Up = 1850;
		Motor.RU_Speed = 30;

		Motor.Right_Mid = 1220;	
		Motor.RM_Speed = 30;
		
		Motor.Right_Down=1490;	
		Motor.RD_Speed=10;
		
		Leg_Move(&Motor);
	  
	}
//	delay_ms(20);
		
/***************** 抬右脚**************************/
		Motor.Left_Up = 1500;	 
		Motor.Left_Mid = 1500 ;
 		Motor.Left_Down = 1410 ;  ////////////********//////////
		Motor.Right_Up = 1500;
		Motor.Right_Mid = 1535;		
		Motor.Right_Down = 1455;	

		Motor.LU_Speed=30;
		Motor.LM_Speed=30;
		Motor.LD_Speed=80;
		Motor.RU_Speed=30;
		Motor.RM_Speed=30;
		Motor.RD_Speed=50;
		
		Leg_Move(&Motor);
	
	
	

/***************  立正  ****************/

	Motor.Right_Down=1500 ;
	Motor.RD_Speed=30;

	Motor.Left_Down=1500 ;
	Motor.LD_Speed=30;

	Leg_Move(&Motor);


}

void Begin2_step( void )
{
	/***************抬左脚**********************/
	Motor.Left_Down=1710;
	Motor.LD_Speed=80;
	
	Motor.Right_Down=1610;	
	Motor.RD_Speed=40;

	Leg_Move(&Motor);
	     
	
	/**********************第一步  迈到中间**********************/	

	Motor.Left_Up=1000 ;   //1040
	Motor.LU_Speed=40;   //30

	Motor.Left_Mid=1690 ;//1690
	Motor.LM_Speed=20; 	//30

	Motor.Left_Down=1820 ;
	Motor.LD_Speed=90; 
	
	Motor.Right_Up=1500;
	Motor.RU_Speed=20;   //35 

	Motor.Right_Mid=1255;	
	Motor.RM_Speed=20;    //  30 
	
	Motor.Right_Down=1670;	
	Motor.RD_Speed=60;  //25


	Leg_Move(&Motor);	


  
      

	
	/********左腿放下并抬右腿***********/
	
	Motor.Left_Up=1000 ;
	Motor.LM_Speed=30; // 35
			
	Motor.Left_Mid=1720;//1770 ;
	Motor.LM_Speed=30;  //35

	Motor.Left_Down=1400 ;   ////
	Motor.LM_Speed=60;  //  40 

	Motor.Right_Up=1545 ;
	Motor.RM_Speed=45;

	Motor.Right_Mid=1175;	
	Motor.RM_Speed=30;

	Motor.Right_Down=1205;		
	Motor.RD_Speed=30;

	Leg_Move(&Motor);	
	
			
	delay_ms(50);
		
  
	 

	 
	/****************迈到中间***************************/	
	Motor.Left_Up = 1500;   //1570
	Motor.LU_Speed = 20;

	Motor.Left_Mid = 1255;	 //1210
	Motor.LM_Speed = 20; 

	Motor.Left_Down=1335 ;    //1335
	Motor.LD_Speed=45;  //20
 
	Motor.Right_Up = 1000 ;    //970
	Motor.RU_Speed = 30;  // 40

	Motor.Right_Mid = 1710;	  //1695
	Motor.RM_Speed = 20;  //  35

	Motor.Right_Down = 1130 ;   //1400
	Motor.RD_Speed = 40;  //40

	Leg_Move(&Motor);
	
//	 return ;
	/*************伸右腿并放下******第二步结束*********/	

	Motor.Left_Up = 1670;
	Motor.LU_Speed = 30;		
	Motor.Left_Mid = 1240;	
	Motor.LM_Speed = 20;		
	Motor.Left_Down = 1470;		
	Motor.LD_Speed = 10;

	Motor.Right_Up = 1030 ;
	Motor.RU_Speed = 30;
	Motor.Right_Mid = 1790;	
	Motor.RM_Speed = 30;
	Motor.Right_Down=1505 ;
	Motor.RD_Speed=20;

	Leg_Move(&Motor);	
	


	/***************抬左脚**********************/

// 	Motor.Left_Up = 1710;
// 	Motor.LU_Speed = 30;
	
	Motor.Left_Down=1710 ;
	Motor.LD_Speed=80;	//60

	Motor.Right_Down=1610;	
	Motor.RD_Speed=40;  //18

	Leg_Move(&Motor);
	

       
	/**********************第三步  迈到中间**********************/	

	Motor.Left_Up=1000 ;
	Motor.LU_Speed=40;  //30

	Motor.Left_Mid=1690 ;
	Motor.LM_Speed=20;

	Motor.Left_Down=1820 ;
	Motor.LD_Speed=90;  //40

	Motor.Right_Up=1500 ;
	Motor.RU_Speed=20;

	Motor.Right_Mid=1255;	
	Motor.RM_Speed=20;

	Motor.Right_Down=1670;		//数值越大  越往右偏   1675
	Motor.RD_Speed=60;  //50
				
	Leg_Move(&Motor);
         	
		 
	/*****************伸左腿并放下*****第三步结束*********************/

	Motor.Left_Up=1020 ;
	Motor.LU_Speed=35;

	Motor.Left_Mid=1715 ;
	Motor.LM_Speed=20;

	Motor.Left_Down=1515 ;
	Motor.LD_Speed=30;

	Motor.Right_Up=1480 ;
	Motor.RU_Speed=35;

	Motor.Right_Mid=1210;	
	Motor.RM_Speed=28;

	Motor.Right_Down=1520;	
	Motor.RD_Speed=50;  //20

	Leg_Move(&Motor);	
	   
  
	/**************抬右脚********************/

	Motor.Left_Up=1020 ;
	Motor.LU_Speed=30;

	Motor.Left_Mid=1715 ;
	Motor.LM_Speed=20;

	Motor.Left_Down=1375 ;
	Motor.LD_Speed=20;

	Motor.Right_Up=1535 ;
	Motor.RU_Speed=30;

	Motor.Right_Mid=1215;	
	Motor.RM_Speed=30;

	Motor.Right_Down=1010;	
	Motor.RD_Speed=50;  //60

	Leg_Move(&Motor);
	

	/****************迈到中间***************************/	
	Motor.Left_Up=1500;
	Motor.LU_Speed=20;  //30

	Motor.Left_Mid=1500;	
	Motor.LM_Speed=15;    //35

	Motor.Left_Down=1380 ;
	Motor.LD_Speed=40;   //  30

	Motor.Right_Up=1500 ;
	Motor.RU_Speed=10;  //40

	Motor.Right_Mid=1475 ; // 1465
	Motor.RM_Speed=10;   //20

	Motor.Right_Down=1350 ;
	Motor.RD_Speed=90; //20
			
	Leg_Move(&Motor);
   
	/***************立正*********/

	Motor.Right_Mid=1500 ; // 1465
	Motor.RM_Speed=20;   //20
	
	Motor.Right_Down=1500 ;
	Motor.RD_Speed=20;

	Motor.Left_Down=1500 ;
	Motor.LD_Speed=20;

	Leg_Move(&Motor);
//   return ;
}

void Begin_step( void )
{
	/***************抬左脚**********************/
	Motor.Left_Down=1710;
	Motor.LD_Speed=80;
	
	Motor.Left_Mid=1450 ;//1690
	Motor.LM_Speed=20; 	//30
	
	Motor.Right_Down=1610;	
	Motor.RD_Speed=40;

	Leg_Move(&Motor);
	     
	/**********************第一步  迈到中间**********************/	

	Motor.Left_Up=1000 ;   //1040
	Motor.LU_Speed=40;   //30

	Motor.Left_Mid=1690 ;//1690
	Motor.LM_Speed=20; 	//30

	Motor.Left_Down=1820 ;
	Motor.LD_Speed=90; 
	
	Motor.Right_Up=1500;
	Motor.RU_Speed=20;   //35 

	Motor.Right_Mid=1255;	
	Motor.RM_Speed=20;    //  30 
	
	Motor.Right_Down=1670;	
	Motor.RD_Speed=60;  //25


	Leg_Move(&Motor);	


	/********左腿放下并抬右腿***********/
	
	Motor.Left_Up=1000 ;
	Motor.LM_Speed=30; // 35
			
	Motor.Left_Mid=1720;//1770 ;
	Motor.LM_Speed=30;  //35

	Motor.Left_Down=1400 ;   ////
	Motor.LM_Speed=60;  //  40 

	Motor.Right_Up=1545 ;
	Motor.RM_Speed=45;

	Motor.Right_Mid=1175;	
	Motor.RM_Speed=30;

	Motor.Right_Down=1205;		
	Motor.RD_Speed=30;

	Leg_Move(&Motor);	 

	 
	/****************迈到中间***************************/	
	Motor.Left_Up = 1500;   //1570
	Motor.LU_Speed = 20;

	Motor.Left_Mid = 1255;	 //1210
	Motor.LM_Speed = 20; 

	Motor.Left_Down=1335 ;    //1335
	Motor.LD_Speed=45;  //20
 
	Motor.Right_Up = 1000 ;    //970
	Motor.RU_Speed = 40;  // 40

	Motor.Right_Mid = 1710;	  //1695
	Motor.RM_Speed = 25;  //  35

	Motor.Right_Down = 1300 ;   //1400
	Motor.RD_Speed = 60;  //40

	Leg_Move(&Motor);
	
//	return ; 
	/*************伸右腿并放下******第二步结束*********/	

	Motor.Left_Up = 1670;
	Motor.LU_Speed = 30;		
	Motor.Left_Mid = 1240;	
	Motor.LM_Speed = 25;		
	Motor.Left_Down = 1470;		
	Motor.LD_Speed = 20;

	Motor.Right_Up = 1030 ;
	Motor.RU_Speed = 30;
	Motor.Right_Mid = 1825;	
	Motor.RM_Speed = 20;
	Motor.Right_Down=1485 ;
	Motor.RD_Speed=15;

	Leg_Move(&Motor);	
	
//	return ;

	/***************抬左脚**********************/

// 	Motor.Left_Up = 1710;
// 	Motor.LU_Speed = 30;
	
	Motor.Left_Down=1710 ;
	Motor.LD_Speed=80;	//60

	Motor.Right_Down=1610;	
	Motor.RD_Speed=40;  //18

	Leg_Move(&Motor);
	

       
	/**********************第三步  迈到中间**********************/	

	Motor.Left_Up=1000 ;
	Motor.LU_Speed=40;  //30

	Motor.Left_Mid=1690 ;
	Motor.LM_Speed = 20;

	Motor.Left_Down=1820 ;
	Motor.LD_Speed=90;  //40

	Motor.Right_Up=1500 ;
	Motor.RU_Speed=20;

	Motor.Right_Mid=1255;	
	Motor.RM_Speed=20;

	Motor.Right_Down=1670;		//数值越大  越往右偏   1675
	Motor.RD_Speed=60;  //50
				
	Leg_Move(&Motor);
      
//return ;			
		 
	/*****************伸左腿并放下*****第三步结束*********************/

	Motor.Left_Up=1020 ;
	Motor.LU_Speed=35;

	Motor.Left_Mid=1715 ;
	Motor.LM_Speed=20;

	Motor.Left_Down=1515 ;
	Motor.LD_Speed=30;

	Motor.Right_Up=1480 ;
	Motor.RU_Speed=35;

	Motor.Right_Mid=1210;	
	Motor.RM_Speed=28;

	Motor.Right_Down=1520;	
	Motor.RD_Speed=40;  //20

	Leg_Move(&Motor);	
	   
  
	/**************抬右脚********************/

	Motor.Left_Up=1020 ;
	Motor.LU_Speed=30;

	Motor.Left_Mid=1715 ;
	Motor.LM_Speed=20;

	Motor.Left_Down=1375 ;
	Motor.LD_Speed=20;

	Motor.Right_Up=1535 ;
	Motor.RU_Speed=30;

	Motor.Right_Mid=1215;	
	Motor.RM_Speed=30;

	Motor.Right_Down=1010;	
	Motor.RD_Speed=40;  //60

	Leg_Move(&Motor);
	

	/****************迈到中间***************************/	
	Motor.Left_Up=1500;
	Motor.LU_Speed=20;  //30

	Motor.Left_Mid=1500;	
	Motor.LM_Speed=15;    //35

	Motor.Left_Down=1380 ;
	Motor.LD_Speed=40;   //  30

	Motor.Right_Up=1500 ;
	Motor.RU_Speed=10;  //40

	Motor.Right_Mid=1475 ; // 1465
	Motor.RM_Speed=10;   //20

	Motor.Right_Down=1350 ;
	Motor.RD_Speed=90; //20
			
	Leg_Move(&Motor);
   
	/***************立正*********/

	Motor.Right_Down=1500 ;
	Motor.RD_Speed=20;

	Motor.Left_Down=1500 ;
	Motor.LD_Speed=20;

	Leg_Move(&Motor);
//   return ;
}

void Setp_31( void )		//向前走3步
{

/*************************抬起左脚***************************************/
	
	Motor.Left_Down = 1750 ;
	Motor.LD_Speed = 80;
	
	Motor.Right_Down=1650;	
	Motor.RD_Speed=50;
	
	Leg_Move(&Motor);

/********************** 第一步 迈左脚***** end *****************/	
		
	Motor.Left_Up = 1100 ;
	Motor.LU_Speed = 25;

	Motor.Left_Mid = 1900 ;
	Motor.LM_Speed = 25;
	
	Motor.Left_Down = 1515 ;
	Motor.LD_Speed = 50;
	
	Motor.Right_Up = 1900;
	Motor.RU_Speed = 25;

	Motor.Right_Mid = 1100;	
	Motor.RM_Speed = 25;
	
	Motor.Right_Down=1500;	
	Motor.RD_Speed=14;
	
	Leg_Move(&Motor);
	
	
/***************** 抬右脚**************************/
		Motor.Left_Up = 1500;	 
		Motor.Left_Mid = 1500 ;
 		Motor.Left_Down = 1420 ;
		Motor.Right_Up = 1500;
		Motor.Right_Mid = 1500;		
		Motor.Right_Down = 1450;	

		Motor.LU_Speed=25;
		Motor.LM_Speed=25;
		Motor.LD_Speed=45;
		Motor.RU_Speed=25;
		Motor.RM_Speed=25;
		Motor.RD_Speed=70;
		

		Leg_Move(&Motor);
	
/******************   放下右脚  ********************/	
		Motor.Left_Up = 1860 ;
		Motor.LU_Speed = 25;

		Motor.Left_Mid = 1140 ;
		Motor.LM_Speed = 25;
		
		Motor.Left_Down = 1466 ;  /////改变此值可改变方向  高 右  低  左 注意电压影响。
		Motor.LD_Speed = 10;
		
		Motor.Right_Up = 1125;
		Motor.RU_Speed = 25;

		Motor.Right_Mid = 1860;	
		Motor.RM_Speed = 25;
		
		Motor.Right_Down=1500;	
		Motor.RD_Speed=45;
		
		Leg_Move(&Motor);
			

/*************************抬起左脚***************************************/
	

	Motor.Left_Up = 1500;	 
	Motor.Left_Mid = 1500 ;
	Motor.Left_Down = 1750 ;
	Motor.Right_Up = 1500;
	Motor.Right_Mid = 1500;		
	Motor.Right_Down = 1650;	


	Motor.RU_Speed=25;
	Motor.RM_Speed=25;
	Motor.RD_Speed=45;
	Motor.LU_Speed=25;
	Motor.LM_Speed=25;
	Motor.LD_Speed=70;
	
	Leg_Move(&Motor);

/********************** 迈左脚***** end *****************/	
		
	Motor.Left_Up = 1100 ;
	Motor.LU_Speed = 25;

	Motor.Left_Mid = 1900 ;
	Motor.LM_Speed = 25;
	
	Motor.Left_Down = 1515 ;
	Motor.LD_Speed = 70;
	
	Motor.Right_Up = 1900;
	Motor.RU_Speed = 25;

	Motor.Right_Mid = 1100;	
	Motor.RM_Speed = 25;
	
	Motor.Right_Down=1500;	
	Motor.RD_Speed=10;
	
	Leg_Move(&Motor);
	
	
	delay_ms(20);
		
/***************** 抬右脚**************************/
	Motor.Left_Up = 1500;	 
	Motor.Left_Mid = 1500 ;
	Motor.Left_Down = 1390 ;
	Motor.Right_Up = 1500;
	Motor.Right_Mid = 1500;		
	Motor.Right_Down = 1400;	
	
	
	Motor.LU_Speed=25;
	Motor.LM_Speed=25;
	Motor.LD_Speed=10;
	Motor.RU_Speed=25;
	Motor.RM_Speed=25;
	Motor.RD_Speed=35;
	
	
	Leg_Move(&Motor);
	
	
	

/***************  立正  ****************/

	Motor.Right_Down=1500 ;
	Motor.RD_Speed=30;

	Motor.Left_Down=1500 ;
	Motor.LD_Speed=30;

	Leg_Move(&Motor);


}


void Roll_forward( void )
{
	
	
	u8 t2;
	t2 = 3 ;		//前翻个数
	while(t2--)
	{
		/***************四个舵机90度*******************/
		Motor.Left_Up = 680;
		Motor.LU_Speed=35;
		
		Motor.Left_Mid=635;
		Motor.LM_Speed=37;
		
		Motor.Left_Down=1485;
		Motor.LD_Speed=10;
		
		Motor.Right_Up=650;
		Motor.RU_Speed=36;
		
		Motor.Right_Mid=585;	
		Motor.RM_Speed=39;
		
		Motor.Right_Down=1545;
		Motor.RD_Speed=10;
		Leg_Move(&Motor);
//		return ;
		
	/*************************************************/
		Motor.Left_Up =1100;
		Motor.LU_Speed=50;
		Leg_Move(&Motor);
				
		Motor.Left_Up = 1650;
		Motor.LU_Speed = 50;
		
		Motor.Left_Mid = 1750 ;
		Motor.LM_Speed = 60;	
		Leg_Move(&Motor);
		
			
		Motor.Left_Up = 2000;
		Motor.LU_Speed = 50;
		Motor.Right_Up = 1000;
		Motor.RU_Speed = 50;
		Leg_Move(&Motor);
		
		Motor.Left_Up = 2040;		//左歪 右歪 控制
		Motor.LU_Speed = 20;
		Motor.Left_Mid = 2115;
		Motor.LM_Speed = 60;	
		Motor.Left_Down = 1535;
		Motor.LD_Speed = 50;
		Motor.Right_Up = 2080;
		Motor.RU_Speed = 75;
		Motor.Right_Mid = 2120;	
		Motor.RM_Speed = 70;
		Motor.Right_Down = 1545;
		Motor.RD_Speed = 50;
		Leg_Move(&Motor);
		
		delay_ms(100);
		
		Motor.Right_Up=1500;
		Motor.Right_Mid=1500;		
		Motor.Right_Down=1500;	
		Motor.Left_Up =1500;	 
		Motor.Left_Mid=1500 ;
		Motor.Left_Down=1500 ;

		Motor.RU_Speed=30;
		Motor.RM_Speed=30;
		Motor.RD_Speed=30;
		Motor.LU_Speed=30;
		Motor.LM_Speed=30;
		Motor.LD_Speed=30;

		Leg_Move(&Motor);

	}
	
	delay_ms(100);
}

void Roll_back( void )
{	
	u8 t=3;
	while(t--)
	{
	/***************四个舵机90度*******************/
		Motor.Left_Up = 2500;
		Motor.Left_Mid=2525;
		Motor.Left_Down=1500 ;
		
		Motor.Right_Up = 2550;	
		Motor.Right_Mid=2570;		
		Motor.Right_Down=1540;

		Motor.RU_Speed=48;
		Motor.RM_Speed=37;
		Motor.RD_Speed=20;
		Motor.LU_Speed=50;
		Motor.LM_Speed=35;
		Motor.LD_Speed=20;
		Leg_Move(&Motor);	
	
		
/*******************LEFT***************/
		Motor.Left_Up = 2000;
		Motor.LU_Speed = 80;
		
		Leg_Move(&Motor);
		
		
		Motor.Left_Up = 1120;//20次
		Motor.LU_Speed = 44;	
		
		Motor.Left_Mid = 1000;
		Motor.LM_Speed = 78;//2060/26
		
		Motor.Left_Down = 1500 ;
		Motor.LU_Speed = 40;//45
		
		Leg_Move(&Motor);
				
	/*******************RIGHT***********************/			
		Motor.Left_Up = 956;//545
		Motor.LU_Speed = 44;	
		
		Motor.Left_Mid = 510;
		Motor.LM_Speed = 110;///26
		
		Motor.Right_Up=2180;
		Motor.RU_Speed=60;
		
		Leg_Move( &Motor );

		
		
		Motor.Left_Up = 545;//545
		Motor.LU_Speed = 45;	
		
		Motor.Right_Up=500;
		Motor.RU_Speed=45;
		
		Motor.Right_Mid=495;	
		Motor.RM_Speed=100;
		Leg_Move(&Motor);

		delay_ms(100);
		
//		return ;
		
		Motor.Left_Mid=965 ;
		Motor.LM_Speed=65;
		
		Motor.Right_Mid=1000;	
		Motor.RM_Speed=40;
		
		Leg_Move(&Motor);
		
//		return ;
		
		Motor.Left_Up =1500;	 
		Motor.Left_Mid=1500 ;
		Motor.Left_Down=1500 ;		
		Motor.Right_Up=1500;
		Motor.Right_Mid=1500;		
		Motor.Right_Down=1500;	

		
		Motor.LU_Speed=60;
		Motor.LM_Speed=50;
		Motor.LD_Speed=10;
		Motor.RU_Speed=80;
		Motor.RM_Speed=40;
		Motor.RD_Speed=15;

		Leg_Move(&Motor);
	}

}


