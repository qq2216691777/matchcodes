#ifndef _ROBOT_H_
#define _ROBOT_H_
#include "stm32f10x_tim.h"
#include "delay.h"
#include "robot_go.h"

void Robot_Ready( void );


extern int	L_Up;
extern int	L_Mid;
extern int	L_Down;
extern int	R_Up;
extern int	R_Mid;
extern int	R_Down;

#define L_U (60)
#define L_M (-5)
#define L_D (115)

#define R_U (-25)
#define R_M (100)
#define R_D (-85)


typedef struct
{
	int Right_Up;	
    int Right_Mid;	
	int Right_Down;
	int Left_Up;	 
	int Left_Mid;	
	int Left_Down;
	
 	int RU_Speed;
	int RM_Speed;	
	int RD_Speed;
	int LU_Speed;
	int LM_Speed;
	int LD_Speed;
} MoTor;



void Leg_Move( MoTor* p );

#endif

