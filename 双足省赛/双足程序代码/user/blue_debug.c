#include "blue_debug.h"

extern MoTor Motor;

int	DL_Up=1500;
int	DL_Mid=1500;
int	DL_Down=1500;
int	DR_Up=1500;
int	DR_Mid=1500;
int	DR_Down=1500;


void Blue_Debug( void )
{
	if( USART_RX_STA&0x8000 )		//判断串口是否接收到数据
		{
			if( USART_RX_BUF[0]== 's' )			//放回
			{
				printf("1.%d\n",Motor.Left_Up);
				delay_ms(100);
				printf("2.%d\n",Motor.Left_Mid);
				delay_ms(100);
				printf("3.%d\n",Motor.Left_Down);
				delay_ms(100);
				printf("4.%d\n",Motor.Right_Up);
				delay_ms(100);
				printf("5.%d\n",Motor.Right_Mid);
				delay_ms(100);
				printf("6.%d\n",Motor.Right_Down);
			}
			else if( USART_RX_BUF[0] == 'a' ) 		//立正
			{
				Motor.Right_Up=1500;
				Motor.Right_Mid=1500;		
				Motor.Right_Down=1500;	
				Motor.Left_Up =1500;	 
				Motor.Left_Mid=1500 ;
				Motor.Left_Down=1500 ;

				Motor.RU_Speed=30;
				Motor.RM_Speed=30;
				Motor.RD_Speed=30;
				Motor.LU_Speed=30;
				Motor.LM_Speed=30;
				Motor.LD_Speed=30;
		
				Leg_Move(&Motor);	
			}
			else if( USART_RX_BUF[0] == 'l')
			{
				Setp_3( 10 );
			}
			else if( USART_RX_BUF[0] == 'r')
			{
				Roll_forward();
			}
			else if( USART_RX_BUF[0] == 'f')
			{						
				Begin2_step();				
			}
			else if( USART_RX_BUF[0] == 'b')
			{						
				Roll_back();					
			}
			else if( USART_RX_BUF[0] == 'z')
			{						
				Begin2_step();
				Roll_forward();
			//	Setp_3( 1 );
				Begin2_step();
				delay_ms(100);
				Roll_back();	
				Setp_3( 14 );
			}
			else if( USART_RX_BUF[0] == 'X')
			{						
				Motor.Left_Up+=5 ;	
				Motor.LU_Speed=5;
				Leg_Move(&Motor);					
			}
			else if( USART_RX_BUF[0] == 'Y')
			{						
				Motor.Left_Mid+=5 ;
				Motor.LM_Speed=5;				
				Leg_Move(&Motor);					
			}
			else if( USART_RX_BUF[0] == 'W')
			{						
				Motor.Left_Down+=5 ;
				Motor.LD_Speed=5;
				Leg_Move(&Motor);					
			}
			else if( USART_RX_BUF[0] == 'x')
			{						
				Motor.Left_Up-=5 ;	
				Motor.LU_Speed=5;
				Leg_Move(&Motor);					
			}
			else if( USART_RX_BUF[0] == 'y')
			{						
				Motor.Left_Mid-=5 ;
				Motor.LM_Speed=5;				
				Leg_Move(&Motor);					
			}
			else if( USART_RX_BUF[0] == 'w')
			{						
				Motor.Left_Down-=5 ;
				Motor.LD_Speed=5;
				Leg_Move(&Motor);					
			}
			
			else if( USART_RX_BUF[0] == 'O')
			{						
				Motor.Right_Up+=5 ;
				Motor.RU_Speed=5;				
				Leg_Move(&Motor);					
			}
			else if( USART_RX_BUF[0] == 'P')
			{						
				Motor.Right_Mid +=5 ;	
				Motor.RM_Speed=5;		
				Leg_Move(&Motor);					
			}
			else if( USART_RX_BUF[0] == 'Q')
			{						
				Motor.Right_Down+=5 ;	
				Motor.RD_Speed=5;		
				Leg_Move(&Motor);					
			}
			
			else if( USART_RX_BUF[0] == 'o')
			{						
				Motor.Right_Up-=5 ;
				Motor.RU_Speed=5;				
				Leg_Move(&Motor);					
			}
			else if( USART_RX_BUF[0] == 'p')
			{						
				Motor.Right_Mid-=5 ;	
				Motor.RM_Speed=5;		
				Leg_Move(&Motor);					
			}
			else if( USART_RX_BUF[0] == 'q')
			{						
				Motor.Right_Down-=5 ;	
				Motor.RD_Speed=5;		
				Leg_Move(&Motor);					
			}
			else
			{
				printf("error\n");
			}
			
			USART_RX_STA = 0;
		
		}
			
}

// void String_Num( unsigned char *p, int *dat )				//把字符型数据转换成整形
// {
// 	int Num_temp=0;
// 		if( *p++ == ' ' )
// 		{
// 			for( ;*p<='9' && *p>='0';*p++)
// 			{
// 					Num_temp *=10;
// 					Num_temp += *p-48;
// 			}
// 			*dat = Num_temp;	
// 		}
// 		else
// 		{
// 				printf("指令格式错误！请重新输入！\n");
// 		}
//  
// }



