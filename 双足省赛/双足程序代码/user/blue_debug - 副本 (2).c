#include "blue_debug.h"
int	L_Up=1500;
int	L_Mid=1500;
int	L_Down=1500;
int	R_Up=1500;
int	R_Mid=1500;
int	R_Down=1500;
extern int temp;
extern int speed;
/*********************************
指令格式 [左右][上下][空格][高电平占空比][回车]
左右  L左  R右
上下  U上  M中	D下
高电平占空比  大小在500-2500
**********************************/
void Blue_Debug( void )
{
	if( USART_RX_STA&0x8000 )		//判断串口是否接收到数据
		{
			if( USART_RX_BUF[0]== 'S' )			//放回
			{
				printf("L_Up：%d\n",L_Up);
				printf("L_Mid：%d\n",L_Mid);
				printf("L_Down：%d\n",L_Down);
				printf("R_Up：%d\n",R_Up);
				printf("R_Mid：%d\n",R_Mid);
				printf("R_Down：%d\n",R_Down);
			}
			else if( 'A' == USART_RX_BUF[0] )	//立正
			{
					L_Up=1500;
					L_Mid=1500;
					L_Down=1500;
					R_Up=1500;
					R_Mid=1500;
					R_Down=1500;
			}
			else if( 'L' == USART_RX_BUF[0] )
			{
					if(temp<9 )temp+=10;
		  }
			else if( 'R' == USART_RX_BUF[0] )
			{
				if(temp<9 )temp+=20;
				else if(temp<19 )temp+=10;
			}
			else if( 'U' == USART_RX_BUF[0] )
			{
				if(temp%10==0 )temp+=1;
				else if(temp%10==2 )temp-=1;
				else if(temp%10==3 )temp-=2;
			}
			else if( 'M' == USART_RX_BUF[0] )
			{
				if(temp%10==0 )temp+=2;
				else if(temp%10==1 )temp+=1;
				else if(temp%10==3 )temp-=1;
			}
			else if( 'D' == USART_RX_BUF[0] )
			{
				if(temp%10==0 )temp+=3;
				else if(temp%10==2 )temp+=1;
				else if(temp%10==1 )temp+=2;
			}
			else if( '1' == USART_RX_BUF[0] )
			{
					speed=10;
			}
			else if( '5' == USART_RX_BUF[0] )
			{
				speed=50;
			}
			else if( '2' == USART_RX_BUF[0] )
			{
				speed=200;
			}
			else if( 'Z' == USART_RX_BUF[0] )
			{
					if( temp/10==1)
					{
						if(temp%10==1)L_Up+=speed;
						else if(temp%10==2)L_Mid+=speed;
						else if(temp%10==3)L_Down+=speed;
						else printf("下达完整的指令不完整！\r\n");
					}
					else if( temp/10==2)
					{
						if(temp%10==1)R_Up+=speed;
						else if(temp%10==2)R_Mid+=speed;
						else if(temp%10==3)R_Down+=speed;
						else printf("下达完整的指令不完整！\r\n");
					}else printf("下达完整的指令不完整！\r\n");
					
					Left_Up( L_Up );
					Left_Mid( L_Mid );
					Left_Down( L_Down );
					Right_Up( R_Up );
					Right_Mid( R_Mid );
					Right_Down( R_Down );
					delay_ms( 1000 );
			}
			else if( 'J' == USART_RX_BUF[0] )
			{
					if( temp/10==1)
					{
						if(temp%10==1)L_Up-=speed;
						else if(temp%10==2)L_Mid-=speed;
						else if(temp%10==3)L_Down-=speed;
						else printf("下达完整的指令不完整！\r\n");
					}
					else if( temp/10==2)
					{
						if(temp%10==1)R_Up-=speed;
						else if(temp%10==2)R_Mid-=speed;
						else if(temp%10==3)R_Down-=speed;
						else printf("下达完整的指令不完整！\r\n");
					}else printf("下达完整的指令不完整！\r\n");
					
					Left_Up( L_Up );
					Left_Mid( L_Mid );
					Left_Down( L_Down );
					Right_Up( R_Up );
					Right_Mid( R_Mid );
					Right_Down( R_Down );
					delay_ms( 1000 );
					USART_RX_STA = 0;
			}
			else if( ' ' == USART_RX_BUF[0] )			
			{	
				
			}
			else				
			{	
				printf("指令格式错误！请重新输入！\n");
			}	
			USART_RX_STA = 0;			
		}		
}



