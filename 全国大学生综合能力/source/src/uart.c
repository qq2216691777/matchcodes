/**
  ***************************************
  *@File     uart.c
  *@Auther   YNXF  
  *@date     2016-09-12
  *@Version  v1.0
  *@brief    stc15w404as-16pin          
  *@HAUST - RobotLab for National Contest of engineering training for comprehensive abilities for College Students
  ***************************************
**/
#include "main.h"

#if  _DEBUG

bit busy;		//Send busy flag bit 

/********************
	name��	void Uart_Init()
	explain��Serial 1 initialization 
********************/
void Uart_Init()
{

#if(PARITYBIT == NONE_PARITY )	
	SCON = 0X50;
#elif(PARITYBIT==ODD_PARITY)||(PARITYBIT==EVEN_PARITY)||(PARITYBIT==MARK_PARITY)
 	SCON = 0Xda;
#elif(PARITYBIT==SPACE_PARITY)
	SCON = 0Xd2;
#endif

	T2L = (65536-(FOSC/4/BAUD));
	T2H = (65536-(FOSC/4/BAUD))>>8;

	AUXR = 0X14;
	AUXR |= 0X01;
	ES = 1;
	EA = 1;

}

void Uart() interrupt 4 using 1
{
 	if(RI)
	{
	 	RI = 0;
	}

	if(TI)
	{
	 	TI = 0;
		busy = 0;
	}
}

/*****************
	name��	void SendData( BYTE dat )
	explain��Send a byte through the serial port 
******************/
void SendData( BYTE dat )
{
	while(busy);
	ACC = dat;
	if(P)
	{
#if(PARITYBIT==ODD_PARITY)
	   	TB8=0;
#elif(PARITYBIT==EVEN_PARITY)
		TB8=1;
#endif
	}
	else
	{
#if(PARITYBIT==ODD_PARITY)
	   	TB8=0;
#elif(PARITYBIT==EVEN_PARITY)
		TB8=1;
#endif
	}
	busy = 1;
	SBUF = ACC;
}


/**************
	name��	void SendString( char *s )
	explain��Send strings through the serial port 
***************/
void SendString( char *s )
{
 	while(*s)
	{
	 	 SendData(*s++);
	}
}

/**************
	name��	void SendNum( long n )
	explain��Send a number through the serial port 
***************/
void SendNum( long n )
{
	long n_flag = (long)n;
	char Num[15]={0};
	BYTE i=13;

	do
	{
	 	Num[i--] = n_flag%10 + 0x30;
		n_flag = n_flag/10;
	}while( n_flag );
	if(n<0)
		Num[i] = '-';
	else
		Num[i] = ' ';
	

	SendString(&Num[i]);
}

#endif