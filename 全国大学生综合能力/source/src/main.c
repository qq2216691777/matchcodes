/**
  ***************************************
  *@File     main.c
  *@Auther   YNXF  
  *@date     2017-03-13
  *@Version  v3.0
  *@brief    stc15w408as-16pin  
  *@Rate	 33.1776Mhz       
  *@HAUST - RobotLab for National Contest of engineering training for comprehensive abilities for College Students
  ***************************************
**/

#include "main.h"
#include "tactics.h"
#include "infrared.h"
u8 ttt = 0;
/*
	name: void delay()
*/
void delay()
{
 	int x,y;
	for(x=150;x>1;x--)
	{
		for(y=1000;y>1;y--);
	}
}


extern u8 time_100ms;

int main()
{
	u8 inf_val=0;
	u8 mid=0;
	u8 temp_n = 0;

	P5M1 &= ~0X20;		 //P55设置为推挽模式
	P5M0 &= ~0X20;
	LED = 0;

#if _DEBUG
	Uart_Init();
#endif

	PWM_Init();
	Motor(MOTOR_CENTER);
    delay();
			//  while(1);
	while(1)  
	{

		while(!time_100ms)
		{

		}
		time_100ms = 0;
		inf_val = infrared();


#if  _DEBUG
	
		SendNum(temp_n);
		
		if( inf_val&0x01 )
			SendString(" inf 1:");
		else
			SendString(" inf 0:");
		if( inf_val&0x02 )
			SendString(" 1:");
		else
			SendString(" 0:");
		
		if( inf_val&0x04 )
			SendString(" 1:");
		else
			SendString(" 0:");
		
		if( inf_val&0x08 )
			SendString(" 1:");
		else
			SendString(" 0:");
		
		if( inf_val&0x10 )
			SendString(" 1\r\n");
		else
			SendString(" 0\r\n");	 
#endif

	    go(inf_val);

	}

}
