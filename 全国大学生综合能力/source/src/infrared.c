#include "infrared.h"
#include "main.h"

sbit inf1 = P1^2;		  
sbit inf2 = P5^4;	 

sbit inf3 = P3^6;
sbit inf4 = P1^4;
sbit inf5 = P1^3;

u8 infrared()
{
	u8 flag = 0;
	if( inf1 )
	{
		flag |= 0x01;
	}

	if( inf2 )
	{
		flag |= 0x02;
	}

	if( inf3 )
	{
		flag |= 0x04;
	}

	if( inf4 )
	{
		flag |= 0x08;
	}

	if( inf5 )
	{
		flag |= 0x10;
	}

	return 	flag;
}
