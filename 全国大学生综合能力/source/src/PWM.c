/**
  ***************************************
  *@File     pwm.c
  *@Auther   YNXF  
  *@date     2016-09-12
  *@Version  v1.0
  *@brief    stc15w404as-16pin          
  *@HAUST - RobotLab for National Contest of engineering training for comprehensive abilities for College Students
  ***************************************
**/
#include "main.h"

sbit P_PWM0 = P1^5;
//sbit P_PWM1 = P5^4;

u16 pwm_duty;		//周期计数值
u8 pwm[2];			//pwm0-pwm1的宽度值

bit B_1ms;
u8	cnt_1ms;
u8	cnt_20ms;

u8 time_100ms = 0;


void PWM_Init( void )
{
	 AUXR	|= (1<<7);
	 TMOD	&= ~(1<<2);
	 TMOD	&=~0x03;
	 TH0 = Timer_Reload/256;
	 TL0 = Timer_Reload%256;
	 ET0 = 1;
	 PT0 = 1;
	 TR0 = 1;
	 EA = 1;
	 cnt_1ms = Timer0_Rate/1000;
	 cnt_20ms = 20;
	 pwm[0]= 150;  //1.5MS
	 pwm[1]= 150;
}

void timer0() interrupt 1
{
	static u8 cnt_100ms=0;
	static u8 led_num=0;
 	if(++pwm_duty == PWM_DUTY_MAX )
	{
	 	  pwm_duty = 0;
		  P_PWM0 = PWM_ON;
	}

	ACC = pwm_duty;
	if(ACC==pwm[0])	P_PWM0 = PWM_OFF;
	if(--cnt_1ms == 0)
	{
	 	 cnt_1ms = Timer0_Rate/1000;
		 B_1ms = 1;
		 cnt_100ms++;
	}
	if( cnt_100ms ==100 )
	{
		 cnt_100ms = 0;
		 led_num++;
		 time_100ms = 1;
		 if( led_num>5 )
		 {
			  led_num = 0;
			  LED = ~LED;
		 }
	}
}

void Motor( float ang1 )
{
	  static float a1=90;
	  float temp;
	 // ang1 = 60;
	  if( (ang1<0)||((ang1>180)) )
	  	return ;

	   if( a1==ang1)
	   		return ;
		temp = a1-ang1;
		if(temp>10)
			a1 -= 10;
		else if(temp<-10)
			a1 += 10;
		else 
			a1 = ang1;	   
	   pwm[0] = ((a1 -90)*1.11)+150;
	  
}