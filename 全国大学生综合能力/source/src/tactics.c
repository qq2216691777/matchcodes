#include "tactics.h"
#include "main.h"


void go( u8 inf_val )
{

	if( (inf_val&0x02)&&(!(inf_val&0x04)) )		//3号有障碍物  2号没有
	{
		Motor(MOTOR_CENTER+20);
	}
	else  if( (inf_val&0x04)&&(!(inf_val&0x02)) )	   //2号有障碍物  3号没有
	{
		Motor(MOTOR_CENTER-20);
	}
	else  if( (!(inf_val&0x04))&&(!(inf_val&0x02)) )	   //  都有障碍物
	{
		if( (!(inf_val&0x08))&&(inf_val&0x01) )			  //如果4号有	1号无
		{
			 Motor(MOTOR_CENTER+40);
		}
		else if( (!(inf_val&0x01))&&(inf_val&0x08) )			  //如果1号有 4号无
		{
			 Motor(MOTOR_CENTER-40);
		}
	}
	else  if( (inf_val&0x10)&&((inf_val&0x02)) )	   //都没有障碍物
	{
		
		if( (!(inf_val&0x08))&&(inf_val&0x01) )			  //如果4号有	1号无
		{
			// Motor(MOTOR_CENTER+10);
		}
		else if( (!(inf_val&0x01))&&(inf_val&0x08) )			  //如果1号有 4号无
		{
			// Motor(MOTOR_CENTER-10);
		}
	}
	else
	{
	 	Motor(MOTOR_CENTER);
	}

}