#ifndef _UART_H__
#define _UART_H__

#define	BAUD	115200

#define NONE_PARITY		0	//无校验
#define ODD_PARITY		1	//奇校验
#define EVEN_PARITY		2	//偶校验
#define	MARK_PARITY		3  	//标记校验
#define SPACE_PARITY	4	//空白校验

#define PARITYBIT NONE_PARITY

sfr AUXR	=	0X8E;
sfr T2H		=	0XD6;
sfr T2L		=	0XD7;
sbit	P22		=	P2^2;

#if  _DEBUG
void Uart_Init();
void SendString( char *s );
void SendNum( long n );
#endif

#endif

