#ifndef _PWM_H__
#define _PWM_H__


#define 	MAIN_Fosc 	33177600L

#define		Timer0_Rate	 100000

typedef unsigned char u8;
typedef unsigned int u16;
typedef unsigned long u32;


#define Timer_Reload	(65536UL-(MAIN_Fosc/Timer0_Rate))

#define	PWM_DUTY_MAX	2000
#define PWM_ON			1

#define	PWM_OFF	(!PWM_ON)

#define	PWM_ALL_ON	(0XFF*PWM_ON)




void PWM_Init( void );
void Motor( float );

#endif
