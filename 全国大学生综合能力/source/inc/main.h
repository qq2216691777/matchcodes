#ifndef _MAIN_H__
#define _MAIN_H__

/*  BYTE Registers  */
sfr P_SW1		= 0XA2;	 	//外设功能切换寄存器

#define CCP_S0	0X10		//P_SW1.4
#define CCP_S1	0X20		//P_SW1.5

sfr CCON    	= 0xD8;			//PCA控制寄存器
sbit CCF0		= CCON^0;
sbit CCF1		= CCON^1;
sbit CCF2		= CCON^2;
sbit CR			= CCON^6;
sbit CF			= CCON^7;

sfr CMOD    	= 0xD9;			//PCA工作模式寄存器
sfr CL    		= 0xE9;
sfr CH    		= 0xF9;

sfr CCAPM0    	= 0xDA;		//比较/捕获寄存器0
sfr CCAP0L    	= 0xEA;	//当PCA模块用于PWM模式时，用来控制输出占空比
sfr CCAP0H    	= 0xFA;

sfr CCAPM1    	= 0xDB;		//比较/捕获寄存器1
sfr CCAP1L    	= 0xEB;
sfr CCAP1H    	= 0xFB;

sfr CCAPM2    	= 0xDC;		//比较/捕获寄存器2
sfr CCAP2L    	= 0xEC;
sfr CCAP2H    	= 0xFC;

sfr	PCA_PWM0	= 0XF2; 	//PWM寄存器
sfr	PCA_PWM1	= 0XF3;
sfr	PCA_PWM2	= 0XF4;

sfr	P5			= 0XC8;
sfr	P5M0		= 0XCA;
sfr	P5M1		= 0XC9;


sfr	INT_CLK0		= 0X8F;


#define _DEBUG		1

#include <REG51.H>
#include <intrins.h>
#include "pwm.h"
#include "uart.h"



#define 	FOSC	33177600L		//33.1776Mhz

typedef	unsigned char	BYTE;
typedef	unsigned int	WORD;
typedef	unsigned long	DWORD;

sbit LED = P5^5;

#define MOTOR_CENTER	90

#endif
